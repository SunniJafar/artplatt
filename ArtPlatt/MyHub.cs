﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ArtPlatt
{
    public class MyHub : Hub
    {
        public override Task OnConnected()
        {
            string SenderID = Context.QueryString["SenderID"];

            Groups.Add(Context.ConnectionId, SenderID);
            return base.OnConnected();
        }
        public void SendMessage(int ReceiverId, string MessageText)
        {
            string SenderID = Context.QueryString["SenderID"];

            Clients.Group(ReceiverId.ToString()).receivedMessage(SenderID, MessageText);
        }

    }
}