﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EditArt.aspx.cs" Inherits="ArtPlatt.EditArt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        function ShowImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgprv1.ClientID%>').prop('src', e.target.result)
                        ;
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function ShowImagePreview2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgprv2.ClientID%>').prop('src', e.target.result)
                        ;
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function ShowImagePreview3(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgprv3.ClientID%>').prop('src', e.target.result)
                        ;
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/backgrounds/shop-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>Edit Art</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->
    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Coupon -->

            <div class="ws-checkout-content clearfix">

                <!-- Cart Content -->
                <div class="col-sm-7">

                    <!-- Billing Details -->
                    <div class="ws-checkout-billing">
                        <h3>Art Details</h3>
                           <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <!-- Form Inputs -->
                        <div class="form-inline">

                            <!-- Name -->
                          <%--  <div class="ws-checkout-first-row">
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                                    <label>Category <span>* </span></label>
                                    <br>
                                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel ID="upSetSession" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drdCategory" runat="server" OnSelectedIndexChanged="drdCategory_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Select Category" ForeColor="Red" ControlToValidate="drdCategory" InitialValue="0" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="drdCategory" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Sub Category <span>* </span></label>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drdsubcategory" runat="server"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Sub Category" ForeColor="Red" ControlToValidate="drdsubcategory" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="drdsubcategory" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>--%>
                            <div class="ws-checkout-first-row">
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                      <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                                    <label>Art Name <span>* </span></label>
                                    <br />
                                    <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Art Name" ForeColor="Red" ControlToValidate="txtname" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                </div>

                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Price</label>
                                    <br>
                                    <asp:TextBox ID="txtprice" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <!-- Company -->

                            <div class="col-no-p ws-checkout-input col-sm-12">
                                <label>Description</label><br />
                                <asp:TextBox ID="txtdesc" Rows="2" Columns="5" runat="server" TextMode="MultiLine"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ErrorMessage="Enter Description" ForeColor="Red" ControlToValidate="txtdesc" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Characters Minimum (50 - 200)"
                                    ControlToValidate="txtdesc" ValidationExpression="^[\s\S]{50,200}" CssClass="required" Display="Dynamic" ValidationGroup="Submit"></asp:RegularExpressionValidator>--%>
                            </div>
                            <div class="col-no-p ws-checkout-input col-sm-12">
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <asp:Button ID="btnsubmit" runat="server" Text="Update" Font-Italic="false" Style="width: 100%; background-color: #C2A476; color: #fff;" OnClick="btnsubmit_Click" ValidationGroup="Add" class="btn ws-btn-fullwidth" />
                                </div>
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete Art" Font-Italic="false" Style="width: 100%; background-color: #C2A476; color: #fff;" OnClick="btnDelete_Click" class="btn ws-btn-fullwidth" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Cart Total -->
                <div class="col-sm-5">
                    <div class="ws-checkout-order">
                        <h2>Art Images / Files (Up to 3)</h2>
                        <label class="col-sm-12 form-control-label">Upload Image, Audio, Video, Pdf</label>
                        <a id="prv1" runat="server" target="_blank"  visible="false" style="color:blue" title="View PDF">View File</a>
                        <asp:Image ID="imgprv1" runat="server" BorderStyle="None" Style="max-height: 300px" CssClass="img-responsive" />
                        <%--<video id="video" runat="server" style="padding-top: 65px;" controls="true" class="img-responsive"></video>--%>
                        <asp:FileUpload ID="fuimage1" onchange="ShowImagePreview(this);" runat="server" />
                        <asp:Image ID="imgprv2" runat="server" Style="max-height: 300px" CssClass="img-responsive" />
                        <a id="prv2" runat="server" target="_blank" visible="false" style="color:blue" title="View PDF">View File</a>
                        <asp:FileUpload ID="fuimage2" onchange="ShowImagePreview2(this);" runat="server" /><br />
                        <asp:Image ID="imgprv3" runat="server" Style="max-height: 300px" CssClass="img-responsive" />
                        <a id="prv3" runat="server" target="_blank"  visible="false" style="color:blue" title="View PDF">View File</a>
                        <asp:FileUpload ID="fuimage3" onchange="ShowImagePreview3(this);" runat="server" /><br />
                        <asp:HiddenField ID="hf1" runat="server" />
                        <asp:HiddenField ID="hf2" runat="server" />
                        <asp:HiddenField ID="hf3" runat="server" />

                        <asp:HiddenField ID="hfid1" runat="server" />
                        <asp:HiddenField ID="hfid2" runat="server" />
                        <asp:HiddenField ID="hfid3" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <asp:ValidationSummary ID="val" runat="server" ShowSummary="false" ShowMessageBox="false" ValidationGroup="Add" />
    </div>
</asp:Content>
