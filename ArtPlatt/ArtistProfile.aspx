﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ArtistProfile.aspx.cs" Inherits="ArtPlatt.ArtistProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container ws-page-container" style="padding: 5px!important">
        <!-- Cart Content -->
        <div class="col-sm-7">

            <!-- Billing Details -->
            <div class="ws-checkout-billing">
                <!-- Form Inputs -->
                <div class="form-inline">
                    <!-- Name -->
                    <div class="ws-checkout-first-row">
                        <div class="col-no-p ws-checkout-input col-sm-12">
                            <div class="ws-instagram-header h3">
                                <h3>
                                    <asp:Label ID="lblName" runat="server" CssClass="ws-instagram-header h3" Text="ArtistName"></asp:Label>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-no-p ws-checkout-input col-sm-12">
                        <%--<div class="ws-item-category">
                            <center><asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label></center>
                        </div>--%>

                        <center> <asp:Label ID="lbladdress" runat="server" Text="Address"></asp:Label></center>
                     
                        <center><asp:Label ID="lblcontact" runat="server" Text="Phone Number"></asp:Label> </center>
                           <div class="ws-item-separator"></div>
                        <br />
                        <p id="pDesc" runat="server"></p>
                        <a class="btn ws-btn-fullwidth" href="#ws-register-modal" id="btncontact" runat="server" data-toggle="modal">Contact </a>
                    </div>
                    <div class="modal fade" id="ws-register-modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                </div>
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="modal-body">
                                            <!-- Register Form -->
                                            <div class="ws-register-form">
                                                <h3>
                                                    <asp:Label runat="server" ID="Label1"></asp:Label></h3>
                                                <div class="ws-separator"></div>
                                                <h4 align="center">You Need to login First!</h4>
                                                <!-- Name -->
                                                <%-- <div class="form-group">
                                                    <label class="control-label">Name <span>*</span></label>
                                                    <input type="text" class="form-control">
                                                </div>

                                                <!-- Email -->
                                                <div class="form-group">
                                                    <label class="control-label">Email Adress <span>*</span></label>
                                                    <input type="email" class="form-control">
                                                </div>

                                                <!-- Password -->
                                                <div class="form-group">
                                                    <label class="control-label">Phone Number <span></span></label>
                                                    <input type="email" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Message <span></span></label>
                                                    <input type="textarea" class="form-control">
                                                </div>--%>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <!-- Button -->
                                            <a class="btn ws-btn-fullwidth" href="Login.aspx">Login</a>
                                            <br>
                                            <br>
                                            <!-- Facebook Button -->

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Cart Total -->
        <div class="col-sm-5">
            <div class="ws-checkout-order" style="background-color: white!important">
                <asp:Image ID="imgurl" runat="server" class="img-responsive" />
            </div>
        </div>
        <div class="ws-checkout-first-row">
            <div class="col-no-p ws-checkout-input col-sm-12">
                <div class="ws-instagram-header h3">
                    <h3 runat="server" id="h3">ARTWORKS BY THIS ARTIST
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <asp:Repeater ID="rptCategory" runat="server">
                <ItemTemplate>

                    <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.1s, ease-in 20px'>
                        <a href="ProductDetail.aspx?ArtID=<%#Eval("ArtID") %>">
                            <div class="ws-item-offer">
                                <figure>
                                    <asp:Panel ID="Panel1" runat="server" Visible='<%# Eval("FileType").ToString()=="Viedio"%>'>
                                        <video id="video" src='/Image/Art/<%#Eval("ArtPath") %>' style="padding-top: 65px;" controls="true" class="img-responsive"></video>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel2" runat="server" Visible='<%# Eval("FileType").ToString()=="Image"%>'>
                                        <img src='/Image/Art/<%#Eval("ArtPath") %>' class="img-responsive" />
                                    </asp:Panel>
                                    <asp:Panel ID="Panel3" runat="server" Visible='<%# Eval("FileType").ToString()=="Audio"%>'>
                                        <img src="assets/DefaultImage/Audio.png" style="height: 236px; width: 100%;" />
                                        <audio src='/Image/Art/<%#Eval("ArtPath") %>' style="width: 100%" controls></audio>

                                    </asp:Panel>
                                    <asp:Panel ID="Panel4" runat="server" Visible='<%# Eval("FileType").ToString()=="Pdf"%>'>
                                        <a href='/Image/Art/<%#Eval("ArtPath") %>' target="_blank">
                                            <img src="assets/DefaultImage/Pdf.png" class="img-responsive" /></a>
                                    </asp:Panel>
                                </figure>
                                <asp:Label runat="server" ForeColor="Red" Font-Size="XX-Large" ID="lblResult" Text=""></asp:Label>
                            </div>
                            <div class="ws-works-caption text-center">
                                <div class="ws-item-category">
                                    <%#Eval("Name") %>
                                </div>
                                <div class="ws-item-separator"></div>
                                <h3 class="ws-item-title">
                                    <%#Eval("ArtName") %>
                                    <div class="ws-item-separator"></div>
                                    <div class="ws-item-price">
                                        <%--€ <%#Eval("Price") %>--%>
                                        <%# !String.IsNullOrEmpty(Eval("Price").ToString()) ? "€"+ Eval("Price").ToString() : "" %>
                                    </div>
                            </div>
                        </a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
