﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ArtPlatt
{
    public partial class ProductDetail : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        string Email;
        int SenderID, RecieverID;
        static List<ArtImage> objArtList = new List<ArtImage>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["ArtID"] != null)
                {
                    int ArtID = Convert.ToInt32(Request.QueryString["ArtID"]);
                    getProduct(ArtID);
                }
            }
        }

        private void getProduct(int ArtId)
        {

            SqlCommand cmd = new SqlCommand("Select * from ImageMaster where ArtID='" + ArtId + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            con.Close();
            da.Fill(ds);
            rptPrv.DataSource = ds;
            rptPrv.DataBind();

            SqlCommand cm = new SqlCommand("Select r.RegID,r.FirstName,r.EmailId, r.LastName,c.CategoryName,S.Name, a.ArtName,a.Description,a.SubCategoryId, a.Price from RegistrationMaster as r inner join ArtMaster as a on a.RegID = r.RegId inner join SubCategoryMaster as s on a.SubCategoryId = s.SubCategoryId inner join CategoryMaster as c on s.CategoryId = c.CategoryId   where a.ArtID='" + ArtId + "'", con);
            DataTable dt = new DataTable();
            con.Open();
            SqlDataAdapter SDA = new SqlDataAdapter(cm);
            SDA.Fill(dt);
            RecieverID = Convert.ToInt32(dt.Rows[0]["RegID"]);
            lblArtistName.Text = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
            lblName.Text = "Contact" + " " + dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
            lblartName.Text = dt.Rows[0]["ArtName"].ToString();
            artName.Text = dt.Rows[0]["ArtName"].ToString();
            if (!string.IsNullOrEmpty(dt.Rows[0]["Price"].ToString()))
                lblPrice.Text = "€" + dt.Rows[0]["Price"].ToString();
            lbldesc.Text = dt.Rows[0]["Description"].ToString();

            artName.Text = dt.Rows[0]["ArtName"].ToString();
            lblcategory.Text = dt.Rows[0]["CategoryName"].ToString();
            lblsub.Text = dt.Rows[0]["Name"].ToString();
            string sid = dt.Rows[0]["SubCategoryId"].ToString();
            hfArtist.Value = dt.Rows[0]["RegId"].ToString();

            SqlCommand pro = new SqlCommand("sp_GetAllCategory", con);
            pro.CommandType = CommandType.StoredProcedure;
            pro.Parameters.AddWithValue("@Action", "RelatedProduct");
            pro.Parameters.AddWithValue("@ArtID", ArtId);
            pro.Parameters.AddWithValue("@sid", sid);
            SqlDataAdapter ad = new SqlDataAdapter(pro);
            DataSet dsCategories = new DataSet();
            ad.Fill(dsCategories);
            objArtList.Clear();
            con.Close();
            if (Session["Email"] != null)
                Email = Convert.ToString(Session["Email"]);
            else if (Session["UserEmail"] != null)
                Email = Convert.ToString(Session["UserEmail"]);

            if (Email != null)
            {
                SqlCommand id = new SqlCommand("select RegID from RegistrationMaster where EmailId='" + Email + "'", con);
                con.Open();
                SenderID = Convert.ToInt32(id.ExecuteScalar());
                btncontact.Attributes["href"] = "MessageChat.aspx?SenderId=" + SenderID + "&RecieverID=" + RecieverID + "";
                if (Email == dt.Rows[0]["EmailID"].ToString())
                {
                    btncontact.Visible = false;
                }

            }

            if (dsCategories.Tables.Count > 0)
            {
                if (dsCategories.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsCategories.Tables[0].Rows)
                    {
                        ArtImage objArt = new ArtImage();
                        objArt.ArtID = Convert.ToInt16(dr["ArtID"]);
                        objArt.SubCategoryId = Convert.ToInt16(dr["SubCategoryID"]);
                        objArt.ArtName = dr["ArtName"].ToString();
                        objArt.CategoryName = dr["CategoryName"].ToString();
                        objArt.ArtPath = dr["ArtPath"].ToString();
                        objArt.Name = dr["Name"].ToString();
                        objArt.FileType = dr["FileType"].ToString();
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["Price"])))
                            objArt.Price = "€" + " " + Convert.ToDecimal(dr["Price"]);
                        if (!string.IsNullOrEmpty(dr["FirstName"].ToString()) && !string.IsNullOrEmpty(dr["LastName"].ToString()))
                            objArt.ArtistName = dr["FirstName"].ToString() + dr["LastName"].ToString();
                        else
                            objArt.ArtistName = dr["CompanyName"].ToString();
                        objArt.ArtistId = Convert.ToInt32(dr["RegID"]);
                        objArtList.Add(objArt);
                    }
                }
            }
            if (dsCategories.Tables.Count > 0)
            {
                if (dsCategories.Tables[0].Rows.Count == 0)
                {
                    h3.Visible = false;
                }
                else
                {
                    rptArt.DataSource = objArtList;
                    rptArt.DataBind();
                }
            }



        }

        protected void lblArtistName_Click(object sender, EventArgs e)
        {
            Response.Redirect("ArtistProfile.aspx?ArtistID=" + hfArtist.Value + "");

        }

        protected void imgPdf_Click(object sender, EventArgs e)
        {

        }
        public class ArtImage
        {
            public string CategoryName { get; set; }
            public short ArtID { get; set; }
            public string ArtName { get; set; }
            public string ArtPath { get; set; }
            public string Price { get; set; }
            public short SubCategoryId { get; set; }
            public string Name { get; set; }
            public string FileType { get; set; }
            public int ArtistId { get; set; }
            public string ArtistName { get; set; }
        }
    }
}