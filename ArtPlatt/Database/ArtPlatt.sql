USE [artpatt]
GO
/****** Object:  User [artpatt]    Script Date: 10/23/2017 11:22:20 AM ******/
CREATE USER [artpatt] FOR LOGIN [artpatt] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [gd_execprocs]    Script Date: 10/23/2017 11:22:21 AM ******/
CREATE ROLE [gd_execprocs]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [artpatt]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [artpatt]
GO
ALTER ROLE [db_datareader] ADD MEMBER [artpatt]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [artpatt]
GO
/****** Object:  FullTextCatalog [artpatt]    Script Date: 10/23/2017 11:22:21 AM ******/
CREATE FULLTEXT CATALOG [artpatt]WITH ACCENT_SENSITIVITY = ON
AS DEFAULT

GO
/****** Object:  StoredProcedure [dbo].[sp_adminLogin]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  procedure [dbo].[sp_adminLogin]
(
@Usename Varchar (20),
@word varchar (10)
)
as
Begin
Select COUNT(*)from AdminMaster where EmailID=@Usename and Password=@word 
End

GO
/****** Object:  StoredProcedure [dbo].[sp_artimage]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_artimage]
@ArtID int=null,
@ArtPath varchar(250)=null,
@ImageID int=null,
@Action varchar(20)=null
AS
BEGIN
 if (@Action = 'select') 
	begin	
		select a.ArtID,a.ArtName,a.Description,a.Price,b.ArtPath,c.Name,d.CategoryName,b.ImageID from ArtMaster as a 
		inner join ImageMaster as b on a.ArtID=b.ArtID
		inner join SubCategoryMaster as c on a.SubCategoryId=c.SubCategoryID
		inner join CategoryMaster as d on c.CategoryID=d.CategoryID
	End
	if (@Action = 'Insert') 
	begin	
		insert into ImageMaster(ArtPath,ArtId)values(@ArtPath,@ArtID)
	End
	if (@Action = 'Delete') 
	begin	
		delete from ImageMaster where ImageID=@ArtID
	End
	if (@Action = 'DeleteArt') 
	begin	
		delete from ArtMaster where ArtID=@ArtID
	End
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ArtistProfile]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ArtistProfile]
(  
@RegId int = null,   
@Action varchar(100)= null  
)
 As begin 
 if @Action = 'Select' 
	select * from RegistrationMaster where UserType='1'
if @Action = 'selected'  
	select * from RegistrationMaster as r
	inner join CategoryMaster as c on r.CategoryID=c.CategoryID
	where RegId=@RegId
End

GO
/****** Object:  StoredProcedure [dbo].[sp_Category]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Category]
	@CatID int= null,  
	@CategoryName varchar(20)=null,  
	@CategoryImage varchar(20)=null,  
	@Action varchar(20)=null
AS
BEGIN
 if (@Action = 'Insert') 
	begin	
		insert into CategoryMaster(CategoryName,CategoryImage) values(@CategoryName,@CategoryImage)
	End
if (@Action = 'Update')  
	begin
		Update CategoryMaster set CategoryName=@CategoryName,CategoryImage=@CategoryImage where CategoryID=@CatID
	End
 if (@Action = 'Delete')
	begin
		Delete from CategoryMaster where CategoryID=@CatID
	End
 if (@Action = 'Select')
	begin
		 select * from CategoryMaster
	End
END


GO
/****** Object:  StoredProcedure [dbo].[sp_change_pwd]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[sp_change_pwd]
(             
  @username VARCHAR(100),
  @old_pwd VARCHAR(50),
  @new_pwd VARCHAR(50),
  @status int OUTPUT,
  @Action varchar(20)=null
)
AS
BEGIN 
  if (@Action = 'Admin') 
	begin	              
		IF EXISTS(SELECT * FROM AdminMaster WHERE EmailID =@username AND Password =@old_pwd)
			BEGIN
				UPDATE AdminMaster SET [Password]=@new_pwd WHERE EmailID=@username
			SET @status=1
			END
		ELSE                    
			BEGIN 
				SET @status=0
			END    
	End 
	if (@Action = 'User') 
	begin	
		IF EXISTS(SELECT * FROM RegistrationMaster WHERE EmailID =@username AND Password =@old_pwd)
			BEGIN
				UPDATE RegistrationMaster SET [Password]=@new_pwd WHERE EmailID=@username
			SET @status=1
			END
		ELSE                    
			BEGIN 
				SET @status=0
			END    

	End
END
RETURN @status 

GO
/****** Object:  StoredProcedure [dbo].[sp_Contactus]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[sp_Contactus]
(  
@ContactUsID int = null,   
@Name varchar(30)= null,  
@EmailID varchar(50)= null,  
@Message varchar(20)= null,  
@Action varchar(100)= null  

)
 As begin 
 Delete from ContactUs where ContactUsID=@ContactUsID
End

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllCategory]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetAllCategory]
	@CatID int= null,
	@ArtID int= null,
	@RegId int=null,
	@SID int= null,
	@Search varchar(20)=null,
	@Action varchar(20)=null
AS
BEGIN
 if (@Action = 'Select') 
	begin	
		select  DISTINCT i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath,
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType,
		 a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName from ArtMaster  as a inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName

	End
if (@Action = 'ByCategory')  
	begin
		select  DISTINCT i.ArtID,(select top 1 (ArtPath) from ImageMaster where ArtId=i.ArtID) as ArtPath,
			(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType, a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName
		from ArtMaster  as a
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID=a.RegID
				where c.CategoryId=@CatID
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName
	End

if (@Action = 'BySearch')  
	begin
		select  DISTINCT i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath, 
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName 
		from ArtMaster  as a
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID=a.RegID
		where c.CategoryName=@Search or r.firstName=@Search or a.ArtName=@Search 
				or s.Name=@Search or r.LastName=@Search
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName 
	End
	if (@Action = 'RelatedProduct')  
	begin
		select  DISTINCT top 3 i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath, 
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType,
		a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName  from ArtMaster  as a 
		
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID = a.RegID 
		where a.SubCategoryId = @sid and a.ArtId != @ArtID
		Group By i.ArtID, a.ArtName, a.Price, s.SubCategoryID, s.Name, c.CategoryName, r.FirstName,i.FileType
		 Order By i.ArtID desc
	End
	if (@Action = 'ArtistProduct')  
	begin
		select  DISTINCT i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath,
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType,
		 a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName 
		from ArtMaster  as a
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID=a.RegID
		where r.RegID=@RegID
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName 
	End
		if (@Action = 'TopProduct')  
	begin
		select  DISTINCT top 6 i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath,
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType, a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName
		from ArtMaster  as a
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID=a.RegID
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName
		 Order By i.ArtID desc
		
	End
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Location]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Location]
@LocID int=null,
@LocLat varchar(50)=null,
@LocLong varchar(50)=null,
@RegID int=null,
@Action varchar(20)= null

AS
BEGIN
 if (@Action = 'select') 
	begin	
	select distinct  r.RegId,l.LocLat,l.LocLong,r.FirstName,r.LastName,r.PhotoPath,r.CompanyName
		 from locationMaster as l
		inner join RegistrationMaster as r on l.RegID=r.RegId
	End
	if (@Action = 'Insert') 
	begin	
		insert into locationMaster values(@LocLat,@LocLong,@RegID)
	End
	if (@Action = 'Update') 
	begin	
		update locationMaster set LocLat=@LocLat,LocLong=@LocLong where RegID=@RegID
	End
END

GO
/****** Object:  StoredProcedure [dbo].[sp_login]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_login]
	@RegID int=null,
	@UserType varchar(20)=null,
	@EmailId varchar(50)=null,
	@Password varchar(50)=null,
	@Action varchar(20)=null,
	@status bit=null
AS
BEGIN
	if (@Action = 'login') 
	begin
		select count(*) from RegistrationMaster where EmailId=@EmailId and Password=@Password
	End
	 if (@Action = 'checkstatus') 
	begin	
		select status from RegistrationMaster where EmailId=@EmailId and Password=@Password
	End
	 if (@Action = 'StatusUpdate') 
	begin	
		update RegistrationMaster set 
		Status=@status where RegID=@RegID
	End
END


GO
/****** Object:  StoredProcedure [dbo].[sp_Message]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Message]
	@SenderID int=null,
	@RecieverID varchar(20)=null,
	@Message varchar(max)=null,
	@TimeStamp varchar(50)=null,
	@Action varchar(20)=null
AS
BEGIN
	if (@Action = 'Insert') 
	begin
	insert into Message(SenderID,RecieverID,Message,TimeStamp) values(@SenderID,@RecieverID,@Message,@TimeStamp) 
			End	
	if (@Action = 'Select') 
	begin
	select convert(char(5), TimeStamp, 108) [TimeStamp],SenderID,RecieverID,Message from Message 
		where (SenderID=@SenderID AND RecieverID=@RecieverID)
		OR (SenderID=@RecieverID AND RecieverID=@SenderID)
	End	
	if (@Action = 'SelectedMessage') 
	begin
	select SenderID,RecieverId,(SELECT TOP 1 message FROM message   where SenderID=m.SenderID or RecieverID=m.RecieverID  ORDER BY message DESC) as message,
	(select TOP 1 convert(char(5), TimeStamp, 108) [TimeStamp]  FROM message where SenderID=m.SenderID ORDER BY TimeStamp DESC ) as Time,
	FirstName,LastName,PhotoPath
	from Message as m
	inner join RegistrationMaster as r on r.RegId=m.RecieverID
	where m.SenderID=@RecieverID
		group by SenderID,RecieverId,FirstName,LastName,PhotoPath
	End	
END


GO
/****** Object:  StoredProcedure [dbo].[sp_Registration]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Registration]
	@RegID int=null,
	@FirstName varchar(20)=null,
	@LastName varchar(20)=null,
	@ContactNo varchar(20)=null,
	@PhotoPath varchar(max)=null,
	@UserType varchar(20)=null,
	@CompanyName varchar(20)=null,
	@EmailId varchar(50)=null,
	@Password varchar(50)=null,
	@Address varchar(60)=null,
	@CategoryID int=null,
	@Description varchar(250)=null,
	@SubCategoryID int=null,
	@Action varchar(20)=null,
	@status bit=null
AS
BEGIN
	if (@Action = 'Insert') 
	begin	
		INSERT INTO RegistrationMaster Values(@FirstName,@LastName,@ContactNo,@PhotoPath,@UserType,@CompanyName,@EmailId,@Password,@Address,@CategoryID,@Description,@status,@SubCategoryID )
	End
	 if (@Action = 'Update') 
	begin	
		Update RegistrationMaster set
		 FirstName = @FirstName,LastName = @LastName , ContactNo = @ContactNo,
		 PhotoPath = @PhotoPath, CompanyName = @CompanyName,
		 Address = @Address, CategoryID = @CategoryID, Description = @Description,SubCategoryID=@SubCategoryID  
		 where RegID=@RegID
	End
	 if (@Action = 'StatusUpdate') 
	begin	
		update RegistrationMaster set 
		Status=@status where RegID=@RegID
	End
	 if (@Action = 'Delete') 
	begin	
		Delete from  RegistrationMaster  where RegID=@RegID
	End
	if(@Action='SelectedByEmail')
	begin
	select * from RegistrationMaster where EmailId=@EmailId
	end
	if(@Action='SelectedById')
	begin
	select * from RegistrationMaster where RegId=@RegID
	end
END


GO
/****** Object:  StoredProcedure [dbo].[sp_Subcategory]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Subcategory]
	@SubcatID int= null,  
	@CategoryID varchar(20)=null,  
	@SubCategoryName varchar(20)=null,  
	@Action varchar(20)=null
AS
BEGIN
 if (@Action = 'Insert') 
	begin	
		insert into SubCategoryMaster(Name,CategoryId) values(@SubCategoryName,@CategoryID)
	End
if (@Action = 'Update')  
	begin
		Update SubCategoryMaster set @SubCategoryName=@SubCategoryName,CategoryId=@CategoryID where SubCategoryID=@SubcatID
	End
 if (@Action = 'Delete')
	begin
		Delete from SubCategoryMaster where SubCategoryID=@SubcatID
	End
 if (@Action = 'Select')
	begin
		select * from SubCategoryMaster as s
		 inner join CategoryMaster as c on s.CategoryId=c.CategoryID 
	End
END


GO
/****** Object:  Table [dbo].[AdminMaster]    Script Date: 10/23/2017 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminMaster](
	[AdminID] [int] NOT NULL,
	[EmailID] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AdminMaster] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ArtMaster]    Script Date: 10/23/2017 11:22:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ArtMaster](
	[ArtID] [bigint] IDENTITY(1,1) NOT NULL,
	[ArtName] [varchar](50) NOT NULL,
	[Price] [varchar](50) NULL,
	[SubCategoryId] [bigint] NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[RegID] [bigint] NULL,
 CONSTRAINT [PK_ArtMaster] PRIMARY KEY CLUSTERED 
(
	[ArtID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 10/23/2017 11:22:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[CategoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
	[CategoryImage] [varchar](max) NOT NULL,
 CONSTRAINT [PK_CategoryMaster] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactUs]    Script Date: 10/23/2017 11:22:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactUs](
	[ContactUsID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[EmailID] [varchar](50) NOT NULL,
	[Message] [varchar](250) NOT NULL,
 CONSTRAINT [PK_ContactUs] PRIMARY KEY CLUSTERED 
(
	[ContactUsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ImageMaster]    Script Date: 10/23/2017 11:22:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImageMaster](
	[ImageID] [bigint] IDENTITY(1,1) NOT NULL,
	[ArtPath] [varchar](max) NOT NULL,
	[ArtID] [bigint] NOT NULL,
	[FileType] [varchar](50) NULL,
 CONSTRAINT [PK_ImageMaster] PRIMARY KEY CLUSTERED 
(
	[ImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LocationMaster]    Script Date: 10/23/2017 11:22:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LocationMaster](
	[LocationID] [bigint] IDENTITY(1,1) NOT NULL,
	[LocLat] [varchar](50) NOT NULL,
	[LocLong] [varchar](50) NOT NULL,
	[RegID] [bigint] NOT NULL,
 CONSTRAINT [PK_LocationMaster] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Message]    Script Date: 10/23/2017 11:22:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Message](
	[MessageID] [int] IDENTITY(1,1) NOT NULL,
	[SenderID] [int] NOT NULL,
	[RecieverID] [int] NULL,
	[Message] [varchar](max) NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegistrationMaster]    Script Date: 10/23/2017 11:22:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegistrationMaster](
	[RegID] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](30) NOT NULL,
	[LastName] [varchar](30) NOT NULL,
	[ContactNo] [varchar](18) NULL,
	[PhotoPath] [varchar](max) NULL,
	[UserType] [int] NOT NULL,
	[CompanyName] [varchar](30) NULL,
	[EmailId] [varchar](255) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[CategoryID] [bigint] NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[Status] [bit] NULL,
	[SubCategoryID] [bigint] NULL,
 CONSTRAINT [PK_RegistrationMaster] PRIMARY KEY CLUSTERED 
(
	[RegID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubCategoryMaster]    Script Date: 10/23/2017 11:22:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubCategoryMaster](
	[SubCategoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CategoryId] [bigint] NOT NULL,
 CONSTRAINT [PK_SubCategoryMaster] PRIMARY KEY CLUSTERED 
(
	[SubCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Message] ADD  CONSTRAINT [DF_Message_TimeStamp]  DEFAULT (getdate()) FOR [TimeStamp]
GO
ALTER TABLE [dbo].[ArtMaster]  WITH CHECK ADD  CONSTRAINT [FK_ArtMaster_RegistrationMaster] FOREIGN KEY([RegID])
REFERENCES [dbo].[RegistrationMaster] ([RegID])
GO
ALTER TABLE [dbo].[ArtMaster] CHECK CONSTRAINT [FK_ArtMaster_RegistrationMaster]
GO
ALTER TABLE [dbo].[ImageMaster]  WITH CHECK ADD  CONSTRAINT [FK_ImageMaster_ArtMaster] FOREIGN KEY([ArtID])
REFERENCES [dbo].[ArtMaster] ([ArtID])
GO
ALTER TABLE [dbo].[ImageMaster] CHECK CONSTRAINT [FK_ImageMaster_ArtMaster]
GO
ALTER TABLE [dbo].[SubCategoryMaster]  WITH CHECK ADD  CONSTRAINT [FK_SubCategoryMaster_CategoryMaster] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[CategoryMaster] ([CategoryID])
GO
ALTER TABLE [dbo].[SubCategoryMaster] CHECK CONSTRAINT [FK_SubCategoryMaster_CategoryMaster]
GO
