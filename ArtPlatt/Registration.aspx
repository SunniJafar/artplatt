﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="ArtPlatt.Registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/jquery-3.2.1.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFt_fAkCV_pURC2Q7qppS-SYO4bhPll2A&sensor=false&libraries=places"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                var opts = {
                    types: ['(cities)']
                };
                document.getElementById("<%=hfLat.ClientID %>").value = latitude;
                document.getElementById("<%=hfLon.ClientID %>").value = longitude;
            });
        });

        $(document).ready(function () {
            show($('#ContentPlaceHolder1_rbltype').val());
        });

        function show(select_item) {
            if (select_item == "1") {
                hiddenDiv.style.visibility = 'visible';
                hiddenDiv.style.display = 'block';

                hiddenNameDiv.style.display = "none";
                hiddenNameDiv.visibility = "hidden";

                hiddenCategory.visibility = "visible";
                hiddenCategory.style.display = "block";

                divDescription.style.display = "block";
                divDescription.visibility = "visible";

                ValidatorEnable(document.getElementById('<%= rfvDesc.ClientID %>'), true);
                ValidatorEnable(document.getElementById('<%= reqfname.ClientID %>'), false);
                ValidatorEnable(document.getElementById('<%= rfvtxtlname.ClientID %>'), false);
            }
            else if (select_item == "3") {

                ValidatorEnable(document.getElementById('<%= rfvDesc.ClientID %>'), false);
                ValidatorEnable(document.getElementById('<%= rfvCategory.ClientID %>'), false);
                hiddenDiv.style.visibility = 'hidden';
                hiddenDiv.style.display = 'none';

                divDescription.style.display = "None";
                divDescription.visibility = "hidden";

                hiddenNameDiv.style.display = "block";
                hiddenNameDiv.visibility = "visible";

                hiddenCategory.visibility = "hidden";
                hiddenCategory.style.display = "none";

            }
            else {
                ValidatorEnable(document.getElementById('<%= rfvDesc.ClientID %>'), false);
                hiddenCategory.visibility = "visible";
                hiddenCategory.style.display = "block";

                divDescription.style.display = "block";
                divDescription.visibility = "visible";

                hiddenDiv.style.visibility = 'hidden';
                hiddenDiv.style.display = 'none';

                hiddenNameDiv.style.display = "block";
                hiddenNameDiv.visibility = "visible";
            }
        }
        function ShowImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgprv.ClientID%>').prop('src', e.target.result)
                        ;
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function ValidateCheckBox(sender, args) {
            if (document.getElementById("<%=chkTerm.ClientID %>").checked == true) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }
    </script>
    <div class="ws-parallax-header parallax-window" runat="server" id="giv" data-parallax="scroll" data-image-src="assets/img/backgrounds/shop-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1 visible="true" id="h1" runat="server">Registration </h1>
                    <asp:Image ID="imgprv" Visible="false" runat="server" CssClass="ProfileImage" ImageUrl="~/assets/DefaultImage/hello.png" />
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->
    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <!-- Coupon -->

            <div class="ws-checkout-content clearfix">

                <!-- Cart Content -->
                <div class="col-md-8 col-md-offset-2">

                    <!-- Billing Details -->
                    <div class="ws-checkout-billing">

                        <!-- Form Inputs -->
                        <div class="form-inline">
                            <center><asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></center>

                            <asp:Label ID="lblWelcome" runat="server" Style="padding: 0 0 7px 0; margin: 0; font-size: 21px; text-transform: uppercase; letter-spacing: 1.5px;"
                                Text="Welcome" Font-Size="X-Large" Visible="false"></asp:Label><br />
                            <br />
                            <asp:UpdatePanel ID="upSetSession" runat="server">
                                <ContentTemplate>
                                    <!-- Name -->
                                    <div class="ws-checkout-first-row">
                                        <div class="col-no-p ws-checkout-input col-sm-12">
                                            <div class="col-no-p ws-checkout-input col-sm-6">

                                                <label>Registration Type<span>* </span></label>

                                                <asp:DropDownList ID="rbltype" runat="server" RepeatDirection="Horizontal" RepeatColumns="2" CellPadding="20" onchange="java_script_:show(this.options[this.selectedIndex].value)">
                                                    <asp:ListItem Value="0" Text="Artist"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="User"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Art Place"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="hfLat" runat="server" />
                                    <asp:HiddenField ID="hfLon" runat="server" />
                                    <asp:HiddenField ID="photo" runat="server" />
                                    <div id="hiddenNameDiv" style='display: block;'>
                                        <div class="ws-checkout-first-row">
                                            <div class="col-no-p ws-checkout-input col-sm-6">
                                                <label>First Name <span>* </span></label>
                                                <br>
                                                <asp:TextBox ID="txtfname" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqfname" runat="server" ControlToValidate="txtfname" CssClass="required" ValidationGroup="Insert" ErrorMessage="Enter First Name"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender runat="server" ID="rfvtxtfname" TargetControlID="txtfname" ValidChars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ." Enabled="true"></cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="col-no-p ws-checkout-input col-sm-6">
                                                <label>Last Name <span>* </span></label>
                                                <br />
                                                <asp:TextBox ID="txtlname" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvtxtlname" CssClass="required" runat="server" ErrorMessage="Enter Last Name" ControlToValidate="txtLName" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender1" TargetControlID="txtlname" ValidChars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ." Enabled="true"></cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="hiddenCategory" style="display: block;">
                                        <div class="ws-checkout-first-row">
                                            <div class="col-no-p ws-checkout-input col-sm-6">
                                                <label>
                                                    Category <span>* </span>
                                                </label>
                                                <asp:DropDownList ID="drdCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drdCategory_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator runat="server" ID="rfvCategory" ErrorMessage="Select Category" CssClass="required" ControlToValidate="drdCategory" ValidationGroup="Insert" InitialValue="0"></asp:RequiredFieldValidator>

                                            </div>
                                            <div class="col-no-p ws-checkout-input col-sm-6">
                                                <label>
                                                    Subcategory <span>* </span>
                                                </label>
                                                <asp:DropDownList ID="drdsubcategory" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="Select subcategory" CssClass="required" ControlToValidate="drdsubcategory" ValidationGroup="Insert" InitialValue="0"></asp:RequiredFieldValidator>

                                            </div>


                                        </div>
                                    </div>
                                    <!-- Email -->
                                    <asp:Panel ID="pnlemail" runat="server">
                                        <div class="ws-checkout-first-row">
                                            <div class="col-no-p ws-checkout-input col-sm-6">
                                                <label>Email Address <span>* </span></label>
                                                <br>
                                                <asp:TextBox ID="txtemail" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Email Address" CssClass="required" ControlToValidate="txtemail" ValidationGroup="Insert" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtemail"
                                                    ErrorMessage="Enter Valid Email Address" CssClass="required" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ValidationGroup="Insert" Display="Dynamic">
                                                </asp:RegularExpressionValidator>
                                            </div>

                                            <div class="col-no-p ws-checkout-input col-sm-6">
                                                <label>Password <span>* </span></label>
                                                <br />
                                                <asp:TextBox ID="txtpass" runat="server" TextMode="Password" AutoCompleteType="Disabled"></asp:TextBox>
                                                <asp:RequiredFieldValidator runat="server" ID="rfvpassword" ErrorMessage="Enter password" CssClass="required" ControlToValidate="txtpass" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <!-- Town -->

                                    <!-- State -->
                                    <%--  <div class="ws-checkout-first-row">
                                        <div class="col-no-p ws-checkout-input col-sm-6">
                                            <label>State / Country <span>* </span></label>
                                            <br>
                                            <asp:TextBox ID="txtstate" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter State / Country" ForeColor="Red" ControlToValidate="txtstate" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtstate"
                                                ValidChars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'" Enabled="True"></cc1:FilteredTextBoxExtender>
                                        </div>

                                        <div class="col-no-p ws-checkout-input col-sm-6">
                                            <label>Postcode / ZIP <span>* </span></label>
                                            <br>
                                            <asp:TextBox ID="txtzip" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ErrorMessage="Enter Postcode / ZIP" ForeColor="Red" ControlToValidate="txtzip" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtzip"
                                                FilterType="Numbers" ValidChars="[1,2,3,4,5,6,7,8,9,0]" runat="server"></cc1:FilteredTextBoxExtender>
                                        </div>
                                    </div>--%>

                                    <div class="ws-checkout-first-row">
                                        <div class="col-no-p ws-checkout-info col-sm-12">
                                            <label>
                                                Profile Photo (Size: 200px* 200px)
                                            </label>
                                            <asp:FileUpload ID="fuPhoto" runat="server" onchange="ShowImagePreview(this);" CssClass="file-caption"></asp:FileUpload>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.gif)$"
                                                ControlToValidate="fuPhoto" runat="server" ForeColor="Red" ValidationGroup="Add" ErrorMessage="Please select a valid Image File"
                                                Display="Dynamic" />
                                        </div>
                                    </div>
                                    <div id="hiddenDiv" style='display: none;'>
                                        <div class="ws-checkout-first-row">
                                            <div class="col-no-p ws-checkout-info col-sm-12">
                                                <label>Company Name</label>
                                                <br>
                                                <asp:TextBox ID="txtcomapnyname" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="divDescription" class="ws-checkout-first-row">
                                        <div class="col-no-p ws-contact-input col-sm-12">
                                            <label>
                                                Description <span>* </span>
                                            </label>
                                            <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvDesc" ErrorMessage="Enter Description" Display="Dynamic" CssClass="required" ControlToValidate="txtDesc" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="rbltype" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="ws-checkout-first-row">
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Phone Number</label>
                                    <br />
                                    <asp:TextBox ID="txtcontact" runat="server"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" TargetControlID="txtcontact"
                                        FilterType="Numbers" ValidChars="[1,2,3,4,5,6,7,8,9,0]" runat="server"></cc1:FilteredTextBoxExtender>
                                </div>

                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Address<span>* </span></label>
                                    <br />
                                    <asp:TextBox ID="txtPlaces" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Address" CssClass="required" ControlToValidate="txtPlaces" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <!-- Order Notes -->
                        </div>

                    </div>
                    <asp:Panel ID="pnlterm" runat="server">
                        <div class="col-sm-12 col-sm-offset-4">
                            <div class="checkbox" style="padding-bottom: 20px;">
                                <div class="ws-forgot-pass">
                                    <asp:CheckBox CssClass="Check" Text="I agree to" ID="chkTerm" runat="server" OnClick="updateValidator();" /><a style="font-style: normal;" href="#ws-register-modal" data-toggle="modal"> Terms of Conditions</a>
                                    <br />
                                    <asp:CustomValidator ID="custumTerm" runat="server" ErrorMessage="Accept Terms of Conditions" CssClass="required" ClientValidationFunction="ValidateCheckBox" ValidationGroup="Insert" Display="Dynamic"></asp:CustomValidator>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Button ID="btnsubmit" runat="server" ValidationGroup="Insert" OnClick="btnsubmit_Click" Text="Register" Font-Italic="false" class="btn ws-btn-fullwidth" Style="width: 100%; background-color: #C2A476; color: #fff;" />
                </div>
                <div class="modal fade" id="ws-register-modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            </div>

                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="modal-body">
                                        <div class="ws-register-form">
                                            <h3>Terms of Conditions</h3>
                                            <div class="ws-separator"></div>
                                            <!-- Name -->
                                            <div class="form-group">
                                            </div>

                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <%--<asp:Button ID="btnforget" runat="server" class="btn ws-btn-fullwidth" OnClick="btnforget_Click" Text="Send password" Style="width: 100%; background-color: #C2A476; color: #fff;"></asp:Button>--%>
                                        <br />
                                        <b></b>


                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <asp:ValidationSummary ID="vali" runat="server" ValidationGroup="Insert" ShowMessageBox="false" ShowSummary="false" />
                <!-- Cart Total -->
            </div>
        </div>
    </div>
</asp:Content>
