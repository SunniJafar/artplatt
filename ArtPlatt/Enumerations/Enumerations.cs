﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtPlatt.Enumerations
{
    public enum Role
    {
        Artist=0,
        ArtPlace = 1,
        User = 3
    }
}