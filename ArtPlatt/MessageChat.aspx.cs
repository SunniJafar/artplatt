﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using ArtPlatt.BusinessModel;
using System.Web.UI.HtmlControls;

namespace ArtPlatt
{
    public partial class Mesage : System.Web.UI.Page
    {
        int SenderID;
        int RecieverID;
        int RegId;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        DataSet dsMessage = new DataSet();
        static List<MessageModel> objMessageList = new List<MessageModel>();
        String dt = System.DateTime.Now.ToString();
        private System.Timers.Timer aTimer;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Email"] != null || Session["UserEmail"] != null)
                {
                    if (Request.QueryString["SenderID"] != null && Request.QueryString["RecieverID"] != null)
                    {
                        SenderID = Convert.ToInt32(Request.QueryString["SenderID"]);
                        RecieverID = Convert.ToInt32(Request.QueryString["RecieverID"]);
                        GetOldMessage(SenderID, RecieverID);
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        private void GetOldMessage(int SenderID, int RecieverID)
        {
            string Email;
            if (Session["Email"] != null)
                Email = Convert.ToString(Session["Email"]);
            else
                Email = Convert.ToString(Session["UserEmail"]);
            SqlCommand id = new SqlCommand("sp_Registration", con);
            id.CommandType = CommandType.StoredProcedure;
            id.Parameters.AddWithValue("@Action", "SelectedByEmail");
            id.Parameters.AddWithValue("@EmailId", Email);
            SqlDataAdapter sda = new SqlDataAdapter(id);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            RegId = Convert.ToInt32(dt.Rows[0]["RegId"].ToString());
            //SqlCommand cm1 = new SqlCommand("select * from ");
            //lblName.Text = Convert.ToString(dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());

            SqlCommand name = new SqlCommand("select * from RegistrationMaster where RegId='" + RecieverID + "'", con);
            //name.CommandType = CommandType.StoredProcedure;
            //name.Parameters.AddWithValue("@Action", "SelectedById");
            //name.Parameters.AddWithValue("@RegID", RecieverID);
            SqlDataAdapter ads = new SqlDataAdapter(name);
            DataTable td = new DataTable();
            ads.Fill(td);
            lblName.Text = Convert.ToString(td.Rows[0]["FirstName"].ToString() + " " + td.Rows[0]["LastName"].ToString());

            objMessageList.Clear();
            SqlCommand cmd = new SqlCommand("sp_Message", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@SenderID", RecieverID);
            cmd.Parameters.AddWithValue("@RecieverID", SenderID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            con.Close();
            da.Fill(dsMessage);
            if (dsMessage.Tables.Count > 0)
            {
                if (dsMessage.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsMessage.Tables[0].Rows)
                    {
                        MessageModel message = new MessageModel();
                        message.SenderID = Convert.ToInt32(dr["SenderID"]);
                        message.RecieverID = Convert.ToString(dr["RecieverID"]);
                        message.Message = Convert.ToString(dr["Message"]);
                        message.TimeStamp = Convert.ToString(dr["TimeStamp"].ToString());
                        objMessageList.Add(message);
                    }
                }
            }
            rpt.DataSource = objMessageList;
            rpt.DataBind();
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            int SenderID = Convert.ToInt32(((HiddenField)e.Item.FindControl("hfd")).Value);
            if (SenderID == RegId)
            {
                ((HtmlGenericControl)(e.Item.FindControl("maindiv"))).
                     Attributes["class"] = "col-sm-6 col-sm-offset-6 pb15";

                ((HtmlGenericControl)(e.Item.FindControl("divselect"))).
                      Attributes["class"] = "white-box2 selectbg";
            }

        }

        protected void btnSendMsg_Click(object sender, EventArgs e)
        {
            if (txtMessage.InnerText != string.Empty)
            {
                if (Request.QueryString["SenderID"] != null && Request.QueryString["RecieverID"] != null)
                {
                    SenderID = Convert.ToInt32(Request.QueryString["SenderID"]);
                    RecieverID = Convert.ToInt32(Request.QueryString["RecieverID"]);
                    SqlCommand cmd = new SqlCommand("sp_Message", con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SenderID", SenderID);
                    cmd.Parameters.AddWithValue("@RecieverID", RecieverID);
                    cmd.Parameters.AddWithValue("@Message", txtMessage.InnerText);
                    cmd.Parameters.AddWithValue("@TimeStamp", Convert.ToDateTime(dt));
                    cmd.Parameters.AddWithValue("@Action", "Insert");
                    cmd.ExecuteNonQuery();
                    con.Close();
                    GetOldMessage(SenderID, RecieverID);
                    txtMessage.InnerText = string.Empty;
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "getData()", true);
                }
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public void OldMessage(int SenderID, int RecieverID)
        {
            string Email;
            if (Session["Email"] != null)
                Email = Convert.ToString(Session["Email"]);
            else
                Email = Convert.ToString(Session["UserEmail"]);
            SqlCommand id = new SqlCommand("sp_Registration", con);
            id.CommandType = CommandType.StoredProcedure;
            id.Parameters.AddWithValue("@Action", "SelectedByEmail");
            id.Parameters.AddWithValue("@EmailId", Email);
            SqlDataAdapter sda = new SqlDataAdapter(id);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            RegId = Convert.ToInt32(dt.Rows[0]["RegId"].ToString());
            //SqlCommand cm1 = new SqlCommand("select * from ");
            //lblName.Text = Convert.ToString(dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());

            SqlCommand name = new SqlCommand("select * from RegistrationMaster where RegId='" + RecieverID + "'", con);
            //name.CommandType = CommandType.StoredProcedure;
            //name.Parameters.AddWithValue("@Action", "SelectedById");
            //name.Parameters.AddWithValue("@RegID", RecieverID);
            SqlDataAdapter ads = new SqlDataAdapter(name);
            DataTable td = new DataTable();
            ads.Fill(td);
            lblName.Text = Convert.ToString(td.Rows[0]["FirstName"].ToString() + " " + td.Rows[0]["LastName"].ToString());

            objMessageList.Clear();
            SqlCommand cmd = new SqlCommand("sp_Message", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            cmd.Parameters.AddWithValue("@SenderID", RecieverID);
            cmd.Parameters.AddWithValue("@RecieverID", SenderID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            con.Close();
            da.Fill(dsMessage);
            if (dsMessage.Tables.Count > 0)
            {
                if (dsMessage.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsMessage.Tables[0].Rows)
                    {
                        MessageModel message = new MessageModel();
                        message.SenderID = Convert.ToInt32(dr["SenderID"]);
                        message.RecieverID = Convert.ToString(dr["RecieverID"]);
                        message.Message = Convert.ToString(dr["Message"]);
                        message.TimeStamp = Convert.ToString(dr["TimeStamp"].ToString());
                        objMessageList.Add(message);
                    }
                }
            }
        }
        protected void tmr_Tick(object sender, EventArgs e)
        {
            SenderID = Convert.ToInt32(Request.QueryString["SenderID"]);
            RecieverID = Convert.ToInt32(Request.QueryString["RecieverID"]);
            GetOldMessage(SenderID, RecieverID);
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "getData", "getData()", true);
        }
    }
}