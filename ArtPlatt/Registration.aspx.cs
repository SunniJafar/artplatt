﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Net.Mail;

namespace ArtPlatt
{
    public partial class Registration : System.Web.UI.Page
    {
        private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        int ID;
        int UserID;
        long MaxId;
        string ImageName;
        string buidstring = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Request.QueryString["ID"] != null && (Session["Email"] != null || Session["UserEmail"] != null))
                {
                    string DecId = decryptData(Request.QueryString["ID"]);
                    ID = Convert.ToInt32(DecId);
                    getDetails(ID);
                    giv.Attributes.Add("style", "background: rgba(0,7,0,0.6);");
                }
                //else if (Request.QueryString["ID"] != null && Session["UserEmail"] != null)
                //{
                //    string DecId = decryptData(Request.QueryString["UserID"]);
                //    UserID = Convert.ToInt32(DecId);
                //    giv.Attributes.Add("style", "background: rgba(0,7,0,0.6);");
                //    getDetails(UserID);
                //}
                BindCategory();
                txtemail.Attributes.Add("autocomplete", "off");

            }
        }

        private void BindCategory()
        {
            SqlCommand com = new SqlCommand("sp_Category", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Action", "Select");
            SqlDataAdapter sda = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            try
            {

                drdCategory.DataSource = ds;
                drdCategory.DataValueField = "CategoryID";
                drdCategory.DataTextField = "CategoryName";
                drdCategory.DataBind();
                drdCategory.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void getDetails(int ID)
        {
            h1.Visible = false;
            imgprv.Visible = true;

            btnsubmit.Text = "Update";
            pnlterm.Visible = false;
            custumTerm.Enabled = chkTerm.Enabled;
            lblWelcome.Visible = true;
            pnlemail.Visible = false;
            chkTerm.Visible = false;
            SqlCommand cmd = new SqlCommand("select FirstName,LastName,CompanyName,ContactNo,Address,Description,UserType,CategoryID,SubCategoryID,PhotoPath,LocLat,LocLong from RegistrationMaster as r left join LocationMaster as l on r.RegID = l.RegID where r.RegID='" + ID + "'", con);
            DataTable dt = new DataTable();
            con.Open();
            SqlDataAdapter SDA = new SqlDataAdapter(cmd);
            SDA.Fill(dt);
            hfLat.Value = dt.Rows[0]["LocLat"].ToString();
            hfLon.Value = dt.Rows[0]["LocLong"].ToString();

            txtfname.Text = dt.Rows[0]["FirstName"].ToString();
            txtlname.Text = dt.Rows[0]["LastName"].ToString();
            if (!string.IsNullOrEmpty(dt.Rows[0]["CompanyName"].ToString()))
                txtcomapnyname.Text = dt.Rows[0]["CompanyName"].ToString();
            lblWelcome.Text = "Welcome " + dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
            txtcontact.Text = dt.Rows[0]["ContactNo"].ToString();
            txtPlaces.Text = dt.Rows[0]["Address"].ToString();
            if (!string.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                txtDesc.Text = dt.Rows[0]["Description"].ToString();
            rbltype.SelectedValue = dt.Rows[0]["UserType"].ToString();
            if (Convert.ToInt32(dt.Rows[0]["CategoryID"].ToString()) != 0)
                drdCategory.SelectedValue = dt.Rows[0]["CategoryID"].ToString();
            //if (Convert.ToInt32(dt.Rows[0]["SubCategoryID"].ToString()) != 0)
            //    drdsubcategory.SelectedValue = dt.Rows[0]["SubCategoryID"].ToString();
            BIndSubcategory(Convert.ToInt32(dt.Rows[0]["CategoryId"]));
            if (Convert.ToInt32(dt.Rows[0]["SubCategoryID"].ToString()) != 0)
                drdsubcategory.SelectedValue = dt.Rows[0]["SubCategoryID"].ToString();
            photo.Value = dt.Rows[0]["PhotoPath"].ToString();
            if (!string.IsNullOrEmpty(dt.Rows[0]["PhotoPath"].ToString()))
                imgprv.ImageUrl = "Image/Artist/" + dt.Rows[0]["PhotoPath"].ToString();
            else
                imgprv.ImageUrl = "~/assets/DefaultImage/hello.png";
            con.Close();

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (btnsubmit.Text == "Register")
            {
                string encPassword = EncryptData(txtpass.Text);
                string CompanyName = txtcomapnyname.Text != null ? txtcomapnyname.Text : string.Empty;

                SqlCommand sqcmd = new SqlCommand("select max(RegID) from RegistrationMaster", con);
                con.Open();
                int maxId = Convert.ToInt32(sqcmd.ExecuteScalar());
                MaxId = maxId + 1;

                SqlCommand chk = new SqlCommand("select count(RegID) from RegistrationMaster where EmailId='" + txtemail.Text + "'", con);

                int obj1 = Convert.ToInt32(chk.ExecuteScalar());
                if (obj1 == 0)
                {
                    if (fuPhoto.HasFile)
                    {
                        string fileextension = Path.GetExtension(fuPhoto.FileName).ToLower();
                        bool isexists = Directory.Exists(Server.MapPath("~/" + "Image" + "/" + "Artist"));
                        if (isexists == false)
                            Directory.CreateDirectory(Server.MapPath("~/" + "Image" + "/" + "Artist"));
                        string path = Server.MapPath("~/" + "Image" + "/" + "Artist");
                        string mappath = path + "\\" + txtfname.Text + "_" + txtlname.Text + "_" + MaxId + fileextension;

                        ImageName = txtfname.Text + "_" + txtlname.Text + "_" + MaxId + fileextension;
                        bool isexists3 = Directory.Exists(mappath);
                        if (isexists3 == false)
                        {
                            fuPhoto.SaveAs(mappath);
                        }
                    }
                    if (rbltype.SelectedValue == "1" && hfLat.Value == string.Empty && hfLon.Value == string.Empty)
                    {
                        lblmessage.Text = "Please choose location!";
                    }
                    else
                    {
                        SqlCommand cmd = new SqlCommand("sp_Registration", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Action", "Insert");
                        cmd.Parameters.AddWithValue("@FirstName", txtfname.Text);
                        cmd.Parameters.AddWithValue("@LastName", txtlname.Text);
                        cmd.Parameters.AddWithValue("@ContactNo", txtcontact.Text);
                        cmd.Parameters.AddWithValue("@PhotoPath", ImageName);
                        cmd.Parameters.AddWithValue("@UserType", rbltype.SelectedValue);
                        cmd.Parameters.AddWithValue("@CompanyName", txtcomapnyname.Text);
                        cmd.Parameters.AddWithValue("@EmailId", txtemail.Text);
                        cmd.Parameters.AddWithValue("@Password", encPassword);
                        cmd.Parameters.AddWithValue("@Address", txtPlaces.Text);
                        cmd.Parameters.AddWithValue("@CategoryID", drdCategory.SelectedValue);
                        cmd.Parameters.AddWithValue("@SubCategoryID", drdsubcategory.SelectedValue);
                        cmd.Parameters.AddWithValue("@Description", txtDesc.Text);
                        cmd.Parameters.AddWithValue("@Status", true);

                        cmd.ExecuteNonQuery();
                        con.Close();

                        #region send mail of Welcome
                        buidstring = txtfname.Text + " " + txtlname.Text + "_" + txtemail.Text + "_" + MaxId + "_" + txtpass.Text;

                        MailMessage messagemail = new MailMessage();
                        SmtpClient smtpClient = new SmtpClient();
                        MailAddress fromAddress = new MailAddress("service@artplatt.com");
                        string[] strbuild = buidstring.Split('_');
                        messagemail.Body = PopulateMailBody(strbuild[0].ToString(), strbuild[1].ToString(), strbuild[2].ToString(), strbuild[3].ToString(), "http://www.artplatt.com/Login.aspx", "");
                        messagemail.From = fromAddress;
                        messagemail.To.Add(strbuild[1].ToString());
                        messagemail.Subject = "Registration successful.";
                        messagemail.IsBodyHtml = true;
                        smtpClient.Host = "smtp.world4you.com";
                        smtpClient.Port = Convert.ToInt32(25);
                        smtpClient.EnableSsl = false;
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new System.Net.NetworkCredential("service@artplatt.com", "service5");
                        smtpClient.Send(messagemail);
                        #endregion

                        lblmessage.Text = "Registeration successfully";
                        if (rbltype.SelectedValue == "1" || rbltype.SelectedValue == "0")
                        {
                            SqlCommand id = new SqlCommand("select Max(RegID) from RegistrationMaster", con);
                            con.Open();
                            int RegId = Convert.ToInt32(id.ExecuteScalar());
                            SqlCommand scmd = new SqlCommand("sp_Location", con);
                            scmd.CommandType = CommandType.StoredProcedure;
                            scmd.Parameters.AddWithValue("@Action", "Insert");
                            scmd.Parameters.AddWithValue("@LocLat", hfLat.Value);
                            scmd.Parameters.AddWithValue("@LocLong", hfLon.Value);
                            scmd.Parameters.AddWithValue("@RegID", RegId);
                            scmd.ExecuteNonQuery();
                            Session["Email"] = txtemail.Text;
                            Response.Redirect("~/Default.aspx");
                            con.Close();
                        }
                        else
                        {
                            Session["UserEmail"] = txtemail.Text;
                            Response.Redirect("~/Default.aspx");
                        }
                    }
                }
                else
                {
                    lblmessage.Text = "Email Id Alredy Exist";
                }
            }
            else if (btnsubmit.Text == "Update")
            {
                con.Open();
                string DecId = decryptData(Request.QueryString["ID"]);
                ID = Convert.ToInt32(DecId);
                SqlCommand Address = new SqlCommand("select * from RegistrationMaster as r left join LocationMaster as l on r.RegID = l.RegID where r.RegID='" + ID + "'", con);
                DataTable adt = new DataTable();
                SqlDataAdapter asda = new SqlDataAdapter(Address);
                asda.Fill(adt);
                con.Close();
                if (rbltype.SelectedValue == "1" && hfLat.Value == string.Empty && hfLon.Value == string.Empty)
                {
                    lblmessage.Text = "Please choose location!";
                }
                else if (adt.Rows[0]["Address"].ToString() != txtPlaces.Text && (adt.Rows[0]["LocLat"].ToString() == hfLat.Value && adt.Rows[0]["LocLong"].ToString() == hfLon.Value))
                {
                    lblmessage.Text = "Please choose location!";
                }
                else
                {
                    string EmailId = Convert.ToString(Session["Email"]);

                    if (Session["Email"] != null)
                    {
                        EmailId = Convert.ToString(Session["Email"]);
                    }
                    else if (Session["UserEmail"] != null)
                    {
                        EmailId = Convert.ToString(Session["UserEmail"]);
                    }


                    string strname = photo.Value;
                    if (fuPhoto.HasFile)
                    {
                        string old_image_name = Server.MapPath("~/Image/Art/" + photo.Value + "");
                        if (System.IO.File.Exists(old_image_name))
                        {
                            System.IO.File.Delete(Server.MapPath("~/Image/Art/" + photo.Value + ""));
                            System.IO.File.Delete(old_image_name);
                        }
                        string fileextension = Path.GetExtension(fuPhoto.FileName).ToLower();
                        bool isexists = Directory.Exists(Server.MapPath("~/" + "Image" + "/" + "Artist"));
                        if (isexists == false)
                            Directory.CreateDirectory(Server.MapPath("~/" + "Image" + "/" + "Artist"));
                        string path = Server.MapPath("~/" + "Image" + "/" + "Artist");
                        string mappath = path + "\\" + txtfname.Text + "_" + txtlname.Text + "_" + ID + fileextension;

                        strname = txtfname.Text + "_" + txtlname.Text + "_" + ID + fileextension;
                        bool isexists3 = Directory.Exists(mappath);
                        if (isexists3 == false)
                        {
                            fuPhoto.SaveAs(mappath);
                        }
                        imgprv.ImageUrl = "~/Image/Artist/" + strname.ToString();
                    }
                    SqlCommand chk = new SqlCommand("select count(RegID) from RegistrationMaster where EmailId='" + EmailId + "'", con);
                    con.Open();

                    int obj1 = Convert.ToInt32(chk.ExecuteScalar());
                    if (obj1 == 1)
                    {
                        SqlCommand cmd = new SqlCommand("sp_Registration", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Action", "Update");
                        cmd.Parameters.AddWithValue("@RegID", ID);
                        cmd.Parameters.AddWithValue("@FirstName", txtfname.Text);
                        cmd.Parameters.AddWithValue("@LastName", txtlname.Text);
                        cmd.Parameters.AddWithValue("@ContactNo", txtcontact.Text);
                        cmd.Parameters.AddWithValue("@PhotoPath", strname);
                        cmd.Parameters.AddWithValue("@UserType", rbltype.SelectedValue);
                        cmd.Parameters.AddWithValue("@CompanyName", txtcomapnyname.Text);
                        cmd.Parameters.AddWithValue("@Address", txtPlaces.Text);
                        cmd.Parameters.AddWithValue("@CategoryID", drdCategory.SelectedValue);
                        cmd.Parameters.AddWithValue("@Description", txtDesc.Text);
                        cmd.Parameters.AddWithValue("@SubCategoryID", drdsubcategory.SelectedValue);
                        cmd.ExecuteNonQuery();
                        con.Close();
                        lblmessage.Text = "Profile Updated successfully ";
                        if (rbltype.SelectedValue == "1" || rbltype.SelectedValue == "0")
                        {
                            con.Open();
                            SqlCommand scmd = new SqlCommand("sp_Location", con);
                            scmd.CommandType = CommandType.StoredProcedure;
                            scmd.Parameters.AddWithValue("@Action", "Update");
                            scmd.Parameters.AddWithValue("@LocLat", hfLat.Value);
                            scmd.Parameters.AddWithValue("@LocLong", hfLon.Value);
                            scmd.Parameters.AddWithValue("@RegID", ID);
                            scmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    else
                    {
                        lblmessage.Text = "Email Id Alredy Exist";
                    }
                }
            }
        }

        public string EncryptData(string pwd)
        {
            byte[] pd = System.Text.ASCIIEncoding.ASCII.GetBytes(pwd);
            string enpwd = Convert.ToBase64String(pd);
            return enpwd;
        }
        public string decryptData(string pwd)
        {
            byte[] pd = Convert.FromBase64String(pwd);
            string str = System.Text.ASCIIEncoding.ASCII.GetString(pd);
            return str;
        }
        public void Clear()
        {
            txtfname.Text = string.Empty;
            txtlname.Text = string.Empty;
            txtemail.Text = string.Empty;
            txtpass.Text = string.Empty;
            txtPlaces.Text = string.Empty;
            txtDesc.Text = string.Empty;
            txtcomapnyname.Text = string.Empty;
            txtcontact.Text = string.Empty;
        }
        private string PopulateMailBody(string name, string userName, string refid, string pwd, string url, string description)
        {
            string tittle = "You Successfully Forget Your Password";
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/HTML/Welcome.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Title}", tittle);
            body = body.Replace("{UserName}", userName);
            body = body.Replace("{Name}", name);
            body = body.Replace("{ReferenceID}", refid);
            body = body.Replace("{Password}", pwd);
            body = body.Replace("{url}", url);
            body = body.Replace("{Description}", description);
            return body;
        }

        protected void drdCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this,this.GetType(), "hiddenNameDiv", "document.getElementById('hiddenNameDiv').style.display='none'", true);

            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "show", "show(" + rbltype.SelectedValue + ")", true);

            drdsubcategory.Enabled = true;
            int CID = Convert.ToInt32(drdCategory.SelectedValue);
            SqlDataAdapter sda = new SqlDataAdapter("select * from SubCategoryMaster where CategoryId='" + CID + "'", con);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            drdsubcategory.DataSource = ds;
            drdsubcategory.DataTextField = "Name";
            drdsubcategory.DataValueField = "SubCategoryID";
            drdsubcategory.DataBind();
            drdsubcategory.Items.Insert(0, new ListItem("Select", "0"));
        }
        private void BIndSubcategory(int CategoryID)
        {
            drdsubcategory.Enabled = true;
            SqlDataAdapter sda = new SqlDataAdapter("select * from SubCategoryMaster where CategoryId='" + CategoryID + "'", con);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            drdsubcategory.DataSource = ds;
            drdsubcategory.DataTextField = "Name";
            drdsubcategory.DataValueField = "SubCategoryID";
            drdsubcategory.DataBind();
            drdsubcategory.Items.Insert(0, new ListItem("Select", "0"));
        }
    }
}