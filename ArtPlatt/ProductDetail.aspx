﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="ArtPlatt.ProductDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <asp:Label ID="lblcategory" runat="server"></asp:Label></li>
                <li>
                    <asp:Label ID="lblsub" runat="server"></asp:Label></li>
                <li class="active">
                    <asp:Label ID="artName" runat="server"></asp:Label></li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Product Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Product Image Carousel -->

            <div class="col-sm-7">
                <div id="ws-products-carousel" class="owl-carousel">
                    <asp:Repeater ID="rptPrv" runat="server">
                        <ItemTemplate>
                            <div class="item">
                                <asp:Panel ID="Panel1" runat="server" Visible='<%# Eval("FileType").ToString()=="Viedio"%>'>
                                    <center><video id="video" src='/Image/Art/<%#Eval("ArtPath") %>' controls="true"  class="img-responsive"></video></center>
                                </asp:Panel>
                                <asp:Panel ID="Panel2" runat="server" Visible='<%# Eval("FileType").ToString()=="Image"%>'>
                                    <center>  <img src='/Image/Art/<%#Eval("ArtPath") %>' class="img-responsive" /> </center>
                                </asp:Panel>
                                <asp:Panel ID="Panel3" runat="server" Visible='<%# Eval("FileType").ToString()=="Audio"%>'>
                                    <center><img src="assets/DefaultImage/Audio.png"/><br />
                                    <audio src='/Image/Art/<%#Eval("ArtPath") %>' controls></audio></center>
                                </asp:Panel>
                                <asp:Panel ID="Panel4" runat="server" Visible='<%# Eval("FileType").ToString()=="Pdf"%>'>
                                    <center>   <a href='/Image/Art/<%#Eval("ArtPath") %>' target="_blank">
                                        <img src="assets/DefaultImage/Pdf.png" class="img-responsive" /></a></center>
                                </asp:Panel>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>

            <!-- Product Information -->
            <div class="col-sm-5">
                <div class="ws-product-content">
                    <header>
                        <div class="ws-item-category">
                            <div class="ws-forgot-pass">
                                <asp:LinkButton ID="lblArtistName" runat="server" Font-Italic="false" OnClick="lblArtistName_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <h3 class="ws-item-title">
                            <asp:Label ID="lblartName" runat="server"></asp:Label>
                        </h3>

                        <div class="ws-separator"></div>

                        <!-- Price -->
                        <div class="ws-single-item-price">
                            <asp:Label ID="lblPrice" runat="server"></asp:Label>
                        </div>
                        <asp:HiddenField ID="hfArtist" runat="server" />
                        <!-- Quantity -->

                    </header>

                    <div class="ws-product-details">
                        <asp:Label ID="lbldesc" runat="server"></asp:Label>
                    </div>
                    <a class="btn ws-btn-fullwidth" href="#ws-register-modal" id="btncontact" runat="server" data-toggle="modal">Contact </a>
                    <div class="modal fade" id="ws-register-modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                </div>
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="modal-body">
                                            <!-- Register Form -->
                                            <div class="ws-register-form">
                                                <h3>
                                                    <asp:Label runat="server" ID="lblName"></asp:Label></h3>
                                                <div class="ws-separator"></div>
                                                <h4 align="center">You Need to login First!</h4>
                                                <!-- Name -->
                                                <%-- <div class="form-group">
                                                    <label class="control-label">Name <span>*</span></label>
                                                    <input type="text" class="form-control">
                                                </div>

                                                <!-- Email -->
                                                <div class="form-group">
                                                    <label class="control-label">Email Adress <span>*</span></label>
                                                    <input type="email" class="form-control">
                                                </div>

                                                <!-- Password -->
                                                <div class="form-group">
                                                    <label class="control-label">Phone Number <span></span></label>
                                                    <input type="email" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Message <span></span></label>
                                                    <input type="textarea" class="form-control">
                                                </div>--%>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <!-- Button -->
                                            <a class="btn ws-btn-fullwidth" href="Login.aspx">Login</a>
                                            <br>
                                            <br>
                                            <!-- Facebook Button -->

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Button -->
                </div>
            </div>
        </div>
    </div>

    <div class="ws-related-section">
        <div class="container">
            <div class="ws-related-title">
                <h3 runat="server" id="h3">Related Products</h3>
            </div>
            <asp:Repeater ID="rptArt" runat="server">
                <ItemTemplate>
                    <div class="col-sm-4">
                        <div class="ws-works-item">

                            <figure>
                                <asp:Panel ID="Panel1" runat="server" Visible='<%# Eval("FileType").ToString()=="Viedio"%>'>
                                    <video id="video" src='/Image/Art/<%#Eval("ArtPath") %>' style="padding-top: 65px;" controls="true" class="img-responsive"></video>
                                </asp:Panel>
                                <asp:Panel ID="Panel2" runat="server" Visible='<%# Eval("FileType").ToString()=="Image"%>'>
                                    <img src='/Image/Art/<%#Eval("ArtPath") %>' class="img-responsive" />
                                </asp:Panel>
                                <asp:Panel ID="Panel3" runat="server" Visible='<%# Eval("FileType").ToString()=="Audio"%>'>
                                    <img src="assets/DefaultImage/Audio.png" style="height: 236px; width: 100%;" />
                                    <audio src='/Image/Art/<%#Eval("ArtPath") %>' style="width: 100%" controls></audio>

                                </asp:Panel>
                                <asp:Panel ID="Panel4" runat="server" Visible='<%# Eval("FileType").ToString()=="Pdf"%>'>
                                    <a href='/Image/Art/<%#Eval("ArtPath") %>' target="_blank">
                                        <img src="assets/DefaultImage/Pdf.png" class="img-responsive" /></a>
                                </asp:Panel>
                            </figure>
                            <div class="ws-works-caption text-center">
                                <a href="ProductDetail.aspx?ArtID=<%#Eval("ArtID") %>">
                                    <div class="ws-item-category">
                                        <%#Eval("Name") %>
                                    </div>
                                    <h3 class="ws-item-title">
                                        <%#Eval("ArtName") %>
                                    </h3>
                                </a>
                                <a href="ArtistProfile.aspx?ArtistID=<%#Eval("ArtistId") %>">
                                    <div class="ws-item-category" style="text-transform: none !important; margin-top: 5px;">
                                        <%#Eval("ArtistName") %>
                                    </div>
                                </a>
                                <div class="ws-item-separator"></div>
                                <div class="ws-item-price">
                                    € <%#Eval("Price") %>
                                </div>
                            </div>

                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
