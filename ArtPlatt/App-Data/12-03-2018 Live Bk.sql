USE [artplatt]
GO
/****** Object:  Table [dbo].[AdminMaster]    Script Date: 12/3/2018 10:26:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminMaster](
	[AdminID] [int] NOT NULL,
	[EmailID] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AdminMaster] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ArtMaster]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ArtMaster](
	[ArtID] [bigint] IDENTITY(1,1) NOT NULL,
	[ArtName] [varchar](50) NOT NULL,
	[Price] [varchar](50) NULL,
	[SubCategoryId] [bigint] NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[RegID] [bigint] NULL,
 CONSTRAINT [PK_ArtMaster] PRIMARY KEY CLUSTERED 
(
	[ArtID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[CategoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NOT NULL,
	[CategoryImage] [varchar](max) NOT NULL,
 CONSTRAINT [PK_CategoryMaster] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactUs]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactUs](
	[ContactUsID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[EmailID] [varchar](50) NOT NULL,
	[Message] [varchar](250) NOT NULL,
 CONSTRAINT [PK_ContactUs] PRIMARY KEY CLUSTERED 
(
	[ContactUsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ImageMaster]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImageMaster](
	[ImageID] [bigint] IDENTITY(1,1) NOT NULL,
	[ArtPath] [varchar](max) NOT NULL,
	[ArtID] [bigint] NOT NULL,
	[FileType] [varchar](50) NULL,
 CONSTRAINT [PK_ImageMaster] PRIMARY KEY CLUSTERED 
(
	[ImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LocationMaster]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LocationMaster](
	[LocationID] [bigint] IDENTITY(1,1) NOT NULL,
	[LocLat] [varchar](50) NOT NULL,
	[LocLong] [varchar](50) NOT NULL,
	[RegID] [bigint] NOT NULL,
 CONSTRAINT [PK_LocationMaster] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Message]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Message](
	[MessageID] [int] IDENTITY(1,1) NOT NULL,
	[SenderID] [int] NOT NULL,
	[RecieverID] [int] NULL,
	[Message] [varchar](max) NOT NULL,
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_Message_TimeStamp]  DEFAULT (getdate()),
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegistrationMaster]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegistrationMaster](
	[RegID] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](30) NOT NULL,
	[LastName] [varchar](30) NOT NULL,
	[ContactNo] [varchar](18) NULL,
	[PhotoPath] [varchar](max) NULL,
	[UserType] [int] NOT NULL,
	[CompanyName] [varchar](max) NULL,
	[EmailId] [varchar](255) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[CategoryID] [bigint] NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[Status] [bit] NULL,
	[SubCategoryID] [bigint] NULL,
 CONSTRAINT [PK_RegistrationMaster] PRIMARY KEY CLUSTERED 
(
	[RegID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubCategoryMaster]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubCategoryMaster](
	[SubCategoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CategoryId] [bigint] NOT NULL,
 CONSTRAINT [PK_SubCategoryMaster] PRIMARY KEY CLUSTERED 
(
	[SubCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[AdminMaster] ([AdminID], [EmailID], [Password]) VALUES (1, N'Admin', N'Artplatt%%%')
SET IDENTITY_INSERT [dbo].[ArtMaster] ON 

INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (85, N'Rebecca Horn - Zenith of the Ocean II', N'770', 17, N' C- print, 2007, Kodak Professional paper, 25 Exemplare, Format: 89,5 x 69,0 cm - Aus  Selbstpositionierungen - Mappenwerk “Doctor’s of the World Portfolio” 2007', 60)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (94, N'Steirischer Panther #4', N'2.900', 42, N'"Die Pantherin"
Keramik, dreiteilig
H-85 cm, B-35 cm, T-36 cm, 2016', 77)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (95, N'Steirischer Panther #4', N'2.900', 42, N'"Die Pantherin"
Keramik, dreiteilig
H-85 cm, B-35 cm, T-36 cm, 2016', 77)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (96, N'Steirischer Panther #4', N'2.900', 42, N'"Die Pantherin"
Keramik, dreiteilig
H-85 cm, B-35 cm, T-36 cm, 2016', 77)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (97, N'Steirischer Panther #4', N'2.900', 42, N'"Die Pantherin"
Keramik, dreiteilig
H-85 cm, B-35 cm, T-36 cm, 2016', 77)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (98, N'Steirischer Panther #4', N'2.900', 42, N'"Die Pantherin"
Keramik, dreiteilig
H-85 cm, B-35 cm, T-36 cm, 2016', 77)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (99, N'Steirischer Panther #4', N'2.900', 42, N'"Die Pantherin"
Keramik, dreiteilig
H-85 cm, B-35 cm, T-36 cm, 2016
', 77)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (100, N'Steirischer Panther #4', N'2900', 42, N'"Die Pantherin"
Keramik, dreiteilig
H-85 cm, B-35 cm, T-36 cm, 2016
', 77)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (102, N'Die Pantherin', N'2900', 42, N'No. 4, Keramik dreiteilig, aus der Werkserie "Steirische Panther",
H-85 cm, B-35 cm, T-36 cm, 2016', 77)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (105, N'Georg Baselitz, Sono sei piedi', N'4700', 42, N'Georg Baselitz,
Sono sei piedi, 2015
Etching and Aquatinta in black on a chamois printed background
86 x 65 cm, 
Edition of 15

', 80)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (106, N'Mark Lammert', N'1700', 42, N'Drawing from the series dt/frz, 2017-18
Oil paint, charcoal and ballpoint pen on handmade paper Moulin de Larroque 400gr.
31 x 45 cm', 80)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (107, N'John Giorno', N'850', 42, N'Du Musst brennen um zu strahlen, 1992
Screen print on coventry rag 335g.
ed of 90
56 x 56 cm', 80)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (115, N'XOOOOX - Nati (TLO)', N'5200', 42, N'2017
Material: Metal 
Method: Spray paint 
Framed 83,5 x 63,5 x 3 cm. 
Signed, dated and titled on verso.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (120, N'Andy Warhol - 25 Cats named Sam', N'', 42, N'Sam (From "25 Cats named Sam and one blue Pussy", FS IV. 67A.)
Andy Warhol, 1954
Size:15 x 23 cm
Material: Paper 
Method: Offset-lithograph hand-watercolored 
Edition: 190 
Other: 	From a portfolio with hand-colored lithographs. Titled by Warhols mother Julia Warhola. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (122, N'BAMBI - Amy', N'', 42, N'Amy (Winehouse) Red Unique with Diamond Dust.
BAMBI Street Artist, 2012
Size:112.76 x 76.2 cm
Material: Paper. 
Method: Stencil and Mixed Media. 
Other: 	Unique handfinished Artists Proof with Diamond Dust and Swarowski Crystal. Signed. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (123, N'BAMBI - Amour Icone (David Beckham)', N'', 42, N'BAMBI Street Artist, 2015
Size:102 x 89 cm
Material: Somerset Soft Wove Paper 
Method: Stencil, Spray Paint & Diamond Dust 
Edition: 5 
Other: Signed. Edition: 5 T.P. Very rare piece, the regular edition of 50 is already sold out. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (125, N'Mr. Brainwash - Life Is Beautiful SIlver', N'', 42, N'Life Is Beautiful - Hard Candy (Silver)
Mr. Brainwash, 2017
Material: Cast Resin and Thermal Coated Reflective Metallic Finish  
Method: Sculpture 
Edition: 100 
Other: Signed and numbered. 
14.6 x 22.9 x 3.8 cm.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (126, N'Mr. Brainwash - Life Is Beautiful Light Pink', N'', 42, N'Life Is Beautiful - Hard Candy (Light Pink)
Mr. Brainwash, 2017
Material: Cast Resin and Thermal Coated Reflective Metallic Finish  
Method: Sculpture 
Edition: 100 
Other: Signed and numbered. 
14.6 x 22.9 x 3.8 cm.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (127, N'Mr. Brainwash - Every Day Life (Keith Haring)', N'', 42, N'Every Day Life - Follow Your Dreams (Keith Haring)
Mr. Brainwash, 2017
Size: 127 x 96.5 cm
Material: Paper 
Method: Stencil and Mixed Media 
Other: Unique, signed. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (128, N'Mr. Brainwash - Marilyn Monroe', N'', 42, N'Mr. Brainwash, 2017
Size:96.5 x 96.5 cm
Material: Paper 
Method: Stencil and Mixed Media 
Other: Unique, signed. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (129, N'Mr. Brainwash - Mickey & Minnie (green)', N'', 42, N'Mr. Brainwash, 2014
Size:96.5 x 127 cm
Material: Paper 
Method: Stencil and Mixed Media 
Other: Unique. Signed front and verso. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (130, N'Mr. Brainwash - Mickey & Minnie (green)', N'', 42, N'Mr. Brainwash, 2014
Size:96.5 x 127 cm
Material: Paper 
Method: Stencil and Mixed Media 
Other: Unique. Signed front and verso. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (131, N'Alex Katz - Summer Flowers', N'', 42, N'Alex Katz, 2018
Size:281.9 x 106.7 cm
Material: Canvas 
Method: Silkscreen 
Edition: 35 
Other: Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (132, N'Alex Katz - Three Portraits', N'', 42, N'Three Portraits (Sarah, Vivien, Sophia)
Alex Katz, 2012
Size: 99 x 104 cm
Material: Paper 
Method: Lithograph 
Edition: 60 
Other: 	Portfolio of three portraits: Sarah Mearns, Vivien Bittencourt, Sophia Holmann. Each signed and numbered.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (133, N'Alex Katz - Nicole', N'', 42, N'Alex Katz, 2016
Size:58 x 116 cm
Material: Crane Museo Max 365 g fine art paper 
Method: Archival pigment inks 
Edition: 90 
Other: Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (134, N'Robert Longo - Bucephalus', N'', 42, N'Robert Longo, 2017
Size:56 x 61 cm
Material: Hahnemühle Photo Rag Ultra Smooth 305g paper 
Method: Ditone Print 
Edition: 30 
Other: Edition: 30 + 10 AP. Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (135, N'Robert Longo - Rosette Nebula', N'', 42, N'Robert Longo, 2014
Size:89 x 151.7 cm
Material: Velin 
Method: Pigment print 
Edition: 25 
Other: Signed and dated. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (136, N'Julian Opie - Heads - Danielle', N'', 42, N'2017
Size:29.7 x 34.2 cm
Material: Aluminium 
Method: Sculpture, screenprint and spray paint  
Edition: 20 
Other: Free-standing, double-sided aluminium profile. Signed and numbered.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (137, N'Julian Opie - Heads - Bobby', N'', 42, N'2017
Size:30.7 x 34.2 cm
Material: Aluminium 
Method: Sculpture, screenprint and spray paint  
Edition: 20 
Other: Free-standing, double-sided aluminium profile. Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (139, N'Julian Opie - Heads - Zhiyun', N'', 42, N'2017
Size:27.7 x 34.2 cm
Material: Aluminium 
Method: Sculpture, screenprint and spray paint  
Edition: 20 
Other: Free-standing, double-sided aluminium profile. Signed and numbered.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (140, N'Julian Opie - Heads - Ian', N'', 42, N'2017
Size:33.9 x 35.8 cm
Material: Aluminium  
Method: Sculpture, screenprint and spray paint  
Edition: 20 
Other:  Free-standing, double-sided aluminium profile. Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (141, N'Julian Opie - Nature 1 - Sheep 1', N'', 42, N'2015
Size:86.8 x 44.9 cm
Material: Aluminium 
Edition: 25 
Other: Part of the "Nature 1" series. Fourteen 4mm aluminium profiles with inset hanging fixing, powder coated in satin black. Signed and numbered on verso. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (142, N'Julian Opie - Nature 1 - Boat 2', N'', 42, N'2015
Size:79 x 28 cm
Material: Aluminium 
Edition: 25 
Other: 	Part of the "Nature 1" series. Fourteen 4mm aluminium profiles with inset hanging fixing, powder coated in satin black. Signed and numbered on verso. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (143, N'Julian Opie - Nature 1 - Pebbles 4', N'', 42, N'2015
Size:102.7 x 38.2 cm
Material: Aluminium 
Edition: 25 
Other: 	Part of the "Nature 1" series. Fourteen 4mm aluminium profiles with inset hanging fixing, powder coated in satin black. Signed and numbered on verso. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (144, N'Julian Opie - Nature 2 - Minnows', N'', 42, N'2015
Size:62.1 x 62.1 cm
Method: Lenticular 
Edition: 35 
Other: 	Part of the "Nature 2" series. Six lenticular acrylic panels, framed. Signed and numbered on reverse. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (145, N'Enoc Perez - Lever House (Purple)', N'', 42, N'2011
Size: 122.5 x 86.3 cm
Method: Screenprint 
Edition: 20 
Other: Signed ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (146, N'Mel Ramos - Mint Pattie', N'', 42, N'2017
Size: 58.5 x 80.5 cm
Material: Revere-Magnani paper 330gr 
Method: Lithograph in 34 colors 
Edition: 120 
Other: 	Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (148, N'Mel Ramos - Coco Cookie', N'', 42, N'2015
Size:16 x 23 cm
Material: Screenprint-cardboard 
Method: Offset lithograph 
Edition: 499 
Other: 	Signed and numbered ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (149, N'Mel Ramos - Navel Oranges', N'', 42, N'2013
Size:28 x 22 cm
Material: Paper 
Method: Digital print 
Edition: 999 
Other: 	Signed and numbered ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (150, N'Mel Ramos - Superman', N'', 42, N'2006
Size:115 x 85 cm
Material: Paper
Method: Lithograph 
Edition: 199 
Other: 	Signed and numbered. Copyright Mel Ramos and DC COmics/Warner Bros  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (151, N'Mel Ramos - Superman', N'', 42, N'2006
Size:115 x 85 cm
Material: Paper
Method: Lithograph 
Edition: 199 
Other: 	Signed and numbered. Copyright Mel Ramos and DC COmics/Warner Bros  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (153, N'Mel Ramos - Zebra', N'', 42, N'1981
Size:52 x 64 cm
Material: Paper
Method: Lithograph
Edition: 250 
Other: Signed and numbered. Very rare piece. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (154, N'Andy Warhol - Flowers III.114', N'', 42, N'Hand Colored Flowers III.114
Andy Warhol, 1974
Size:69.2 x 103.8 cm
Material: Arches paper and J.Green paper 
Method: Screenprint hand-colored with Dr. Martin’s aniline water color dyes 
Edition: 250 
Other: 	Signed, each piece is unique. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (155, N'Andy Warhol - Grace Kelly FS II.305', N'', 42, N'1984
Size:81 x 101.6 cm
Material: Lenox Museum Board 
Method: Lithograph
Edition: 225 
Other: 	Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (156, N'Alex Katz - A Tremor In The Morning', N'', 42, N'1986
Size 50.3 x 51.6 cm
Method: Woodcut 
Edition: 45 
Other: The complete portfolio, comprising ten woodcuts printed in colors, each signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (157, N'Atsushi Kaga - David Bowie Flash', N'', 42, N'2012
Size:21 x 29.7 cm
Material: Paper 
Method: Lithograph 
Edition: 20 
Other: Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (158, N'Atsushi Kaga - Elvis Costello', N'', 42, N'2012
Size:21 x 29.7 cm
Material: Paper 
Method: Lithograph  
Edition: 20 
Other: 	signed and numbered ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (159, N'Atsushi Kaga - Usacchi with owls', N'', 42, N'2012
Size: 21 x 29.7 cm
Material: Paper
Method: Lithograph
Edition: 20 
Other: 	Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (164, N'Alpenwinde', N'470', 12, N'Alpenwinde', 87)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (165, N'Act 114', N'1250', 12, N'Abstraktes Aktbild , Galeriebestand, Marz: Act 114', 87)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (166, N'Am Monte Labbro', N'1120', 12, N'Am Monte Labbro (2017)
120x80cm
Acryl auf Leinwand', 87)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (167, N'Painting: Have a Look', N'1190', 12, N'Have a Look
100 x 100 cm
Acryl on Canvas', 87)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (168, N'Painting Have a Look', N'1190', 12, N'Have a Look
100 x 100 cm
Acryl on Canvas', 87)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (169, N'Erwartungsvoll', N'1900', 12, N'Erwartungsvoll (2017)
150 x 100 cm
Acryl auf Leinwand
', 87)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (170, N'Weltengarten', N'600', 12, N'Weltengarten
65 x 65 cm
gerahmt', 87)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (171, N'Nora Gres - Zürich (2013)', N'', 42, N'44 x 37 cm. 
Mixed Media, Textile, Patchwork, Sewing. 
Unique artwork.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (172, N'Nora Gres - Chanel (2013)', N'', 42, N'64 x 56 cm.
Mixed Media, Textile, Patchwork, Sewing. 
Unique artwork.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (173, N'Jae Yong Kim - Donut Rush (20015)', N'', 42, N'Size: 15 x 3.8 cm / 6 x 1 inch
Material: Ceramic 
Method: Sculpture / Object 
Other: Ceramic, underglaze, glaze, luster glaze, Swarovski crystals, mixed media. 
Unique, signed on verso. Approx. 15 x 15 X 3.8 cm. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (175, N'Retna - Sacred Dance Of Memories (2017)', N'', 42, N'Size: 68 x 82 cm/ 27 x 32 inch
Material: White Paper BFK Rives 270 g 
Method: Lithograph in 4 colors 
Edition: 99 
Other: Hand cut.
Signed, titled and numbered by the artist. 

In this artwork Retna`s particular calligraphic style can be found. Arabic, Egyptian, Hebrew, Old English and Native American typographies interbreed and form Retna’s universal language. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (176, N'Jürgen Teller - Swimming, Glemmingebro, Sweden ', N'', 42, N'2015
Size: 30.5 x 25.4 cm
Method: Giclée print 
Edition: 120 
Other: 	Edition: 120?+?20 A.P., with signed and numbered certificate. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (177, N' Jim Dine, Pinocchio Coming from the Green (2011)', N'', 42, N'Size: 55.88 x 75.95 cm
Method: Lithograph in colors 
Edition: 75 
Other: Signed and numbered lower left in pencil ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (178, N'John CRASH Matos - X-Men Portfolio (2000)', N'', 42, N'4 Prints with Marvel`s X-Men comic book characters Magneto, Storm, Wolverine and Cylops. 

Size: 46 x 61 cm
Material: 2-ply museum board 
Method: 9-color silkscreen  
Edition: 250 
Other: Signed and numbered by Crash with the Marvel copyright on verso. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (179, N'Paul Smith / Gufram - Cactus Rosso (2010)', N'', 42, N'Size: 70 x 70 x 170 cm
Material: Polyurethane with Guflac finish  
Edition: 500 
Other: Design object, coat stand ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (180, N'Paul Smith / Gufram - Cactus Rosso (2010)', N'', 42, N'Size: 70 x 70 x 170 cm
Material: Polyurethane with Guflac finish  
Edition: 500 
Other: Design object, coat stand ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (181, N'Paul Smith / Gufram - Cactus Rosso (2010)', N'', 42, N'Size: 70 x 70 x 170 cm
Material: Polyurethane with Guflac finish  
Edition: 500 
Other: Design object, coat stand ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (182, N'Paul Smith / Gufram - Cactus Rosso (2010)', N'', 42, N'Size: 70 x 70 x 170 cm
Material: Polyurethane with Guflac finish  
Edition: 500 
Other: Design object, coat stand ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (183, N'Paul Smith / Gufram - Psychodelic Cactus (2016)', N'', 42, N'Size: 70 x 70 x 170 cm
Material: Polyurethane with Guflac finish  
Edition: 169
Other: Design object, coat stand . 
Numbered. 

Paul Smith is a British fashion designer. Together with the Italian furniture brand Gufram he created "Psychodelic Cactus", a new version of the cactus-shaped coat stand first launched in the 1970s. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (184, N'Paul Smith / Gufram - Psychodelic Cactus (2016)', N'', 42, N'Size: 70 x 70 x 170 cm
Material: Polyurethane with Guflac finish  
Edition: 169
Other: Design object, coat stand . 
Numbered. 

Paul Smith is a British fashion designer. Together with the Italian furniture brand Gufram he created "Psychodelic Cactus", a new version of the cactus-shaped coat stand first launched in the 1970s. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (185, N'Maurizio Cattelan - The End (marble)', N'', 42, N'2016
Size: 36 x 60 cm
Material: Polyurethane 
Method: Object / Design: Stool 
Edition: 500 
Other: 60 x 36 x 22 cm.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (186, N'Maurizio Cattelan - The End (granite)', N'', 42, N'2016
Size: 36 x 60 cm
Material: Polyurethane 
Method: Object / Design: Stool 
Edition: 1000 
Other: 60 x 36 x 22 cm. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (187, N'Maurizio Cattelan - The End (granite)', N'', 42, N'2016
Size: 36 x 60 cm
Material: Polyurethane 
Method: Object / Design: Stool 
Edition: 1000 
Other: 60 x 36 x 22 cm. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (188, N'Fabio Novembre, Gufram - Jolly Roger Chair ', N'', 42, N'2013
Size: 74 x 85 cm
Material: Polyethylene 
Other: Armchair, Indoor & Outdoor. Motif back: skull, motif front: world map. 

Homage to the Jolly Roger skull-and-bones flag of pirates. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (189, N'Studio Job, Supergufram - "Punch a Wall"', N'', 42, N'2017
Size: 60 x 160 cm
Material: Polyurethane with Guflac-finish 
Method: Sculpture, punching bag 
Edition: 7 
Other: Hand-finished ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (190, N'Studio Job, Supergufram - Punch a Wall', N'', 42, N'2017
Size: 60 x 160 cm
Material: Polyurethane with Guflac-finish 
Method: Sculpture, punching bag 
Edition: 7 
Other: Hand-finished ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (191, N'Christo - Wrapped Volkswagen (2013)', N'', 42, N'Wrapped Volkswagen (PROJECT FOR 1961 VOLKSWAGEN BEETLE SALOON)

Christo, 2013

Size:71 x 55.8 cm
Material: Paper, Cloth 
Method: Collage print with cloth-wrapped Volkswagen and paint. 
Edition: 160 
Other: Edition: 160 with Arabic numerals and 90 with Roman numerals. Signed, dated and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (192, N'Christo - Wrapped Snoopy House (2004)', N'', 42, N'Size: 61.27 x 54.92 cm
Material: Paper 
Method: Lithograph, Collage 
Edition: 250 
Other: Signed and dated. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (193, N'Between two Souls ', N'11.000', 12, N'Ali Zülfikar was born in 1971 in Milelis in Turkey and studied art at Firat University under Professor Memduh Kuzay. His success began early, with his work already curated since 1993 at meanwhile over 170 international exhibitions and museums. We are delighted to display Ali Zülfikar’s masterly portrait drawings on canvas and handmade paper, drawings that he never reworks, this being a distinguishing feature of his approach.

 

It is the highest goal of a portraitist to understand and make visible the soul and life of a person, and this is something that Zülfikar achieves in a unique way: It is above all the pictures of old people that unfurl in his hands an emotional intensity that comprehends the wisdom and dignity of lived experience and bring to the topic of old age a novel and positive serenity. As a master of his craft, he develops in his drawings detailed structures that, seen from a distance, create a strong presence, while viewed close-up there is a tension between the spatial depth and proportions which generates the effect of relief by using layers and ruptures in the flow of the lines. 

Most of those portrayed arouse the sympathy of the observer. Either it is the wakeful eyes that attract or else a tension in the features that signals living energy. In the case of “Oo No!” both apply. The faces of the women have often something warming, motherly, patient, although it is difficult to say what it is exactly that elicits this perception. But one finds oneself stopping to admire, too, “Genuß/Pleasure”, which is the portrait of an engaging, smiling woman with surprisingly soft features.

 

The outstanding feature of Picasso’s wonderful head is clearly the striking physiognomy in which his determination is reflected. A vigorous curiosity speaks from his large eyes. Less trust-inspiring contemporaries, too, provoke fascination such as “Curse of the Caribbean”. 

 

Seen in profile, the old unshaven man has his mouth open. Must one fear him, or is this an unkempt person who deserves compassion?

 

It is Ali Zülfikar’s achievement that his pictures provoke such questions. His portraits pull the observer into the stories that they tell – through their eyes, their facial expressions, their tense features. He is without doubt a great draughtsman whose understanding for the unmistakeable lifelines of people is undeniable.

 

Ali Zülfikar brings his oversized portraits up close to the observer and shows them in an unusually intense light: each furrow or scar, each ever so fine hair, is captured in the drawings. By presenting his portraits in oversize dimensions, and also strongly modulated, he creates a monument of human feeling and a lived life.

Dr. Stephanie Eckhardt | Cologne, 06/06/2016
Tranlation: Paul Gregory

 
', 97)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (194, N'Fluch der Karibik', N'12.000', 12, N'Ali Zülfikar was born in 1971 in Milelis in Turkey and studied art at Firat University under Professor Memduh Kuzay. His success began early, with his work already curated since 1993 at meanwhile over 170 international exhibitions and museums. We are delighted to display Ali Zülfikar’s masterly portrait drawings on canvas and handmade paper, drawings that he never reworks, this being a distinguishing feature of his approach.

 

It is the highest goal of a portraitist to understand and make visible the soul and life of a person, and this is something that Zülfikar achieves in a unique way: It is above all the pictures of old people that unfurl in his hands an emotional intensity that comprehends the wisdom and dignity of lived experience and bring to the topic of old age a novel and positive serenity. As a master of his craft, he develops in his drawings detailed structures that, seen from a distance, create a strong presence, while viewed close-up there is a tension between the spatial depth and proportions which generates the effect of relief by using layers and ruptures in the flow of the lines. 

Most of those portrayed arouse the sympathy of the observer. Either it is the wakeful eyes that attract or else a tension in the features that signals living energy. In the case of “Oo No!” both apply. The faces of the women have often something warming, motherly, patient, although it is difficult to say what it is exactly that elicits this perception. But one finds oneself stopping to admire, too, “Genuß/Pleasure”, which is the portrait of an engaging, smiling woman with surprisingly soft features.

 

The outstanding feature of Picasso’s wonderful head is clearly the striking physiognomy in which his determination is reflected. A vigorous curiosity speaks from his large eyes. Less trust-inspiring contemporaries, too, provoke fascination such as “Curse of the Caribbean”. 

 

Seen in profile, the old unshaven man has his mouth open. Must one fear him, or is this an unkempt person who deserves compassion?

 

It is Ali Zülfikar’s achievement that his pictures provoke such questions. His portraits pull the observer into the stories that they tell – through their eyes, their facial expressions, their tense features. He is without doubt a great draughtsman whose understanding for the unmistakeable lifelines of people is undeniable.

 

Ali Zülfikar brings his oversized portraits up close to the observer and shows them in an unusually intense light: each furrow or scar, each ever so fine hair, is captured in the drawings. By presenting his portraits in oversize dimensions, and also strongly modulated, he creates a monument of human feeling and a lived life.

Dr. Stephanie Eckhardt | Cologne, 06/06/2016
Tranlation: Paul Gregory

', 97)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (195, N'The Cuban', N'16.000', 12, N'Ali Zülfikar was born in 1971 in Milelis in Turkey and studied art at Firat University under Professor Memduh Kuzay. His success began early, with his work already curated since 1993 at meanwhile over 170 international exhibitions and museums. We are delighted to display Ali Zülfikar’s masterly portrait drawings on canvas and handmade paper, drawings that he never reworks, this being a distinguishing feature of his approach.

 

It is the highest goal of a portraitist to understand and make visible the soul and life of a person, and this is something that Zülfikar achieves in a unique way: It is above all the pictures of old people that unfurl in his hands an emotional intensity that comprehends the wisdom and dignity of lived experience and bring to the topic of old age a novel and positive serenity. As a master of his craft, he develops in his drawings detailed structures that, seen from a distance, create a strong presence, while viewed close-up there is a tension between the spatial depth and proportions which generates the effect of relief by using layers and ruptures in the flow of the lines. 

Most of those portrayed arouse the sympathy of the observer. Either it is the wakeful eyes that attract or else a tension in the features that signals living energy. In the case of “Oo No!” both apply. The faces of the women have often something warming, motherly, patient, although it is difficult to say what it is exactly that elicits this perception. But one finds oneself stopping to admire, too, “Genuß/Pleasure”, which is the portrait of an engaging, smiling woman with surprisingly soft features.

 

The outstanding feature of Picasso’s wonderful head is clearly the striking physiognomy in which his determination is reflected. A vigorous curiosity speaks from his large eyes. Less trust-inspiring contemporaries, too, provoke fascination such as “Curse of the Caribbean”. 

 

Seen in profile, the old unshaven man has his mouth open. Must one fear him, or is this an unkempt person who deserves compassion?

 

It is Ali Zülfikar’s achievement that his pictures provoke such questions. His portraits pull the observer into the stories that they tell – through their eyes, their facial expressions, their tense features. He is without doubt a great draughtsman whose understanding for the unmistakeable lifelines of people is undeniable.

 

Ali Zülfikar brings his oversized portraits up close to the observer and shows them in an unusually intense light: each furrow or scar, each ever so fine hair, is captured in the drawings. By presenting his portraits in oversize dimensions, and also strongly modulated, he creates a monument of human feeling and a lived life.

Dr. Stephanie Eckhardt | Cologne, 06/06/2016
Tranlation: Paul Gregory
', 97)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (196, N'The Singer', N'10.500', 12, N'Ali Zülfikar was born in 1971 in Milelis in Turkey and studied art at Firat University under Professor Memduh Kuzay. His success began early, with his work already curated since 1993 at meanwhile over 170 international exhibitions and museums. We are delighted to display Ali Zülfikar’s masterly portrait drawings on canvas and handmade paper, drawings that he never reworks, this being a distinguishing feature of his approach.

 

It is the highest goal of a portraitist to understand and make visible the soul and life of a person, and this is something that Zülfikar achieves in a unique way: It is above all the pictures of old people that unfurl in his hands an emotional intensity that comprehends the wisdom and dignity of lived experience and bring to the topic of old age a novel and positive serenity. As a master of his craft, he develops in his drawings detailed structures that, seen from a distance, create a strong presence, while viewed close-up there is a tension between the spatial depth and proportions which generates the effect of relief by using layers and ruptures in the flow of the lines. 

Most of those portrayed arouse the sympathy of the observer. Either it is the wakeful eyes that attract or else a tension in the features that signals living energy. In the case of “Oo No!” both apply. The faces of the women have often something warming, motherly, patient, although it is difficult to say what it is exactly that elicits this perception. But one finds oneself stopping to admire, too, “Genuß/Pleasure”, which is the portrait of an engaging, smiling woman with surprisingly soft features.

 

The outstanding feature of Picasso’s wonderful head is clearly the striking physiognomy in which his determination is reflected. A vigorous curiosity speaks from his large eyes. Less trust-inspiring contemporaries, too, provoke fascination such as “Curse of the Caribbean”. 

 

Seen in profile, the old unshaven man has his mouth open. Must one fear him, or is this an unkempt person who deserves compassion?

 

It is Ali Zülfikar’s achievement that his pictures provoke such questions. His portraits pull the observer into the stories that they tell – through their eyes, their facial expressions, their tense features. He is without doubt a great draughtsman whose understanding for the unmistakeable lifelines of people is undeniable.

 

Ali Zülfikar brings his oversized portraits up close to the observer and shows them in an unusually intense light: each furrow or scar, each ever so fine hair, is captured in the drawings. By presenting his portraits in oversize dimensions, and also strongly modulated, he creates a monument of human feeling and a lived life.

Dr. Stephanie Eckhardt | Cologne, 06/06/2016
Tranlation: Paul Gregory', 97)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (198, N'ARMX', N'2700', 42, N'ARTIST: ARMX

TITLE: Police Jesus

YEAR: 2017

MEDIUM: Spraypaint on Wood with special Ink technique

SIZE: 80x120cm (32×47 inch.)

EDITION: 1

SIGEND: YES

NUMBERED:

FRAMED:

COA: Yes', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (199, N'Banksy ', N'23000', 42, N'Artist: Banksy

Year: 2007

Title: Trolley Hunter (color)

Size:  76,2 cm x 55,9cm (30 x 22 inch)

Edition: 750 (POW)

numbered: 720/750

Medium: Screenprint on paper

Signed: yes

COA: yes', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (200, N'Otto Schade  - The Cyclist', N'3200', 42, N'Artist: Otto Schade

Year: 2014

Title: cyclist

Size: 100cm x 100cm (39,37 x 39,37 inch)

Medium: Stencil spray paint mixed technique on canvas

Edition: 2/5 every unique

Signed: yes

framed: no

COA: yes', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (201, N'Blek Le Rat - David with kalashnikov', N'37500', 42, N'Artist: Blek le Rat

Year: 2015

Title: David with Kalashnikov

Size: 244  cm x 200 cm  (96,06 x 78,74 inch)

Medium: Spraypaint on canvas

Edition: 1/1

Signed: yes

Framed: yes

COA: yes

', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (202, N'Francis Tucker', N'2590', 42, N'ARTIST: Francis Tucker

TITLE: Glowing Corn

YEAR: 2014

MEDIUM: spraypaint on canvas

SIZE: 120cm x 100cm (47,24?x 39,37 inch)

EDITION: 1/1

SIGNED: yes

FRAMED: No

COA: yes', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (203, N'C.Mank', N'750', 42, N'ARTIST: c.mank

YEAR: 2016

TITLE: Exterior Views #Love

MEDIUM: Acrylics/ Stencil Spraypaint on Canvas

SIZE: 50 x 50 cm (19.69 x 19.69 inch)

EDITION: 1

FRAMED: yes

SIGNED: yes

COA: yes', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (204, N'Cosmos Sarson - Breakdancin jesis', N'13500', 42, N'Artist: Cosmo Sarson

Year: 2015

Title: Screaming Pope

Medium: Oil on 8mm mdf board

Size: 104 cm  x 145 cm

framed: yes (120 cm  x 161 cm x 5cm)

Signed: yes

COA: yes', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (205, N'Brain M. Viveros - 1942', N'950', 42, N'Artist: Brain M Viveros

Title: 1942

Year: 2018

Size: 51cm x 36cm (20×14 inch)

Medium: Giclee fine art print on this art paper

Edition: 100

Signed: yes

Framed: no

COA: yes', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (206, N'Findac - Taaniko', N'750', 42, N'ARTIST: FIN DAC

TITLE: Taaniko

YEAR: 2018

SIZE: 760mm x 560mm

MEDIUM: 8 Layer Screenprint on art board

EDITION: 100

SIGNED: hand signed and numbered

', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (207, N'Ben Eine - Homeless', N'790', 42, N'Artist: Ben Eine

Year: 2017

Title: Homeless Red

Size: 60 cm x 47cm (24×17 inch.)

Medium: 3 Colour Screenprint on Somerset Satin 300gsm Paper

 

Edition: 100

Framed: no

', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (208, N'D*Face - 15 years in the making', N'18000', 42, N'rtist: D*Face

Year: 2000

Title: Bomb for peace

Edition: 1/1

Size: 40cm x 30cm (15,75×11,81 inch.)

Medium: Spraypaint on panel

COA: yes

Framed: yes

', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (209, N'D*Face - 15 years in the making', N'18000', 42, N'rtist: D*Face

Year: 2000

Title: Bomb for peace

Edition: 1/1

Size: 40cm x 30cm (15,75×11,81 inch.)

Medium: Spraypaint on panel

COA: yes

Framed: yes

', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (210, N'D*Face - 15 years in the making', N'18000', 42, N'rtist: D*Face

Year: 2000

Title: Bomb for peace

Edition: 1/1

Size: 40cm x 30cm (15,75×11,81 inch.)

Medium: Spraypaint on panel

COA: yes

Framed: yes

', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (211, N'D*Face  - bomb for peace', N'18000', 42, N'rtist: D*Face

Year: 2000

Title: Bomb for peace

Edition: 1/1

Size: 40cm x 30cm (15,75×11,81 inch.)

Medium: Spraypaint on panel

COA: yes

Framed: yes

', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (212, N'D*Face  - bomb for peace', N'18000', 42, N'rtist: D*Face

Year: 2000

Title: Bomb for peace

Edition: 1/1

Size: 40cm x 30cm (15,75×11,81 inch.)

Medium: Spraypaint on panel

COA: yes

Framed: yes

', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (213, N'DFace  - bomb for peace', N'18000', 42, N'rtist: DFace

Year: 2000

Title: Bomb for peace

Edition: 1/1

Size: 40cm x 30cm (15,75×11,81 inch.)

Medium: Spraypaint on panel

COA: yes

Framed: yes

', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (214, N'Pure Evil- Andy Warhols Nightmare', N'11000', 42, N'Artist: Pure Evil

Year: 2014

Title: Andy Warhols Nightmare

Medium: Spraypaint on Canvas

Size: 100 cm x 100cm (36,37 x 36,37)

Edition: (unique)

Signed: yes

framed: no', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (215, N'JR - Lady Ly', N'3000', 42, N'ARTIST: JR

YEAR: 2011

TITLE: 28 millimetres, Portrait of a Generation- Lady LY

MEDIUM: 4 colors lithography printed with Marinoni machine on paper

SIZE: 70 x 100.5 cm. (27.6 x 39.6 in.)

EDITION: 250

SIGNED: yes

FRAMED: no

COA: yes', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (216, N'Nick Walker  - Amour Plated', N'1450', 42, N'ARTIST: Nick Walker

YEAR: 2016

TITLE: Amour plated

MEDIUM:Fine art wood print on 1/2? sustainable Birch, bright white finish

SIZE: 61x61cm – 24?x24? inch.

Edition: 40

SIGNED: yes', 99)
GO
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (217, N'Nick Flatt and Paul Punk', N'6500', 42, N'ARTIST: Nick Flatt & Punkone

YEAR: 2016

TITLE: more

MEDIUM: acrylics an oil on canvas

SIZE: 160 x 120 cm (63?x47,27? inch.)

SIGNED: YES

COA: YES

FRAMED: NO', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (218, N'Nick Flatt and Paul Punk', N'7900', 42, N'ARTIST: Nick Flatt

YEAR: 2017

TITLE: cutout 2 “The Captain”

MEDIUM: Acrylicspray paint on aluminium and Polystyrene panel

SIZE: 90cm x 130cm

SIGNED: YES

COA: YES

FRAMED: NO', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (219, N'Inkie ', N'1050', 42, N'ARTIST: Inkie

YEAR: 2015

TITLE: Ink Nouveau Aqua

SIZE: 50 x  40 cm (20x16inch.)

MEDIUM: spraypaint acrylic and liquid lead on canvas

EDITION: 1

SIGNED: Yes

COA: YES', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (220, N'Copyright', N'12500', 42, N'Copyright - Gabrielle - 2014 - Spraypaint on canvas - 150cm x 100cm - 59,06 x 39,37 - Edition 1/1 - Gallery Ministry of Walls Miami & Cologne - The urban Art Broker -
Copyright – Gabrielle
12.500,00 €
1 in stock

zzgl. shipping

Artist: Copyright

Title: Gabrielle

Year: 2014

Size: 150 x 100 cm

Edition: 1/1

Medium: Sparypaint on canvas

Signed: yes

Framed: no

COA: yes', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (221, N'Otto Schade - Love and Kiss', N'220', 42, N'Artist: Otto Schade

Year: 2017

Title: Kiss and Love

Size: 50cm x 50cm (19,69 x 19,69 inch)

Medium: x/20  screen print Somerset paper 300 grs

Edition: 20

Signed: handsigned

framed: white wooden frame', 99)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (223, N'Kreml-Geheimnisse', N'25.00', 0, N'Kreml-Geheimnisse von Valerij Nikolaevsky | veröffentlicht: 2000, Löcker Verlag in Wien.

Material aus den geheimen Archiv des KGB. Zusammengefasst und mit Literarischen Aspekten versehenes Meisterwerk von Valerij Nikolaevsy (Waleri Nikolajewski). Zum ersten mal herausgegeben im Jahr 2000 Löcker Verlag. Erhältlich nur in deutscher Sprache.
', 95)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (224, N'The Dangerous Patrician', N'14.99', 0, N'The Dangerous Patrician by Valerij Nikolaevsky | published: Mysl`, 1989 Moscow 

Historical Novel about the ancient Rome by famous russian Writer and Poet Valerij Nikolaevsky. This Novel was forbidden to publish about more than 22 years in the Soviet Union. First time published under Gorbachev in the time of the Perestroika in 1989. Available only in Russian language. ', 95)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (225, N'Kreml-Geheimnisse', N'', 0, N'Kreml Geheimnisse by Valerij Nikolaevsky', 95)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (226, N'Kreml-Geheimnisse', N'29..99', 0, N'Valerij Nikolaevsky - Kreml-Geheimnisse | published: 2000 Löcker Verlag| Vienna/Austria

Kreml Geheimnisse - Aus den geheimen Archiven des KGB von Waleri Nikolajewki (Valerij Nikolaevsky). Erhältlich in deutscher Sprache. ', 95)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (227, N'Kreml-Geheimnisse', N'29.99', 0, N'Valerij Nikolaevsky - Kreml-Geheimnisse | veröffentlicht: 2000, Löcker Verlag, Wien.

Kreml-Geheimnisse - Aus den geheimen Archiven des KGB 

Mehr Informationen zum Autor:  


https://de.wikipedia.org/wiki/Waleri_Michailowitsch_Nikolajewski', 95)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (229, N'Test test', N'', 0, N'Only for Test reasons only', 95)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (233, N'Ruby Anemic - I Need More', N'', 42, N'2012
Size: 70 x 50 cm / 27 x 20 inch
Material: Five Dollar bills on paper. 
Method: Collage 
Edition: 8 
Other: In wood frame black. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (234, N'Ruby Anemic - Trust Me', N'', 42, N'2014
Size: 60 x 50 cm / 23 x 20 inch
Material: One Dollar bills on paper. 
Method: Collage  
Edition: 7 
Other: In wood frame black. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (235, N'Mr. Brainwash - Einstein, 2017', N'', 42, N'Size: 57.1 x 76.2 cm / 22 x 30 inch
Material: Paper 
Method: Stencil and Mixed Media 
Other: Unique, signed. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (238, N'Mr. Brainwash - The King Pelé - Portrait', N'', 42, N'2016
Size: 76.2 x 57.2 cm / 30 x 22 inch
Material: Hand-torn archival art paper 
Method: One color screenprint and acrylic 
Edition: 75 
Other: Individual finish, each piece is unique. Signed by Pelé and Mr. Brainwash on front. Dated and thumbprint by Mr. Brainwash on verso. Sold out at the artist. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (239, N'Mr. Brainwash - The King Pelé - Football', N'', 42, N'2016
Size: 57.2 x 57.2 cm / 22 x 22 inch
Material: Hand-torn archival art paper  
Method: One color screenprint and acrylic  
Edition: 75 
Other: Individual finish, each piece is unique. Signed by Pelé and Mr. Brainwash on front. Dated and thumbprint by Mr. Brainwash on verso. Sold out at the artist.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (240, N'Peter Doig - Fisherman Boys', N'', 42, N'2013
Size: 64 x 86 cm / 25 x 34 inch
Material: Somerset Photo Rag 300gsm 
Method: Archival digital pigment print 
Edition: 500 
Other: Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (241, N'Peter Doig - Untitled (Canoe)', N'', 42, N'2008
Size: 75 x 64.9 cm
Method: Aquatinta 
Edition: 500 
Other: 	igned and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (242, N'Peter Doig - Fisherman', N'', 42, N'2013
Size: 87.5 x 69 cm
Material: Paper 
Method: Lithograph 
Edition: 500 
Other: Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (243, N'Robert Motelski, Light 27.08.15:14, 2016', N'3.500', 42, N'Oil and acrylic on canvas
35 2/5 × 51 1/5 in
90 × 130 cm
Backside signed', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (244, N'Robert Motelski, Light 27.08.15:14, 2016', N'3.500', 42, N'Oil and acrylic on canvas
90 × 130 cm
Backside signed', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (245, N'Robert Motelski, Light 27.08.15:14, 2016', N'3.500', 42, N'Oil and Acrylic on canvas 90x130 cm
Backside signed', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (246, N'Robert Motelski, Light 27.08.15:14, 2016', N'3500', 42, N'Oil and Acrylic on canvas 90x130 cm
Backside signed', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (247, N'Robert Motelski, Light 27.08.15:14, 2016', N'3.500', 42, N'Oil and Acrylic on canvas 90x130 cm
Backside signed', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (248, N'Robert Motelski, Light 27.08.15:14, 2016', N'3.500', 42, N'Oil and Acrylic on canvas 90x130 cm
Backside signed', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (249, N'Saša Marjanovic "The Boy"', N'700', 42, N'Saša Marjanovic   56x38  Aquarell', 103)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (250, N'Saša Marjanovic "The Boy"', N'700', 42, N'Saša Marjanovic   56x38  Aquarell', 103)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (255, N'Robert Motelski "Mountains 04.03, 17:14" 2017', N'2.500', 42, N'Oil on Canvas
90 × 130 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (256, N'Robert Motelski "Mountains 04.03, 17:14" 2017', N'2.500', 42, N'Oil on Canvas
90 × 130 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (258, N'Robert Motelski, Montains 04.03. 17:14, 2017', N'2.500', 42, N'Oil on Canvas, 90x130 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (259, N'Robert Motelski, Montains 04.03. 17:14, 2017', N'2.500', 42, N'Robert Motelski, Montains 04.03. 17:14, 2017', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (260, N'Robert Motelski, Montains 04.03. 17:14, 2017', N'2.500', 42, N'Oil on Canvas, 90x130cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (262, N'Robert Motelski "Mountains 04.03, 17:14" 2017', N'2.500', 42, N'Oil on Canvas 90x130cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (263, N'Test Art', N'2200', 0, N'dasdasd', 108)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (264, N'Test', N'2200', 0, N'!@#$%^&())Ää, Öö, Üü', 108)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (265, N'TEst', N'22.0', 0, N'sdsdsd', 108)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (269, N'Robert Longo - Riot Cops, 2ß17', N'', 42, N'Size:61 x 56 cm
Material: Hahnemühle Photo Rag Ultra Smooth 305g paper  
Method: Ditone print 
Edition: 30 
Other: Edition: 30 + 10 AP. Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (270, N'Robert Longo - Capitol, 2ß17', N'', 42, N'Size: 56 x 61 cm
Material: Hahnemühle Photo Rag Ultra Smooth 305g paper 
Method: Ditone print 
Edition: 30 
Other: Edition: 30 + 10 AP. Signed and numbered ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (271, N'Matthias Meyer - Rio Beach, 2008', N'', 42, N'Size: 60 x 60 cm / 23 x 23 inch
Material: Canvas 
Method: Oil painting 
Other: Unique, signed and dated. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (272, N'Matthias Meyer - Coral Reef, 2006', N'', 42, N'Unique painting, oil on canvas, signed.
100 x 200 cm.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (274, N'Tomasz Prymon Untitled I 2017', N'950', 42, N'Acrylic on Cardboard 75x100 cm

Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (275, N'Tomasz Prymon Untitled III 2017', N'950', 42, N'Acrylic on Cardboard  93x76 cm 

Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (278, N'Tomasz Prymon Untitled V 2017', N'950', 42, N'Acrylic on Cardboard 90x75 cm

Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (279, N'Tomasz Prymon Untitled IX 2017', N'950', 42, N'Acrylic on Cardboard 75x89 cm

Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (280, N'Tomasz Prymon Infinity I 2010', N'', 42, N'Oil on Canvas 115x200 cm

Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (281, N'Tomasz Prymon Infinity III 2010', N'', 42, N'Oil on Canvas 115x200 cm

Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (282, N'Tomasz Prymon Infinity IV 2010', N'', 42, N'Oil on Canvas 115x200 cm

Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (283, N'Tomasz Prymon Infinoty V 2010', N'', 42, N'Oil on Canvas 115x200 cm,  Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (284, N'Tomasz Prymon Infinity VI 2010', N'', 42, N'Oil on Canvas 115x200 cm,  Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (285, N'Tomasz Prymon Infinity  VIII 2010', N'', 42, N'Oil on Canvas 115x200 cm,  Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (286, N'Tomasz Prymon Infinity IXI 2010', N'', 42, N'Oil on Canvas 115x200 cm,  Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (287, N'Robert Motelski Trees 21 October 14:15, 2015', N'', 42, N'Oil on Canvas 24 × 50 cm, Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (288, N'Robert Motelski "Mountains 12 March 10:54" , 2014', N'', 42, N'Oil on Canvas, 90 × 130 cm, Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (289, N'Robert Motelski Light 21. July 22:09, 2017 ', N'', 42, N'Oil and Acrylic on Canvas,  Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (290, N'Robert Motelski Mountains 04.03, 17:14, 2017', N'', 42, N'Oil on Canvas, 90 × 130 cm, Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (291, N'Robert Motelski Campfire 23 August 21:37, 2017', N'', 42, N'Oil and Acrylic on Canvas,  40 × 53 cm, Kontakt: info@galerie-sandhofer.com', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (292, N'Robert Motelsk , Light 28 August 19:59, 2017', N'', 42, N'Oil and Acrylic on Canvas, 40 × 53 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (293, N'Stillife with Bomb', N'1.900', 12, N'Peter Szalc, Stillife with Bomb, 2015, acrylic on canvas, 100 x 120 cm', 111)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (294, N'Breakfast on the Grass', N'1.700', 12, N'Peter Szalc, Breakfast on the Grass, 2015, öl/ acryl  auf Leinwand, 100 x 100 cm', 111)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (296, N'Armin Rohr, O.T.', N'750', 42, N'Armin Rohr, o.T., 2014, Öl auf Aluminium, 26 x 33 cm', 111)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (297, N'Armin Rohr, O.T.', N'750', 42, N'Armin Rohr, o.T., 2014, Öl auf Aluminium, 26 x 33 cm', 111)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (298, N'Armin Rohr, O.T.', N'950', 42, N'Armin Rohr, o.T., 2014, Öl auf Aluminium, 26 x 34 cm', 111)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (299, N'Jarek Puczel, Awakening, 2015', N'', 42, N'Oil on Canvas, 50 × 70 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (300, N'Jarek Puczel, Hesitation, 2018', N'', 42, N'Oil on Canvas, 60 × 70 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (301, N'Jarek Puczel, Caspars Daughter, 2017', N'', 42, N'Oil on Canvas, 100 × 120 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (317, N'Marcin Ryczek, Space 2015', N'', 42, N'Hahnemuhle Paper ; Digigraphie technology , Edition 15 in Format 40 × 80 cm, Limited Edition 
Signature: Backside, Frontside, Certificate of Authenticity
', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (330, N'Marcin Ryczek', N'', 42, N'"A Man Feeding Swans in the Snow "  2013
Hahnemuhle Paper ; Digigraphie technology ,  Limited Edition 25 in format 45x81 cm
Signature: Backside, Frontside, Certificate of Authenticity', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (331, N'Marcin Ryczek, Secret Window 2014', N'', 42, N'edition 8 in format 50x75 cm Photograph on Hahnemuhle Paper ; Digigraphie technology , Limited edition', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (334, N'Robert Bubel , In Search of Forgotten Places  2015', N'', 42, N'Oil on canvas,  95 × 120 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (335, N'Robert Bubel,  Before Fight 2014 ', N'', 42, N'85x95 cm, Oil on Canvas', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (336, N'Robert Bubel, Blue Sky Over Concrete, 2014', N'', 42, N' 80x95 cm, Oil on Canvas', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (337, N'Robert Bubel, Yesterday, Today, Fin, 2015', N'', 42, N'Oil on Canvas, 45 × 55 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (338, N'Robert Bubel, Mile stones, 2013', N'', 42, N'Oil on Canva, 65 × 75 cm', 102)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (339, N'Bambi - Monadonna', N'', 42, N'Lithograph with diamond dust. 
Signed and numbered.
Edition: 90 
Size: 76.2 x 111.8 cm / 30 x 44 inch', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (340, N'Robert Indiana - The Book of Love #8 (green/blue)', N'', 42, N'Size: 60.96 x 50.8 cm / 24 x 20 inches
Lithograph on paper. 
Edition: 200 
Signed and numbered.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (341, N'Mr. Brainwash - Kate Moss, 2018', N'', 42, N'Size: 86.4 x 106.7 cm / 34 x 42 inch. 
Silkscreen and mixed media on paper. 
Unique artwork, signed on verso.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (343, N'Miles Aldridge - New Utopias #4', N'', 42, N'Year: 2018
Size: 98 x 111 cm / 38 x 43 inch. 
Screen-print in colours with silver ink on 410gsm Somerset paper.  
Edition: 15 + 3 AP. 
Signed verso. 
Published by Polígrafa Obra Gráfica, Barcelona.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (344, N'Miles Aldridge - New Utopias #3', N'', 42, N'Year: 2018
Size: 87 x 150 cm
Screen-print in colours with silver ink on 410gsm Somerset paper.  
Edition: 15 + 3 AP. 
Signed verso. 
Published by Polígrafa Obra Gráfica, Barcelona.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (345, N'Miles Aldridge - New Utopias #2', N'', 42, N'Year: 2018
Size: 98 x 111 cm / 38 x 43 inch. 
Screen-print in colours with silver ink on 410gsm Somerset paper.  
Edition: 15 + 3 AP. 
Signed verso. 
Published by Polígrafa Obra Gráfica, Barcelona.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (346, N'Miles Aldridge - New Utopias #1', N'', 42, N'Year: 2018
Size: 98 x 111 cm / 38 x 43 inch. 
Screen-print in colours with silver ink on 410gsm Somerset paper.  
Edition: 15 + 3 AP. 
Signed verso. 
Published by Polígrafa Obra Gráfica, Barcelona.  ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (347, N'Kenny Scharf - Seeserpent (2017)', N'', 42, N'43 x 31.5 cm. 
Edition: 150. 
Archival Pigment Inks on Moab Entrada Rag Bright 300 gsm fine art paper. 
Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (348, N'Mel Ramos - Martini Miss, 1995', N'', 42, N'75 x 57 cm. 
Lithograph. 
Edition of 199. 
Signed, dated and numbered on front.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (349, N'Alex Katz - Tree, 2006', N'', 42, N'Oil painting on board. Signed and dated.
30,5 x 22,8 cm.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (350, N'Shepard Fairey - Eelus, Raven Haired', N'', 42, N'2008
Size: 50 x 69 cm / 20 x 27 inch
Method: Screenprint 
Edition: 200 
Other: Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (351, N'Shepard Fairey - Enchanting Sounds Album', N'', 42, N'2011. 
Silkscreen on cotton rag archival paper with deckled edges. 
Rare piece. 
Edition: 40. 
Signed and numbered.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (352, N'Robert  Longo - Study of the Capitol', N'', 42, N'Year: 2012
Size: 60.5 x 48.3 cm / 24 x 19 inch
Material: Vellum 
Method: Charcoal and ink 
Other: 	Signed, titled and dated in pencil. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (353, N'Jeff Koons - Balloon Venus, 2013', N'', 42, N'Material: Polyurethane Resin 
Method: Sculpture 
Edition: 650 
Other: 24,21 x 12,8 x 13,98 in / 61,5 x 32,5 x 35,5 cm ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (354, N'Jeff Koons - Puppy Vase, 1998', N'', 42, N'Size: 26.7 x 44.5 cm
Material: Porcelaine 
Method: Vase, glazed 
Edition: 3000 
Other: Signed and numbered in print. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (355, N'Jeff Koons - Split-Rocker Vase, 2012', N'', 42, N'Size:	6 x 40 cm
Material: Porcelain 
Method: Sculpture Vase 
Edition: 3500 
Other: 36 x 40 x 33 cm. Numbered, Signature engraved. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (356, N'Jeff Koons - Balloon Swan', N'', 42, N'Balloon Swan yellow (Celebration Serie)

Jeff Koons, 2017
Material: Porcelain 
Method: Sculpture with chromatic coating 
Edition: 999 
Other: Signed and numbered on COA. Manufacturer:Bernardaud, Limoges. Size: 28 x 21 x 14,5 cm. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (357, N'Jeff Koons - Balloon Dog magenta', N'', 42, N'2016
Size: 6.7 x 26.7 cm
Material: Porcelain 
Method: Chrome coat 
Edition: 2300 
Other: Sculpture attached to plate. Fired signature of Jeff Koons and hand-written edition number on verso. With certificate of authenticity. Produced by Bernardaud in Limoges, France. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (359, N'Roy Lichtenstein - Red Apple, 1983', N'', 42, N'Red Apple, from Seven Apple Woodcuts Series (C. 196).  
Size: 94.3 x 76.8 cm / 37 x 30 inch. 
Woodcut in colors on handmade Iwano Kizuki Hosho paper. 
Signed and dated in pencil. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (360, N'Mel Ramos - Lola Cola 5, 2018', N'', 42, N'Size: 58.5 x 80.5 cm / 23 x 31 inch 
Material: 330 g Revere-Magnani paper 
Method: Lithograph in 30 colors  
Edition: 120  
Other: Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (361, N'Mel Ramos - Black Cat, 2018', N'', 42, N'Size: 71 x 100 cm / 28 x 39 inch
Material: Revere-Magnani paper 330g 
Method: Silkscreen in colors 
Edition: 120 
Other: Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (362, N'Bozena Bosko - SALE (2009)', N'', 42, N'Acrylic on canvas. Framed. 200 x 150 cm.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (364, N'XOOOOX - Night Rider (Messenger)', N'', 42, N'2018
Spray paint on metal. 
Unique. 
Signed on front lower left. 
Titled, signed and dated on verso. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (365, N'Raqib Shaw - Midsummer Night`s Dream', N'', 42, N'2017
31 x 25 cm
Hand-colored etching. 
Edition of 30. 
Each piece is unique. 
Signed.', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (366, N'Digitalis 2', N'', 42, N'Sebastian Fritzsch - Digitalis 2 - 2015 - inkt on paper, wood, glas', 114)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (367, N'Jenseits der Vermittler', N'', 42, N'Patrick Niemann - Jenseits der Vermittler - oil on canvas, wood framed - 2016 photography: Jochen Seelhammer', 114)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (368, N'Mel Ramos - Drawing Lesson (2016)', N'', 42, N'84 x 80 cm / 33 x 31 inch
Edition: 120.
Lithograph in 32 colors on BKF Rives paper. 
Numbered and hand-signed by the artist. ', 64)
GO
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (369, N'Damien Hirst, 2012,  Ammonium Sulfamate', N'', 42, N'Size: 15.2 x 15.2 cm / 6 x 6 inch
Material: 410g Somerset White Paper 
Method: Woodcut 
Edition: 55 	
Signed by the artist. Numbered on verso. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (370, N'Alex Katz - Three Trees, 2018', N'', 42, N'Size: 150 x 94 cm / 59 x 37 inch
Material: Paper 
Method: Silkscreen 
Edition: 60 
Other: Signed and numbered. ', 64)
INSERT [dbo].[ArtMaster] ([ArtID], [ArtName], [Price], [SubCategoryId], [Description], [RegID]) VALUES (371, N'Horst Antes - Figur 1000, 1987', N'7800', 42, N'Steel sculpture with gravure and natural rust patina. 
Numbered and signed. 
Hight: 220 cm. Size base plate: 90 x 70 cm. ', 64)
SET IDENTITY_INSERT [dbo].[ArtMaster] OFF
SET IDENTITY_INSERT [dbo].[CategoryMaster] ON 

INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [CategoryImage]) VALUES (1, N'Art', N'Das Bild.jpg')
INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [CategoryImage]) VALUES (2, N'Music', N'600.jpg')
INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [CategoryImage]) VALUES (3, N'Fashion & Design', N'IMG_4960-600x600.jpg')
INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [CategoryImage]) VALUES (4, N'Film & Theatre ', N'FKMOgBsqkMEl.jpg')
INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [CategoryImage]) VALUES (6, N'Literature', N'artplatt babel.jpg')
SET IDENTITY_INSERT [dbo].[CategoryMaster] OFF
SET IDENTITY_INSERT [dbo].[ContactUs] ON 

INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (4, N'ridvan bektas', N'ridvanbektas0@gmail.com', N'Hi 

how are you

i want to ask you if i can buy this art from you.

best regards')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (8, N'DavidEvikE', N'alex.m2s@mail.ru', N' Do you want something new? Look at this site. Only here the choice of girls for every taste and completely free! They are obedient slaves, they will do everything you say! 
http://vik.shortcm.li/trust#y4')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (9, N'GoveEvikE', N'ermolnik-vasja@rambler.ru', N'Do you! like something new? Open and look at this link. Only there the choice of girls for every taste and completely free! They are good slaves, they will implement anything you order  ! 
http://gov.shortcm.li/kings#T54')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (10, N'coulman', N'sumitcenzic@gmail.com', N'Ciao! Here is  an interesting offers for you. http://bit.ly/2JZwneb')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (11, N'DoveEvikE', N'tenzor@rambler.ru', N'Can you! prefer something extremely new? Take a look at this page. Only here the choice of slaves for every desire and completely free! They are responsible slaves, they will and want perform everything you command ! 
http://gov.shortcm.li/kings#Q34')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (12, N'Lily Power', N'elizabeth@franklyliz.com', N'Hello

Shop Oakley Sunglasses 19.95 dollars only today at https://www.isunglasshut.us

Best,


ArtPlatt artplatt.com')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (13, N'ClieeEvikE', N'laduller28@yahoo.com', N'Do you! prefer something new? Look at this offer. Only here the choice of hot pussy girls for every taste and completely free! They are responsible slaves, they will and want perform everything you order ! 
http://gov.shortcm.li/kings1#M39')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (15, N'KeithFarma', N'gcrcompane@yandex.ru', N'Good afternoon. 
Inquiry. I wrote everything in the app, reply to my email. Thank you in advance. 
 
https://privatlab.com/s/v/BApwl28MZlspwoB0mxMb')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (17, N'Oliverwogma', N'2204774@mail.ru', N'Hello! 
That is 
an important 
offers for you. 
 
http://bit.ly/2P6liNV')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (18, N'GregoryLog', N'pushina.mariya@mail.ru', N'Good day! 
Please note 
an important 
offering for you. 
 
https://llmad.lovenights.net/c/da57dc555e50572d?s1=13606&s2=78781&s3=1&j1=1&j3=1')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (19, N'brothert', N'tyrinari@gmail.com', N'Ciao! There is a good bonus offering for you.  Are you in?  http://bit.ly/2J9beOY')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (20, N'DanielSpuro', N'sibirvivatsv@mail.ru', N'Ciao! Here is  a good offer for you. 
 
http://bit.ly/2EHLb2R')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (21, N'subraman', N'player_safa65@yandex.ru', N'Hello! Look at an amazing - more than 400 incredible games & 300 top online slots waiting for you.  Try and be our next winner. http://bit.ly/2J8CSLX')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (22, N'DavidWrady', N'veret-11@mail.ru', N'Good day! Here is  an amazing offers for you. 
 
http://bit.ly/2EMyKmr')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (23, N'CharlesAudit', N'nurzhamalbu@mail.ru', N'Ciao! 
That is 
a good 
offering for you. 
http://bit.ly/2Pvk3YX')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (24, N'kolbe', N'winkiesmomma@yahoo.com', N'Hello! Behold is  a best - slots, roulette and blackjack games.  Are you in?  http://bit.ly/2J7YEPX')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (25, N'DavidAlort', N'joel.holmquist@svenskakyrkan.se', N'Hi! Please note an amazing offers for you. 
http://digig.datedreamon.com/?utm_source=5bcdf3f8d2f30 
http://bit.ly/2qlvxQz')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (26, N'BarryWet', N'caverken@caverken.se', N' Hi Look what we touch looking inasmuch as you! an entrancingthe hour 
 Neutral click 
 
 
http://facebook.topratinglist.com?11578')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (27, N'BarryWet', N'daniel@brandclub.se', N' Hey a be befittinghit town consign 
 Legal click 
 
 
https://drive.google.com/file/d/1bp_qrVEUz18o6tRGmxoGRz5R_ckCn_zF/preview')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (28, N'DariusBUb', N'china.sales@arenco.com.cn', N'  a in goodoblation 
 To leaning click on the cement below  
 
 
https://drive.google.com/file/d/10oRt2BR0kRbJyH2Iw8xB8hCzclwC2Dtz/preview')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (30, N'DariusBUb', N'info.stenungsund@colorama.se', N' Hi What we inspect here is , agreeabledonation 
 To create eligible click on the coupling in this bailiwick  
 
 
https://drive.google.com/file/d/1T3VwQlExFD0AeKwshPeMHTa5_V7yM8aF/preview')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (31, N'jakeman', N'america1@gmx.net', N'Hello! There is a good offers offer for you.  To qualify click on the link below. http://bit.ly/2J9kYZy')
INSERT [dbo].[ContactUs] ([ContactUsID], [Name], [EmailID], [Message]) VALUES (32, N'OscarAgist', N'christer@aqps.se', N' Hi Look what we be enduring with a view you! pleasantoblation 
 Are you in?  
 
 
https://drive.google.com/file/d/1cbtzr-btIdF13a8oQfM9eWTthaUW76_z/preview')
SET IDENTITY_INSERT [dbo].[ContactUs] OFF
SET IDENTITY_INSERT [dbo].[ImageMaster] ON 

INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (9, N'Rebecca Horn - Zenith of the Ocean II_8.jpg', 85, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (12, N'Die Pantherin_10.jpg', 102, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (15, N'Sono sei piedi_13.jpg', 105, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (16, N'Mark Lammert_16.jpg', 106, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (17, N'John Giorno_17.jpg', 107, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (25, N'Nati (TLO)_25.jpg', 115, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (28, N'Andy Warhol - 25 Cats named Sam_28.jpg', 120, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (30, N'BAMBI - Amy_30.jpg', 122, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (31, N'BAMBI - Amour Icone (David Beckham)_31.jpg', 123, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (32, N'Mr. Brainwash - Life Is Beautiful SIlver_32.jpg', 125, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (33, N'Silver_Signatur.JPG', 125, NULL)
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (34, N'Mr. Brainwash - Life Is Beautiful Light Pink_34.jpg', 126, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (35, N'Mr. Brainwash - Every Day Life (Keith Haring)_35.jpg', 127, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (36, N'Mr. Brainwash - Marilyn Monroe_36.jpg', 128, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (37, N'Mr. Brainwash - Mickey & Minnie (green)_37.jpg', 129, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (38, N'Mr. Brainwash - Mickey & Minnie (green)_38.jpg', 130, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (39, N'Alex Katz - Summer Flowers_39.jpg', 131, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (40, N'Alex Katz - Three Portraits_40.jpg', 132, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (41, N'Alex Katz - Nicole_41.jpg', 133, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (42, N'Robert Longo - Bucephalus_42.jpg', 134, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (43, N'Robert Longo - Rosette Nebula_43.jpg', 135, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (44, N'Julian Opie - Heads - Danielle_44.jpg', 136, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (45, N'Julian Opie - Heads - Bobby_45.jpg', 137, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (47, N'Julian Opie - Heads - Zhiyun_47.jpg', 139, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (48, N'Julian Opie - Heads - Ian_48.jpg', 140, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (49, N'Julian Opie - Nature 1 - Sheep 1_49.jpg', 141, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (50, N'Julian Opie - Nature 1 - Boat 2_50.jpg', 142, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (51, N'Julian Opie - Nature 1 - Pebbles 4_51.jpg', 143, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (52, N'Julian Opie - Nature 2 - Minnows_52.jpg', 144, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (53, N'Enoc Perez - Lever House (Purple)_53.jpg', 145, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (54, N'Mel Ramos - Mint Pattie_54.jpg', 146, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (56, N'Mel Ramos - Coco Cookie_56.jpg', 148, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (57, N'Mel Ramos - Navel Oranges_57.jpg', 149, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (58, N'Mel Ramos - Superman_58.jpg', 150, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (59, N'Mel Ramos - Superman_59.jpg', 151, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (61, N'Mel Ramos - Zebra_61.jpg', 153, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (62, N'Andy Warhol - Flowers III.114_62.jpg', 154, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (63, N'Andy Warhol - Grace Kelly FS II.305_63.jpg', 155, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (64, N'Alex Katz - A Tremor In The Morning_64.jpg', 156, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (65, N'Atsushi Kaga - David Bowie Flash_65.jpg', 157, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (66, N'Atsushi Kaga - Elvis Costello_66.jpg', 158, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (67, N'Atsushi Kaga - Usacchi with owls_67.jpg', 159, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (68, N'Alpenwinde_68.jpg', 164, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (69, N'Act 114_69.jpg', 165, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (70, N'Am Monte Labbro_70.jpg', 166, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (71, N'Painting Have a Look_71.jpg', 168, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (72, N'Erwartungsvoll_72.jpg', 169, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (73, N'Weltengarten_73.jpg', 170, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (74, N'Nora Gres - Zürich (2013)_74.jpg', 171, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (75, N'Nora Gres - Chanel (2013)_75.jpg', 172, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (76, N'Jae Yong Kim - Donut Rush (20015)_76.jpg', 173, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (77, N'Jae Yong Kim - Donut Rush (20015)_77.jpg', 173, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (79, N'Retna - Sacred Dance Of Memories (2017)_79.jpg', 175, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (80, N'Jürgen Teller - Swimming, Glemmingebro, Sweden _80.jpg', 176, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (81, N' Jim Dine, Pinocchio Coming from the Green (2011)_81.jpg', 177, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (82, N'John CRASH Matos - X-Men Portfolio (2000)_82.jpg', 178, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (83, N'Maurizio Cattelan - The End (marble)_83.jpg', 185, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (84, N'Maurizio Cattelan - The End (granite)_84.jpg', 186, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (85, N'Maurizio Cattelan - The End (granite)_85.jpg', 187, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (86, N'Fabio Novembre, Gufram - Jolly Roger Chair _86.jpg', 188, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (87, N'Studio Job, Supergufram - Punch a Wall_87.jpg', 190, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (88, N'Christo - Wrapped Volkswagen (2013)_88.jpg', 191, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (89, N'Christo - Wrapped Snoopy House (2004)_89.jpg', 192, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (90, N'Between two Souls _90.jpg', 193, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (91, N'Fluch der Karibik_91.jpg', 194, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (92, N'The Cuban_92.jpg', 195, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (93, N'The Singer_93.jpg', 196, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (94, N'ARMX_94.jpg', 198, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (95, N'Banksy _95.jpg', 199, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (96, N'Otto Schade  - The Cyclist_96.jpg', 200, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (97, N'Blek Le Rat - David with kalashnikov_97.jpg', 201, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (98, N'Francis Tucker_98.jpg', 202, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (99, N'C.Mank_99.jpg', 203, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (100, N'Cosmos Sarson - Breakdancin jesis_100.jpg', 204, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (101, N'Brain M. Viveros - 1942_101.jpg', 205, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (102, N'Findac - Taaniko_102.jpg', 206, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (103, N'Ben Eine - Homeless_103.jpg', 207, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (104, N'DFace  - bomb for peace_104.jpg', 213, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (105, N'Pure Evil- Andy Warhols Nightmare_105.jpg', 214, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (106, N'JR - Lady Ly_106.jpg', 215, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (107, N'Nick Walker  - Amour Plated_107.jpg', 216, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (108, N'Nick Flatt and Paul Punk_108.jpg', 217, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (109, N'Nick Flatt and Paul Punk_109.jpg', 218, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (110, N'Inkie _110.jpg', 219, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (111, N'Copyright_111.jpg', 220, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (112, N'Otto Schade - Love and Kiss_112.jpg', 221, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (114, N'Kreml-Geheimnisse_113.jpg', 223, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (115, N'The Dangerous Patrician_115.jpg', 224, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (116, N'Kreml-Geheimnisse_116.jpg', 225, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (117, N'Kreml-Geheimnisse_117.jpg', 226, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (118, N'Kreml-Geheimnisse_118.jpg', 227, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (120, N'Test test_120.jpg', 229, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (124, N'Ruby Anemic - I Need More_121.jpg', 233, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (125, N'Ruby Anemic - Trust Me_125.jpg', 234, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (126, N'Mr. Brainwash - Einstein, 2017_126.jpg', 235, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (129, N'Mr. Brainwash - The King Pelé - Portrait_129.jpg', 238, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (130, N'Mr. Brainwash - The King Pelé - Football_130.jpg', 239, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (131, N'Peter Doig - Fisherman Boys_131.jpg', 240, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (132, N'Peter Doig - Untitled (Canoe)_132.jpg', 241, N'Image')
GO
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (133, N'Peter Doig - Fisherman_133.jpg', 242, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (144, N'Test Art_134.png', 263, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (145, N'!@#$%^&())Ää, Öö, Üü.jpg', 263, NULL)
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (146, N'Test_146.png', 264, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (147, N'Üü.png', 264, NULL)
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (148, N'TEst_148.jpg', 265, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (151, N'uXbtludAPEejzWX4bdXWlA.jpg', 269, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (152, N'ZLibW5rPLke6ip5IG0ZHrw.jpg', 270, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (153, N'2M3zmPTvv0mtFh_mv-18Yg.jpg', 271, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (154, N'rhOBel5Y20u2KpdtgysHAg.jpg', 272, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (156, N'-JJHjaVE5kWsCkeU6WO0xg.jpg', 274, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (157, N'NTuhLlTRpkilPSMV-CG8Jg.jpg', 275, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (158, N'8m4sGltBn0SsmtLV7QXyDw.jpg', 278, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (159, N'Y-AK_jFUq0qP-dNHnAKTTg.jpg', 279, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (160, N'WwC0zWZckE2myDYZTqoakQ.jpg', 280, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (161, N'LcXlimswskOREBDPyayDhw.jpg', 281, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (162, N'ZRPmeUaom0WDVmISf_TAbA.jpg', 282, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (163, N'XX1lRXSSWkypn1Ee6fHCrw.jpg', 283, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (164, N'9JX90ur9tkG1d0kzW-j4zw.jpg', 284, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (165, N'KcLnO5_ouUKzDitCAVC2wA.jpg', 285, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (166, N'5SPbMV12oEiPQSNysbCvrg.jpg', 286, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (167, N'-EuW5HIygEOdncn9lnsrRg.jpg', 287, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (168, N'9AYcyVhVTkaKLPu0SosSZA.jpg', 288, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (169, N'7B6_Ib6ONEaLDdh7KqtADw.jpg', 289, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (170, N'xgHpimFcSk2uT9JktQpMAQ.jpg', 290, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (171, N'd46nzi3EzEKReLt4BR1a8g.jpg', 291, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (172, N'IgXpqg6fuUqWbJw1a-LsIQ.jpg', 292, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (173, N'LdFMKso80k-V9kAzc2lM8A.jpg', 293, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (174, N'XkOhYi17kUSU0J-2EAMC-g.jpg', 294, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (176, N'5MSKLfKs306G4q8dkUH31Q.jpg', 299, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (177, N'cYvhKZvI30mquehSDiQvYA.jpg', 300, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (178, N'M-XkNCWj6Ue7GMLzKzjqHQ.jpg', 301, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (181, N'kQeZ20LKA0y3o4W8oEFauw.jpg', 317, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (182, N'89puaxHL1ECXoPQYeYB8Sg.jpg', 330, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (183, N'RHkYIvpzQkaeJF9lH5s4Zg.jpg', 331, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (185, N'bTlTr1dkJU2zB45rt8aFaw.jpg', 334, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (186, N'RyTmEyg9mUqjH4FntOSpew.jpg', 335, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (187, N'BeeDkmTIkEiFCsN00Ab_Mw.jpg', 336, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (188, N'_hG_nbp_GkmuZVebXTSHIA.jpg', 337, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (189, N'lVN0AAnIc0agzPKLxhUoQg.jpg', 338, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (190, N'5dU4eve1nkyT_4TUY2SU9g.jpg', 339, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (191, N'zwRHegA6WkSgs8SnqveAUQ.jpg', 340, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (192, N'tTCPGEV8BUS4WLI-t8xlAg.jpg', 341, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (194, N'PBMzqM-v-ESrtl3zpTAJXg.jpg', 343, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (195, N'jobzwukJeEycUGjwfMjYBQ.jpg', 344, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (196, N'Fl_Y2Mdf7kuXchI_dW-xVQ.jpg', 345, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (197, N'5yNtYjh4jkSFr1oZsd6S9Q.jpg', 346, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (198, N'3y4YhJ0fP0qfd7ztkkITXA.jpg', 347, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (199, N'HJA9bsHyTU-cvKtnUlyDPw.jpg', 348, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (200, N'r1VnKMWrtEamj_EtOcAEQA.jpg', 349, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (201, N'wLTzQC5SuEa9uwnedL7DVw.jpg', 350, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (202, N'wKtUe-rmH0WvGHM_WVfh-A.jpg', 351, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (203, N'C6HHHOnogUqnNtTF5gBI0w.jpg', 352, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (204, N'_OctFKjTPEWxTAyXPJ-KdQ.jpg', 353, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (205, N'SIPxTJHnXkyNZ4c4iXnvwA.jpg', 354, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (206, N'V-It1GRSOEO5YTzIaCTB8Q.jpg', 354, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (207, N'i91FDKtIa0G-DXYQhiquKg.jpg', 355, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (208, N'uly81Ris6EKoAImtaSCAYg.jpg', 356, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (209, N'rJ-3xybNbkSow9vnp8HDCQ.jpg', 357, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (211, N'UOMxxIVhx0yl73_97ih6nQ.jpg', 359, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (212, N'agUcaAJzDkeEYnWiUwcPWQ.jpg', 360, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (213, N'NGR17GtUV0qhBL2BUe3N7A.jpg', 361, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (214, N'2muwEVBr70S8RB0h_WZEyg.jpg', 362, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (215, N'ONTTqdQGoUqM5hJ-J9NPTA.jpg', 364, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (216, N'B7F3kKlzjE24CmOh9sqhCg.jpg', 365, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (217, N'z1QqtY7SPE2AYX31K-Th0w.jpg', 366, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (218, N'iKHSNX5Caka-h99X64GLdQ.jpg', 367, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (219, N'c4jGB7xMt0Szb9sud3M-UQ.jpg', 368, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (220, N'h7nVZVBNE0eWF6GVwEzdWQ.jpg', 369, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (221, N'mmCVxjHbukmnMWgLcfKRgQ.jpg', 370, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (222, N'B3GvltU6Gk-yeFZhXDugpQ.jpg', 371, N'Image')
INSERT [dbo].[ImageMaster] ([ImageID], [ArtPath], [ArtID], [FileType]) VALUES (223, N'KDIxZCLQ8UagJO_OQ84UnA.jpg', 371, N'Image')
SET IDENTITY_INSERT [dbo].[ImageMaster] OFF
SET IDENTITY_INSERT [dbo].[LocationMaster] ON 

INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (1, N'22.9797708', N'72.49271079999994', 12)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (2, N'23.022505', N'72.57136209999999', 13)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (3, N'22.3038945', N'70.8021599', 14)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (4, N'22.99619', N'72.52350920000003', 16)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (5, N'23.022505', N'72.57136209999999', 18)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (6, N'48.2081743', N'16.37381890000006', 19)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (7, N'23.022505', N'72.57136209999999', 20)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (8, N'23.022505', N'72.57136209999999', 24)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (9, N'23.022505', N'72.57136209999999', 25)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (10, N'48.2081743', N'16.37381890000006', 28)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (11, N'', N'', 30)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (12, N'48.2081743', N'16.37381890000006', 31)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (13, N'48.2081743', N'16.37381890000006', 35)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (14, N'48.2081743', N'16.37381890000006', 37)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (15, N'48.2081743', N'16.37381890000006', 38)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (16, N'48.2042286', N'16.369008500000064', 42)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (17, N'48.2046992', N'16.368182400000023', 43)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (18, N'48.1179542', N'16.566261199999985', 45)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (19, N'', N'', 49)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (20, N'48.2202018', N'16.396979699999974', 50)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (21, N'', N'', 53)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (22, N'48.22228339999999', N'16.402355100000023', 54)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (23, N'23.0422422', N'72.63433520000001', 55)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (24, N'48.2081743', N'16.37381890000006', 56)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (25, N'', N'', 57)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (26, N'34.0522342', N'-118.2436849', 58)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (27, N'48.17842779999999', N'16.32668149999995', 59)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (28, N'47.051785', N'15.422191', 60)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (29, N'47.2655769', N'11.3914307', 61)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (30, N'48.207025', N'16.378470', 62)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (31, N'48.2041341', N'16.356405799999948', 63)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (32, N'47.449238', N'12.392541', 64)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (33, N'48.20636349999999', N'16.37763749999999', 65)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (34, N'23.022505', N'72.57136209999999', 66)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (35, N'48.200937', N'16.395535', 67)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (36, N'', N'', 68)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (37, N'48.20738579999999', N'16.369232900000043', 69)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (38, N'47.055014', N'8.313224', 71)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (39, N'23.022505', N'72.57136209999999', 72)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (40, N'', N'', 73)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (41, N'', N'', 74)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (42, N'48.3666391', N'10.919083699999987', 75)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (43, N'49.4643308', N'11.074607300000025', 76)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (44, N'47.07104440000001', N'15.426249900000016', 77)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (45, N'53.5488353', N'10.002390399999967', 78)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (46, N'53.5414944', N'9.994794800000022', 79)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (47, N'52.52667', N'13.396449999999959', 80)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (48, N'48.2069317', N'16.35198259999993', 81)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (49, N'23.0734262', N'72.62657100000001', 82)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (50, N'23.0734262', N'72.62657100000001', 83)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (51, N'48.7773463', N'9.162517999999977', 84)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (52, N'48.1943829', N'16.323514299999942', 85)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (53, N'49.75196', N'6.628940000000057', 86)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (54, N'51.05040880000001', N'13.737262099999953', 87)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (55, N'51.3396955', N'12.373074699999961', 88)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (56, N'51.0632105', N'13.75589059999993', 89)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (57, N'50.93751959999999', N'6.954002400000036', 90)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (58, N'50.9363787', N'6.93187039999998', 91)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (59, N'51.51335', N'7.1008299999999735', 92)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (60, N'23.022505', N'72.57136209999999', 93)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (61, N'51.05040880000001', N'13.737262099999952', 94)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (62, N'48.19617760000001', N'16.319018899999946', 95)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (63, N'50.9479272', N'6.918510100000049', 96)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (64, N'50.9557008', N'6.893440300000066', 97)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (65, N'51.71364', N'8.752089999999953', 98)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (66, N'50.937531', N'6.960278600000038', 99)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (67, N'23.022505', N'72.57136209999999', 100)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (68, N'47.7894742', N'13.050096199999984', 102)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (69, N'47.8035912', N'13.045509300000049', 103)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (70, N'47.516231', N'14.550072', 104)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (71, N'51.4501187', N'7.239285099999961', 105)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (72, N'51.42904919999999', N'6.769809799999962', 106)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (73, N'47.516231', N'14.550072', 107)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (74, N'23.022505', N'72.57136209999999', 108)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (75, N'48.2301769', N'16.372017899999946', 109)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (76, N'51.4918354', N'6.86029510000003', 110)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (77, N'50.7246519', N'7.09657059999995', 111)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (78, N'51.5201485', N'7.448730100000034', 112)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (79, N'48.2081743', N'16.37381890000006', 113)
INSERT [dbo].[LocationMaster] ([LocationID], [LocLat], [LocLong], [RegID]) VALUES (80, N'50.937531', N'6.960278600000038', 114)
SET IDENTITY_INSERT [dbo].[LocationMaster] OFF
SET IDENTITY_INSERT [dbo].[Message] ON 

INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (1, 41, 49, N'hey', CAST(N'2017-09-18 18:01:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (2, 49, 41, N'fgfg', CAST(N'2017-09-18 18:03:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (3, 49, 41, N'hello
', CAST(N'2017-09-18 18:05:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (4, 41, 49, N'hello', CAST(N'2017-09-18 18:11:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (5, 41, 49, N'hi
', CAST(N'2017-09-18 06:00:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (6, 49, 41, N'hello
', CAST(N'2017-09-18 06:01:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (7, 49, 41, N'Hi 
', CAST(N'2017-09-18 06:03:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (8, 49, 41, N'jafar', CAST(N'2017-09-18 06:06:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (9, 41, 49, N'I''m Che ', CAST(N'2017-09-18 06:06:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (10, 49, 41, N'vghvtgyviyuvyvygyguguihguhghuhhi
', CAST(N'2017-09-18 06:06:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (11, 41, 49, N'Hi 
', CAST(N'2017-09-18 06:08:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (12, 41, 49, N'yuyu', CAST(N'2017-09-18 07:14:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (13, 40, 49, N'Hallo', CAST(N'2017-09-18 08:11:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (14, 40, 49, N'hi 
', CAST(N'2017-09-19 13:29:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (15, 49, 41, N'cdd', CAST(N'2017-09-20 11:23:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (16, 41, 49, N'cdcd', CAST(N'2017-09-20 11:23:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (17, 49, 41, N'ddfdf', CAST(N'2017-09-20 11:38:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (18, 41, 49, N'nbn', CAST(N'2017-09-20 11:41:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (19, 49, 41, N'l', CAST(N'2017-09-20 11:43:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (20, 41, 49, N'dsds', CAST(N'2017-09-20 11:46:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (21, 49, 41, N'kol', CAST(N'2017-09-20 11:49:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (22, 49, 41, N'yasin', CAST(N'2017-09-20 13:43:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (23, 50, 49, N'Hi how are you. Everything all right.', CAST(N'2017-12-19 13:10:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (24, 49, 41, N'hi                                                 ', CAST(N'2017-12-20 04:46:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (25, 49, 41, N'sasasswasasasasas
asdsds', CAST(N'2017-12-20 05:03:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (26, 49, 41, N'hhhh
jklkjlj', CAST(N'2017-12-20 05:08:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (27, 48, 49, N'hi', CAST(N'2017-12-21 11:23:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (28, 48, 49, N'hello', CAST(N'2017-12-21 11:23:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (29, 41, 49, N'hello', CAST(N'2017-12-21 11:27:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (30, 41, 49, N'geeee', CAST(N'2017-12-21 11:28:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (31, 41, 49, N'het', CAST(N'2017-12-21 11:37:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (32, 48, 49, N'hello', CAST(N'2017-12-21 13:21:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (33, 48, 49, N'hey', CAST(N'2017-12-21 13:21:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (34, 49, 41, N'dsd', CAST(N'2017-12-21 13:23:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (35, 49, 41, N'sdsdsdsdsdsdsdsdsdsdsds', CAST(N'2017-12-21 13:24:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (36, 48, 49, N'dsdsd', CAST(N'2017-12-21 13:24:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (37, 49, 49, N'hello', CAST(N'2017-12-21 13:25:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (38, 49, 48, N'fsdfsdfdsf', CAST(N'2017-12-21 13:26:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (39, 49, 48, N'dasdasdasdasdasdasdasd', CAST(N'2017-12-21 13:40:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (40, 58, 57, N'hey', CAST(N'2018-01-11 08:23:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (41, 58, 57, N'helo', CAST(N'2018-01-11 08:23:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (42, 58, 49, N'hello Ali', CAST(N'2018-01-11 10:30:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (43, 58, 49, N'test both', CAST(N'2018-01-13 08:09:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (44, 58, 49, N'hi', CAST(N'2018-01-13 08:16:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (45, 58, 49, N'hi', CAST(N'2018-01-13 08:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (46, 49, 58, N'hi
', CAST(N'2018-01-13 08:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (47, 49, 41, N'j', CAST(N'2018-01-13 08:25:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageID], [SenderID], [RecieverID], [Message], [TimeStamp]) VALUES (48, 49, 58, N'dafsdg', CAST(N'2018-01-18 08:16:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Message] OFF
SET IDENTITY_INSERT [dbo].[RegistrationMaster] ON 

INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (40, N'Ridvan', N'Bektas', N'06601111225', N'Ridvan_Bektas_40.png', 0, N'', N'ridvanbektas0@gmail.com', N'NzQzODI2ISY/', N'Wien, Österreich', 6, N'', 1, 51)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (50, N'Eliah', N'Nikolaev', N'', NULL, 0, N'', N'nikolaev@artplatt.com', N'dmFsZXJpajU=', N'Ybbsstraße 4, Wien, Österreich', 6, N'', 1, 51)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (51, N'Jackson', N'Slim', N'', NULL, 3, N'', N'ellia_urucun@hotmail.com', N'dmFsZXJpajU=', N'Praterstraße 45', 6, N'', 1, 51)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (52, N'Lino', N'Mino', N'', NULL, 3, N'', N'carambooly@hotmail.com', N'dmFsZXJpajU=', N'Praterstraße 44, 1020 Wien', 6, N'', 1, 51)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (55, N'Met', N'Leo', N'', N'Met_Leo_53.jpg', 0, N'', N'metLeo@gmail.com', N'MTIzNDU2', N'India Colony, Ahmedabad, Gujarat, India', 6, N'', 1, 15)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (58, N'testArtist', N'Artist', N'9898458545', N'testArtist_Artist_58.png', 0, N'', N'testArtist@gmail.com', N'MTIzNDU2', N'Los Angeles, CA, United States', 6, N'', 1, 51)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (60, N'', N'', N'43316262787', N'__60.jpg', 1, N'kunstGarten', N'kunstGarten@mur.at', N'SXJNaUxlSW4=', N'Payer-Weyprecht-Straße 27, 8020 Graz Autria', 1, N'www.kunstGarten.at develops intercreative scenarios looking at art as a medium with the power to connect people – through inspiration, human interactions, empathy, and tolerance: a meeting space connecting nature and art with science.', 1, 17)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (61, N'', N'', N'00436641614076', N'__61.png', 1, N'Antiquariat Tausch', N'dieter.tausch@aon.at', N'ZGlldGVyLnRhdXNjaEBhb24uYXQ=', N'Adolf-Pichler-Platz 12, Innsbruck, Österreich', 1, N'Antiquariat und Galerie Dieter Tausch.



Allgemein beeideter und gerichtlich zertifizierter Sachverständiger für alte Bücher und alte Grafik.', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (62, N'', N'', N'5134800', N'__62.jpg', 1, N'Club alpha', N'clubalpha@alphafrauen.org', N'Y2x1cGFsZmFTZWl0MzA=', N'Stubenbastei 12-14, 1010 Wien', 1, N'The Club Alpha Gallery for contemporary Art is located in the  heart of Vienna, first district in Stubenbastei 12.
In our exhibitions we frequently present  selected works of female artists, as for we are an association for advancement of women.
Op', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (63, N'', N'', N'4315240976', N'__63.jpg', 1, N'Galerie Hubert Winter', N'office@galeriewinter.at', N'VzEwNzBpZW4=', N'Breite Gasse 17, Vienna, Austria', 1, N'William Anastasi 
Guillaume Bijl 
Mary Ellen Carroll 
Judith Fegerl
Ian Hamilton Finlay 
Marcia Hafif 
Nancy Haynes
Michael Höpfner
Birgit Jürgenssen (Estate) 
Michael Kidner RA 
Tina Lechner
James Lewis
Paul Etienne Lincoln 
Urs Lüthi ', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (64, N'', N'', N'00491728120255', N'__64.jpg', 1, N'GALERE FRANK FLUEGEL', N'info@frankfluegel.com', N'TWF4aU1pbmkxMA==', N'Kitzbühel, Österreich', 1, N'Gallery Frank Fluegel with stores in Nuernberg and Kitzbuehel is focused on original artwork of Pop Art and Contemporary Art like Andy Warhol, Tom Wesselmann, Keith Haring, Mr. Brainwash, BAMBI, Roy Lichtenstein, Julian Opie, Alex Katz and others.', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (67, N'', N'', N'4369912253828', N'__65.jpg', 1, N'Loft 8', N'office@loft8.at', N'dmFuZ29naDIwMTg=', N'Wassergasse 19, Wien, Österreich', 1, N'Contemporary Art Gallery
www.loft8.at', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (68, N'Michael', N'Michlmayr', N'', N'Michael_Michlmayr_68.jpg', 0, N'Fotogalerie-Wien', N'mail@michaelmichlmayr.at', N'VGlua2ExOTY1', N'www.michaelmichlmayr.at', 1, N'The FOTOGALERIE WIEN is a non-profit organization organized as an association for the advancement of artistic photography and new media. The collective acts as an information gallery running independently from trends set by the commercial mainstream.', 1, 49)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (69, N'', N'', N'0043015120915', N'__69.jpg', 1, N'Charim Galerie', N'charim@charimgalerie.at', N'Y2hhcmltMTk5OA==', N'Dorotheergasse 12, Wien, Österreich', 1, N'Charim Galerie Wien was founded 1998 by Miryam Charim.
Since the beginning Miryam Charim puts the focus of her gallery program on well known international artist in exchange with young emerging artist. The gallery represents participants of document', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (70, N'Emanuel', N'Kolar', N'', NULL, 3, N'', N'kolar.emanuel.92@gmail.com', N'RW1lazU3ODI=', N'Strobachgasse, Wien, Österreich', 0, N'', 1, 0)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (71, N'', N'', N'0798661233', N'__71.jpg', 1, N'Galerie Vitrine', N'info@galerie-vitrine.ch', N'VHVybUxpZWJlNA==', N'Stiftstrasse 4, 6006 Luzern, Schweiz', 1, N'Die Galerie Vitrine in Luzern ist offen: offen für Kunstinteressierte, Kunstsammler und Kunstliebhaber.
Wir verstehen uns als Schaufenster des zeitgenössischen Kunstschaffens und freuen uns über Ihren Besuch.', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (75, N'', N'', N'00498218151163', N'__72.jpg', 1, N'Galerie Noah', N'nathalie.bauer@galerienoah.com', N'U3RhcnQyMDE3TkIl', N'Beim Glaspalast 1, Augsburg, Deutschland', 1, N'Die GALERIE NOAH besteht seit 2002 und befindet sich im Glaspalast. Der „Glaspalast“, ein Industriedenkmal von 1909/10, liegt inmitten des "Textilviertels" unmittelbar neben dem neuen Staatlichen Textil- und Industriemuseum Augsburg (tim).

', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (76, N'', N'', N'', N'__76.jpg', 1, N'Galerie PR - Pia Rub', N'pr@piarubner.de', N'VDltMnVrdHUh', N'Kobergerstraße, Nürnberg, Deutschland', 1, N'Die Galerie zeigt seit 2005 zeitgenössische deutsche und internationale Kunst aller Genres. In den bisher mehr als 90 Ausstellungen ist die Galeristin Pia Rubner auch immer mit eigenen Arbeiten vertreten.', 1, 12)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (77, N'', N'', N'00436508171610', N'__77.jpg', 1, N'GalerieBlauesAtelier', N'florinda@florinda.at', N'a2Vzb3BoaWU=', N'Annenstraße 33, Graz, Österreich', 1, N'Galerie für zeitgenössische Kunst in Graz, gegründet von Florinda Ke Sophie, 2005. Malerei, Skulpturen, Installationen, Literatur.', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (78, N'', N'', N'', N'__78.png', 1, N'Evelyn Drewes | Galerie', N'info@evelyndrewes.de', N'QnVyY2hhcmRzdHJhc3NlMTQ=', N'Burchardstraße 14, 20095 Hamburg, Deutschland', 1, N'The gallery´s artistic focus is clearly set on the passionate promotion of young talents. Hosting individual and group exhibitions, the gallery presents paintings of young promising artists and international master-class students from the German Acad', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (79, N'', N'', N'016094816783', N'__79.jpg', 1, N'Nissis Kunstkantine', N'hallo@nissis-kunstkantine.de', N'bmlzc2lza3Vuc3Q=', N'Am Dalmannkai  6, Hamburg, Deutschland', 1, N'Nissis Kunstkantine, nahe Elbphilharmonie in der HafenCity, verbindet Kunst mit Kulinarischem, veranstaltet Events für und mit den Künstlern, versteht sich als Brücke zwischen Kunst und Öffentlichkeit. Die Galerie bietet Kunstberatung, sowie Kunstver', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (80, N'', N'', N'00493051736506', N'__80.png', 1, N'Jordan/Seydoux', N'galerie@jordan-seydoux.com', N'Z2FsZXJpZWJlcmxpbg==', N'Auguststraße 22, 10117 Berlin, Allemagne', 1, N'The gallery Jordan/Seydoux, launched in 2008 in Berlin by Amélie Seydoux and
Bernard Jordan, is specialized in contemporary works on paper: limited editions
and drawings.
', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (81, N'', N'', N'004369911506010', N'__81.jpg', 1, N'Galerie Hochdruck', N'contact@galeriehochdruck.com', N'UmVoNTBnaXck', N'Neudeggergasse 8, 1080 Vienna, Austria', 1, N'Vienna''s only gallery specializing  in original prints from the 15th century to the present day.  Special fields: Woodcut, Expressionism, old master prints, young art, historical art journals and illustrated books. www.galeriehochdruck.com', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (84, N'', N'', N'00491755740042', N'__84.jpg', 1, N'Galerie Merkle', N'horst.merkle@galerie-merkle.de', N'bWFncml0dDU=', N'Breitscheidstraße 48, Stuttgart, Deutschland', 1, N'Die Galerie Merkle organisiert wechselnde Ausstellungen von etablierten Stuttgarter Künstler wie Hannes Steinert, Ulrike Kirbach, Ruth Baumann oder Danielle Zimmermann, genauso sind aber auch nationale wie internationale Künstler vertreten.', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (85, N'Susanna', N'Dinkic', N'', NULL, 0, N'', N'sus.a.nna@hotmail.com', N'YXJ0cGxhdHQxMjM=', N'1150 Wien, Österreich', 1, N'', 1, 12)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (86, N'', N'', N'4906519763840', N'__86.jpg', 1, N'Kunstverein Trier Junge Kunst', N'kunstverein@junge-kunst-trier.de', N'anVrdTIwMThBcnRwbGF0dA==', N'Kunstverein Trier, Karl-Marx-Straße 90, 54290 Trier, Deutsch', 1, N'Seit seiner Gründung im Jahr 1985 entwickelte sich der Kunstverein Trier Junge Kunst von einer Produzentengalerie zu einem öffentlich beachteten Kunstverein, der sich längst als eine tragende Säule des Trierer Kunstgeschehens etabliert hat.', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (87, N'', N'', N'00493513393959', N'__87.jpg', 1, N'Galerie Inspire Art', N'kontakt@inspire-art.de', N'cHJpbWF0ZTc=', N'Dresden, Deutschland', 1, N'Modern Art Gallery for abstract paintings and modern art sculpture exhibition.
Find more Artwork on https://www.inspire-art.de
We are an regional modern art gallery work with an aspiring artist group.
Our philosophy is to support growing artists.', 1, 12)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (88, N'', N'', N'493412229100', N'__88.jpg', 1, N'GRASSI Museum of Applied Arts', N'grassimuseum@leipzig.de', N'R3JAc3NpMjAxOA==', N'Leipzig, Deutschland', 1, N'Grassimuseum, an impressive building complex in Art déco-style, houses three museums of domestic and international significance: the Museum of Applied Arts, the Museum of Ethnology and the Museum of Music instruments. ', 1, 17)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (89, N'', N'', N'00493518113755', N'__89.jpg', 1, N'Kultur Aktiv e. V.', N'galerie@kulturaktiv.org', N'bm9jaDFQYXNzd29ydA==', N'Bautzner Straße 49, Dresden, Deutschland', 1, N'Gallery nEUROPA

The gallery nEUROPA  presents individual positions of international artists and photographers.', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (90, N'', N'', N'004922125899720', N'__90.jpg', 1, N'Galerie Iliev', N'galerie@iliev.de', N'cHJvbGV0', N'Herzogstraße 26, Köln, Deutschland', 1, N'Seit 20 Jahren zeigen wir moderne Kunst. Die Sammlung an Malerei, Grafik, Skulptur, Bronzen wird laufend erweitert und ist ständig zu besichtigen. Die Galerie vertritt Künstler aus der Region und den gesamten Europäischen Raum. Blattgoldrahmen fertig', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (91, N'', N'', N'004922127056840', N'__91.jpg', 1, N'Philipp von Rosen Galerie', N'info@philippvonrosen.com', N'cHZyZzIwMTg=', N'Aachener Straße 65, Köln, Deutschland', 1, N'Philipp von Rosen Gallery is a gallery for contemporary art located in Cologne, Germany, representing German and international contemporary artists in its gallery space as well as at international art fairs. ', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (92, N'', N'', N'492091487461', N'__92.jpg', 1, N'Galerie Jutta Kabuth', N'info@galerie-kabuth.de', N'TVNoTXVpMjFKYQ==', N'Wanner Str. 4, Gelsenkirchen, Deutschland', 1, N'Established in 1996 the gallery  Jutta Kabuth represents contemporary and international Fine Art such as paintings, sculptures, objects, photography, graphic works and installations. ?The?variety of works and artists spans from figurative and abstrac', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (94, N'Boris', N'Hecht', N'', N'Boris_Hecht_94.jpg', 0, N'', N'boris-hecht@gmx.de', N'YW5hc3Rhc2lhMTA3', N'Dresden, Deutschland', 1, N'', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (95, N'Valerij', N'Nikolaevsky', N'', N'Valerij_Nikolaevsky_95.jpg', 0, N'', N'valerij@artplatt.com', N'dmFsZXJpam5pa28=', N'Märzstraße, Wien, Österreich', 6, N'Russian Writer and Poet. 

Deutsche Biografie:



Mit fünfzehn Jahren folgte er einem Aufruf der Kommunistischen Partei der Sowjetunion und zog als Arbeiter nach Altai und Sibirien. Er arbeitete im Ural und in der Arktis als Berg-, Lade- und Bauarbeiter, Schiffsheizer und Seemann. In Tschukotka wurde er zum Sekretär des Komsomol-Komitees gewählt. Wegen seiner Kritik an Nikita Chruschtschows Auftreten vor der 15. UNO-Vollversammlung 1960 wurde er aus dem Komsomol ausgeschlossen und vom KGB beschattet.

Er widmete sich fortan der Kultur und arbeitete als Journalist und Regisseur des Literaturtheaters der Stadt Togliatti. Er stieg zum Vorsitzenden der örtlichen Literaturvereinigung, des „Klubs der kreativen Intelligenz“ und des Rates des „Sowjetischen Fonds der Kultur“ auf. 1987 wurde er zum Leiter des Kunst- und Dokumentationszentrums „Ilja Jefimowitsch Repin“ ernannt. Als Organisator öffentlicher Veranstaltungen in der Sowjetunion wurde er mehrmals ausgezeichnet.

Sein Roman über Russland Az, grešnyj so slovami istiny ... (deutsch: Ich, Sünder, mit den Worten der Wahrheit) wurde 22 Jahre lang nicht herausgegeben und war verboten. Zuerst herausgegeben wurde er unter Gorbatschow, nach dessen Rücktritt jedoch von der Produktion genommen. Sein Buch Kreml-Geheimnisse: Aus den geheimen Archiven des KGB ist auf Grund von politischen Motiven in Russland verboten.

„Das ist der Name des Buches welches in Österreich in deutscher Sprache veröffentlicht worden ist. Der Autor, der berühmte russische Schriftsteller Waleri Michailowitsch Nikolajewski. Die Geschichte des Buches ist voller Dramatik. Es basiert auf Dokumenten, die seit über 30 Jahren vom Schriftsteller Nikolajewski gesammelt wurden, die er vom Stalin-Preisträger Orest Maltsev erhalten hatte. Die meisten Dokumente musste VM Nikolajewski nach einer gewissen Zeit abgeben, da er unter ständiger Beobachtung durch den KGB stand. Nikolajewski war es verboten, das Land zu verlassen. Die Materialien wurden nach England, Dänemark, Deutschland, Schweiz, etc. gebracht und durch seine wahren Freunde verwaltet. Als er in der Lage war, selber nach Europa zu gehen, begann er jene seltenen Dokumente aufzusammeln ... Der auf Fakten basierende, journalistische Roman "Kreml Geheimnisse" wurde sehr schnell in verschiedenen europäischen Ländern verkauft und erhielt allerbeste Kritiken."“

– Aus der russischen Zeitschrift über Wissenschaft, Bildung und Kultur "Alma Mater", Nr. 3 (20), 2000
Er schrieb historische Werke über das antike Rom und das Russische Reich, die in Russland, Deutschland, Österreich, der Schweiz, China, Israel und den USA veröffentlicht wurden. Nach dem Zerfall der Sowjetunion 1991 zog er nach Israel. Nach Zwischenaufenthalten in England, Frankreich, Belgien und Deutschland ließ er sich 1996 in Österreich nieder. 1997 wurde er in Wien am offenen Herzen operiert.

Nikolajewski ist Mitglied der Gemeinschaft der unabhängigen Literaten Russlands, des Vereines der Schriftsteller Israels und des österreichischen P.E.N.-Clubs.

Werke
- Die Republik der Jugend – 1969/Russland, Brjansk
- Silhouetten – 1986/Russland, Moskau – Pravda
-Az, grešnyj so slovami istiny ... (Ich, Sünder, mit den Worten der Wahrheit) – 1990/Russland, Moskau – Mysl
- Strašnyj patricij – 1989/Russland, Moskau – Mysl
- Strašnyj patricij – 1990/Russland, Moskau – Mysl
- Strašnyj patricij – 1991/Israel, Jerusalem – Leksikon
- Strašnyj patricij – 1993–1994/Russland, Togliatti–Moskau – Sowremennik
- Helmuth A. Niederle (Hrsg.): Die Fremde in mir. Lyrik und Prosa der österreichischen Minderheiten und - - Zuwanderer (Anthologie) – 1999/Österreich, Klagenfurt – Hermagoras (Texte von ca. 100 Autoren, die entweder einer sogenannten Minderheit angehören oder mehr oder weniger freiwillig in Österreich leben.)
- Kreml-Geheimnisse: Aus den geheimen Archiven des KGB – 2000/Österreich, Wien – Löcker, ISBN 3-85409-305-5', 1, 39)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (96, N'', N'', N'004915786525646', N'__96.jpg', 1, N'Fritz Böhme galerie eyegenart', N'kontakt@eyegen-art.de', N'Rmlua2Vsc3RlaW4yMDA5', N'Rothehausstraße 14, Köln, Deutschland', 1, N'Fritz Böhme pflegt eine Galerie, die unterschiedlichsten, hauptsächlich zeitgenössischen, KünstlerInnen eine Bühne bietet. Im Rahmen der zahlreichen, abwechslungsreichen Ausstellungen werden ebenso abwechslungsreiche kulturelle Veranstaltungen angeboten, über die bei Facebook genauer informiert wird. 

Besuchen Sie uns gerne auch unter http://www.eyegen-art.de! ', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (97, N'Ali Zulfikar', N'Dogan', N'4915209854527', N'Ali Zulfikar_Dogan_97.jpg', 0, N'', N'art@alizulfikar.com', N'TmVydWRhMjYwMzIwMDZEb2dhbg==', N'Wilhelm-Mauser-Straße 47, Köln, Deutschland', 1, N'', 1, 12)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (98, N'', N'', N'5251280358', NULL, 1, N'Galerie Pictures Point', N'info@galerie-pictures-point.de', N'JFBFSj9HRlo=', N'Karlstraße 13, 33098 Paderborn, Deutschland', 1, N'Einrahmungen vom Original bis zum Selbstgemalten in Aluminium oder Holz', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (99, N'', N'', N'', N'__99.jpg', 1, N'Ministry of Walls Street Art Gallery', N'frank@ministryofwalls.com', N'QVJUbWluaXN0cnkyMDE1', N'Cologne, Deutschland', 1, N'Ministry of Walls is Street Art Gallery in Cologne / Germany ', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (101, N'Alika', N'Noomer', N'', NULL, 3, N'', N'naturcosmeticum@gmx.at', N'c2E3N3NhNzdzYQ==', N'Wien, ???????', 0, N'', 1, 0)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (102, N'', N'', N'06507283828', N'__102.jpg', 1, N'Galerie Sandhofer', N'mail@galerie-sandhofer.com', N'S2FzaWE4', N'Nonntaler Hauptstraße 50, 5020 Salzburg, Österreich', 1, N'Galerie Sandhofer is a modern gallery. The opening of the gallery took place in 2005 on the ground floor of a villa in the middle of the exclusive residential area Saggen in Innsbruck, Claudiastrasse 10. In June 2013, the gallery moved to Salzburg.
The program of the gallery unifies young as well as established contemporary art and presents new approaches in the area of painting, drawing and graphics. The gallery is focused mainly on young, contemporary and figurative art with an emphasis on Poland.', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (103, N'', N'', N'00436644130568', N'__103.jpg', 1, N'Galerie b11', N'info@jazzwe.net', N'bWlja28zMTA1', N'Bergstraße 11, Salzburg, Österreich', 1, N'Kunst Galerie Salzburg, Gemälde, Designer Schmuck, Konzerte, Art Gallery Shop, Online Verkauf', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (104, N'Aadil', N'Keshwani', N'', N'Aadil_Keshwani_104.jpg', 0, N'', N'aadil@imperoit.com', N'YWFkaWwxMjM=', N'Austria', 1, N'Test', 1, 12)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (105, N'', N'', N'0234701027', NULL, 1, N'Galerie Hebler GmbH', N'info@galerie-hebler.de', N'MTU1MT1NRExp', N'Markstraße 77a, 44801 Bochum, Deutschland', 1, N'Kunsthandel  mit zeitgenössischer  Original-Graphik', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (106, N'', N'', N'', N'__106.jpg', 1, N'Museum DKM ', N'mail@museum-dkm.de', N'TURLTTQ1NDc4dHRy', N'Güntherstraße, Duisburg, Deutschland', 1, N'The DKM Museum is located in a quiet side street in central
Duisburg, between Kant Park, LehmbruckMuseum and
the Central Station. Named using the initials of donors and
museum founders Dirk Krämer and Klaus Maas, the private
exhibition space was opened in early 2009. The building is a
former residential and commercial property from the 1960s,
which Swiss architect Hans Rohr transformed into a home
for the arts with over 2,700 square metres of exhibition space.
Titled “Lines of quiet beauty”, the permanent exhibition
shows the result of 40 years of collecting by the museum
founders. The presentation focuses on the comparison of
past and contemporary design language and the dialogue
between traditional Asian and modern European art. Distributed
over 51 rooms and five floors of exhibition space, the
“Lines of quiet beauty” is the order of the day.
All works in the DKM Collection
ask to be seen or looked at in silence.
exhibition includes works from over 40 modern and contemporary
artists from Europe and Asia, Japanese scroll paintings and ceramics
from the 18th and 19th centuries, artifacts from ancient
China, from ancient Egypt, Gandhara, Cambodia and Thailand’s
Ayutthaya Kingdom. The museum also hosts regular temporary
exhibitions related to the focal points of the collection.

Museum DKM; Song Dong, Write your message with water, 2001 
© Stiftung DKM | Foto: Werner J. Hannappel
', 1, 17)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (107, N'chetanTest', N'chetanTest', N'1234567896', NULL, 0, N'', N'chetan@test.com', N'MTIzNDU2', N'Austria', 1, N'testing', 1, 12)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (108, N'New', N'Artisi', N'9898989898', N'TEst_Artist_108.jpg', 0, N'', N'test@gmail.com', N'MTIzNDU2Nzg5', N'Ahmedabad, Gujarat, India', 1, N'Test', 1, 12)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (110, N'', N'', N'00492084124928', N'__110.jpg', 1, N'LUDWIGGALERIE Schloss Oberhausen', N'natascha.kurek@oberhausen.de', N'THVkd2lnMjAxOA==', N'Konrad-Adenauer-Allee 46, Oberhausen, Deutschland', 1, N'In den großzügigen Räumen des klassizistischen Schlosses Oberhausen werden ambitionierte Wechselausstellungen mit Leihgaben aus aller Welt unter drei Themenschwerpunkten präsentiert:

Die LUDWIGGALERIE stellt die Sammlung Peter und Irene Ludwig, eine der größten Privatsammlungen, die über das internationale Netz der Ludwig Museen mit Oberhausen verbunden ist, in kultur- und zeitübergreifenden Themen vor. So wurde 2017 die Ausstellung „Let''s buy it! Kunst und Einkauf“ gezeigt – unter anderem mit Arbeiten von Albrecht Dürer, Andy Warhol oder Gerhard Richter.

Die POPULÄRE GALERIE dagegen widmet sich der Präsentation von Comic, Karikatur, Illustration und Fotografie. Beispielsweise waren 2018 die Arbeiten namenhafter Fotografen wie Helmut Newton, Bert Stern oder Richard Avedon in der Ausstellung „Shoot! Shoot! Shoot!“ zu sehen. Als einziges Haus in Nordrhein-Westfalen, präsentiert das Schloss Oberhausen seit Jahren zentrale Comic-Positionen wie Ralf König oder Walter Moers.

Hingegen beschäftigt sich die LANDMARKENGALERIE  mit dem Strukturwandel des Ruhrgebiets. Das ehemalige Kohle- und Stahlgebiet wandelt sich zum Dienstleistungszentrum. Die Ausstellungsprojekte begleiten diesen Prozess.

Das heutige Ausstellungshaus, 1947 nach dem Zweiten Weltkrieg als erste Galerie im Ruhrgebiet eröffnet, besitzt einen kleinen eigenen Bestand, darunter Arbeiten von Pablo Picasso, Conrad Felixmüller oder Otto Pankok.

Weitere Informationen unter www.ludwiggalerie.de; www.twitter.com/LUDWIGGALERIE;  www.facebook.com/LUDWIGGALERIE; www.instagram.com/ludwiggalerie; www.ludwiggalerie.blogspot.de; 

Öffnungszeiten: Di bis So, 11 bis18 Uhr, montags geschlossen; Eintritt: 8 €, ermäßigt 4 €, Familien (2 Erwachsene + Kinder) 12 €; 

Abb.: LUDWIGGALERIE Schloss Oberhausen © Thomas Wolf', 1, 17)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (111, N'', N'', N'228653230', N'__111.jpg', 1, N'GALERIE SZALC', N'galerie@szalc.de', N'UnVkYTE5NjU=', N'Kurfürstenstraße 31, Bonn, Germany', 1, N'Galerie für Gegenwartskunst', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (112, N'', N'', N'02315348205', N'__112.jpg', 1, N'der kunstbetrieb', N'spieckermann@derkunstbetrieb.de', N'NjRzb21tZXJzYXVzZQ==', N'Gneisenaustraße 30, 44147 Dortmund, Deutschland', 1, N'Wir wollen im Stadtteil den Ideenreichtum, Gestaltungswillen und die Offenheit der Dortmunderinnen und Dortmunder für ästhetische Prozesse und deren Materialisierungen aufgreifen, bedienen und fördern und somit einen Gegenpol setzen zu einem Denken einer bloßen Wirtschaftlichkeit und eines puren Pragmatismus. Die Kunst? Den Künstlerinnen und Künstler der Region wollen wir die Möglichkeit geben, ihre Kunst zu zeigen und zu vermarkten, indem die Bedingungen fair und bestmöglich sind. Der Betrieb? Wir realisieren Treffen, Workshops und Vernetzungen, was den Austausch über und das Machen von Kunst anstoßen und fördern soll. Der Ort?  "der kunstbetrieb " liegt im Dortmunder Hafenviertel. ', 1, 42)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (113, N'Aurora', N'Noir', N'', N'Aurora_Noir_113.jpeg', 0, N'', N'aurora.noir@gmx.at', N'Y2hlbHNlYTAxMjM=', N'Wien, Österreich', 1, N'photographer, based in VIE

dark photography // artistic photography', 1, 49)
INSERT [dbo].[RegistrationMaster] ([RegID], [FirstName], [LastName], [ContactNo], [PhotoPath], [UserType], [CompanyName], [EmailId], [Password], [Address], [CategoryID], [Description], [Status], [SubCategoryID]) VALUES (114, N'', N'', N'', N'__114.jpg', 1, N'DD  55 Gallery', N'info@dd55.gallery', N'anVnZW5kc3RpbDE=', N'Cologne, Duitsland', 1, N'Das Programm der DD 55 Gallery fokusiert sich auf eigenwillige künstlerische Positionen der Gegenwartkunst. Gattungen der bildenden Künste wie Malerei, Bildhauerei, Fotografie und Zeichnungen und deren Überlappungen in den Kunstweken stehen in Dialog und in Spannung zueinander. Hohen Ästhetik wird kontonuerlich mit tiefen  künstlerischen Gedanken verbunden. ', 1, 42)
SET IDENTITY_INSERT [dbo].[RegistrationMaster] OFF
SET IDENTITY_INSERT [dbo].[SubCategoryMaster] ON 

INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (12, N'Painters', 1)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (13, N'Statuary', 1)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (15, N'Tattoo', 1)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (17, N'Museums', 1)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (19, N'Fair', 1)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (20, N'Producer', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (21, N'Producing Company', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (22, N'Actors', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (23, N'Casting Company', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (24, N'Location Leaser', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (25, N'FilmEquipmentRenta', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (26, N'Singers', 2)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (27, N'Songwriter', 2)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (28, N'Producer', 2)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (29, N'DJ''s', 2)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (30, N'Studios', 2)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (31, N'Design', 3)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (32, N'ProductionCompany', 3)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (33, N'Brands', 3)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (34, N'Labels', 3)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (35, N'Models', 3)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (36, N'Model Agency', 3)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (37, N'Tailor', 3)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (38, N'Poets', 6)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (39, N'Writer', 6)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (40, N'Publisher', 6)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (41, N'Producer', 6)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (42, N'Gallery', 1)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (43, N'Director', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (44, N'Screenwriter', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (45, N'Theatre', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (46, N'Opera', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (47, N'Instrumentals', 2)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (48, N'Film Technician', 4)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (49, N'Photographer', 1)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (50, N'Studio', 1)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (51, N'Poetry Slam', 6)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (52, N'Architecture', 3)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (53, N'Dance', 2)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (54, N'Library', 6)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (55, N'Graffiti', 1)
INSERT [dbo].[SubCategoryMaster] ([SubCategoryID], [Name], [CategoryId]) VALUES (56, N'Art Project', 1)
SET IDENTITY_INSERT [dbo].[SubCategoryMaster] OFF
ALTER TABLE [dbo].[ArtMaster]  WITH CHECK ADD  CONSTRAINT [FK_ArtMaster_RegistrationMaster] FOREIGN KEY([RegID])
REFERENCES [dbo].[RegistrationMaster] ([RegID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ArtMaster] CHECK CONSTRAINT [FK_ArtMaster_RegistrationMaster]
GO
ALTER TABLE [dbo].[ImageMaster]  WITH CHECK ADD  CONSTRAINT [FK_ImageMaster_ArtMaster] FOREIGN KEY([ArtID])
REFERENCES [dbo].[ArtMaster] ([ArtID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ImageMaster] CHECK CONSTRAINT [FK_ImageMaster_ArtMaster]
GO
ALTER TABLE [dbo].[SubCategoryMaster]  WITH CHECK ADD  CONSTRAINT [FK_SubCategoryMaster_CategoryMaster] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[CategoryMaster] ([CategoryID])
GO
ALTER TABLE [dbo].[SubCategoryMaster] CHECK CONSTRAINT [FK_SubCategoryMaster_CategoryMaster]
GO
/****** Object:  StoredProcedure [dbo].[sp_adminLogin]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[sp_adminLogin]
(
@Usename Varchar (30),
@word varchar (30)
)
as
Begin
Select COUNT(*)from AdminMaster where EmailID=@Usename and Password=@word 
End


GO
/****** Object:  StoredProcedure [dbo].[sp_artimage]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_artimage]
@ArtID int=null,
@ArtPath varchar(250)=null,
@ImageID int=null,
@Action varchar(20)=null
AS
BEGIN
 if (@Action = 'select') 
	begin	
		select a.ArtID,a.ArtName,a.Description,a.Price,b.ArtPath,c.Name,d.CategoryName,b.ImageID from ArtMaster as a 
		inner join ImageMaster as b on a.ArtID=b.ArtID
		inner join SubCategoryMaster as c on a.SubCategoryId=c.SubCategoryID
		inner join CategoryMaster as d on c.CategoryID=d.CategoryID
	End
	if (@Action = 'Insert') 
	begin	
		insert into ImageMaster(ArtPath,ArtId)values(@ArtPath,@ArtID)
	End
	if (@Action = 'Delete') 
	begin	
		delete from ImageMaster where ImageID=@ArtID
	End
	if (@Action = 'DeleteArt') 
	begin	
		delete from ArtMaster where ArtID=@ArtID
	End
END


GO
/****** Object:  StoredProcedure [dbo].[sp_ArtistProfile]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ArtistProfile]
(  
@RegId int = null,   
@Action varchar(100)= null  
)
 As begin 
 if @Action = 'Select' 
	select * from RegistrationMaster where UserType='1'
if @Action = 'selected'  
		select r.UserType,r.FirstName,r.LastName,r.RegID,r.ContactNo,r.Address,r.EmailID,r.Description,r.PhotoPath,r.CompanyName,c.CategoryName,s.Name from RegistrationMaster as r
	left join CategoryMaster as c on r.CategoryID=c.CategoryID
	left join SubcategoryMaster as s on r.SubcategoryId=s.SubcategoryId
	where r.RegId=@RegId
End


GO
/****** Object:  StoredProcedure [dbo].[sp_Category]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Category]
	@CatID int= null,  
	@CategoryName varchar(max)=null,  
	@CategoryImage varchar(max)=null,  
	@Action varchar(20)=null
AS
BEGIN
 if (@Action = 'Insert') 
	begin	
		insert into CategoryMaster(CategoryName,CategoryImage) values(@CategoryName,@CategoryImage)
	End
if (@Action = 'Update')  
	begin
		Update CategoryMaster set CategoryName=@CategoryName,CategoryImage=@CategoryImage where CategoryID=@CatID
	End
 if (@Action = 'Delete')
	begin
		Delete from CategoryMaster where CategoryID=@CatID
	End
 if (@Action = 'Select')
	begin
		 select * from CategoryMaster
	End
END



GO
/****** Object:  StoredProcedure [dbo].[sp_change_pwd]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[sp_change_pwd]
(             
  @username VARCHAR(100),
  @old_pwd VARCHAR(50),
  @new_pwd VARCHAR(50),
  @status int OUTPUT,
  @Action varchar(20)=null
)
AS
BEGIN 
  if (@Action = 'Admin') 
	begin	              
		IF EXISTS(SELECT * FROM AdminMaster WHERE EmailID =@username AND Password =@old_pwd)
			BEGIN
				UPDATE AdminMaster SET [Password]=@new_pwd WHERE EmailID=@username
			SET @status=1
			END
		ELSE                    
			BEGIN 
				SET @status=0
			END    
	End 
	if (@Action = 'User') 
	begin	
		IF EXISTS(SELECT * FROM RegistrationMaster WHERE EmailID =@username AND Password =@old_pwd)
			BEGIN
				UPDATE RegistrationMaster SET [Password]=@new_pwd WHERE EmailID=@username
			SET @status=1
			END
		ELSE                    
			BEGIN 
				SET @status=0
			END    

	End
END
RETURN @status 


GO
/****** Object:  StoredProcedure [dbo].[sp_Contactus]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[sp_Contactus]
(  
@ContactUsID int = null,   
@Name varchar(30)= null,  
@EmailID varchar(50)= null,  
@Message varchar(20)= null,  
@Action varchar(100)= null  

)
 As begin 
 Delete from ContactUs where ContactUsID=@ContactUsID
End


GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllCategory]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetAllCategory]
	@CatID int= null,
	@ArtID int= null,
	@RegId int=null,
	@SID int= null,
	@Search varchar(20)=null,
	@Action varchar(20)=null
AS
BEGIN
 if (@Action = 'Select') 
	begin	
		select  DISTINCT i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath,
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType,
		 a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName from ArtMaster  as a inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName

	End
if (@Action = 'ByCategory')  
	begin
			select  DISTINCT i.ArtID,(select top 1 (ArtPath) from ImageMaster where ArtId=i.ArtID) as ArtPath,
			(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType, a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName,r.LastName,r.CompanyName,r.RegID
		from ArtMaster  as a
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID=a.RegID
				where c.CategoryId=@CatID
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName,r.LastName,r.CompanyName,r.RegID
	End

if (@Action = 'BySearch')  
	begin
		select  DISTINCT i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath, 
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName 
		from ArtMaster  as a
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID=a.RegID
		where c.CategoryName=@Search or r.firstName=@Search or a.ArtName=@Search 
				or s.Name=@Search or r.LastName=@Search
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName 
	End
	if (@Action = 'RelatedProduct')  
	begin
	select  DISTINCT top 3 i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath, 
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType,
		a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName,r.LastName,r.CompanyName,r.RegID  from ArtMaster  as a 
		
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID = a.RegID 
		where a.SubCategoryId = @sid and a.ArtId != @ArtID
		Group By i.ArtID, a.ArtName, a.Price, s.SubCategoryID, s.Name, c.CategoryName, r.FirstName,r.LastName,r.CompanyName,r.RegID,i.FileType
		 Order By i.ArtID desc
	End
	if (@Action = 'ArtistProduct')  
	begin
		select  DISTINCT i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath,
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType,
		 a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName 
		from ArtMaster  as a
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID=a.RegID
		where r.RegID=@RegID
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName 
	End
		if (@Action = 'TopProduct')  
	begin
		select  DISTINCT top 6 i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath,
		(select top 1 FileType from ImageMaster where ArtId=i.ArtID) as FileType, a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName,r.LastName,r.CompanyName,r.RegID
		from ArtMaster  as a
		inner join ImageMaster as i on a.ArtID = i.ArtID 
		inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID 
		inner join CategoryMaster as c on s.CategoryID = c.CategoryId 
		inner join RegistrationMaster as r on r.RegID=a.RegID
		Group By i.ArtID,a.ArtName,a.Price,s.SubCategoryID,s.Name,c.CategoryName,r.FirstName,r.LastName,r.CompanyName,r.RegID
		 Order By i.ArtID desc
		
	End
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Location]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Location]
@LocID int=null,
@LocLat varchar(50)=null,
@LocLong varchar(50)=null,
@RegID int=null,
@Action varchar(20)= null

AS
BEGIN
 if (@Action = 'select') 
	begin	
	select distinct  r.RegId,l.LocLat,l.LocLong,r.FirstName,r.LastName,r.PhotoPath,r.CompanyName
		 from locationMaster as l
		inner join RegistrationMaster as r on l.RegID=r.RegId
		where r.UserType=1
	End
	if (@Action = 'Insert') 
	begin	
		insert into locationMaster values(@LocLat,@LocLong,@RegID)
	End
	if (@Action = 'Update') 
	begin	
		update locationMaster set LocLat=@LocLat,LocLong=@LocLong where RegID=@RegID
	End
END


GO
/****** Object:  StoredProcedure [dbo].[sp_login]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_login]
	@RegID int=null,
	@UserType varchar(20)=null,
	@EmailId varchar(50)=null,
	@Password varchar(50)=null,
	@Action varchar(20)=null,
	@status bit=null
AS
BEGIN
	if (@Action = 'login') 
	begin
		select count(*) from RegistrationMaster where EmailId=@EmailId and Password=@Password
	End
	 if (@Action = 'checkstatus') 
	begin	
		select status from RegistrationMaster where EmailId=@EmailId and Password=@Password
	End
	 if (@Action = 'StatusUpdate') 
	begin	
		update RegistrationMaster set 
		Status=@status where RegID=@RegID
	End
END



GO
/****** Object:  StoredProcedure [dbo].[sp_Message]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Message]
	@SenderID int=null,
	@RecieverID varchar(20)=null,
	@Message varchar(max)=null,
	@TimeStamp varchar(50)=null,
	@Action varchar(20)=null
AS
BEGIN
	if (@Action = 'Insert') 
	begin
	insert into Message(SenderID,RecieverID,Message,TimeStamp) values(@SenderID,@RecieverID,@Message,@TimeStamp) 
			End	
	if (@Action = 'Select') 
	begin
	select convert(char(5), TimeStamp, 108) [TimeStamp],SenderID,RecieverID,Message from Message 
		where (SenderID=@SenderID AND RecieverID=@RecieverID)
		OR (SenderID=@RecieverID AND RecieverID=@SenderID)
	End	
	if (@Action = 'SelectedMessage') 
	begin
	select SenderID,RecieverId,(SELECT TOP 1 message FROM message   where SenderID=m.SenderID or RecieverID=m.RecieverID  ORDER BY message DESC) as message,
	(select TOP 1 convert(char(5), TimeStamp, 108) [TimeStamp]  FROM message where SenderID=m.SenderID ORDER BY TimeStamp DESC ) as Time,
	FirstName,LastName,PhotoPath
	from Message as m
	inner join RegistrationMaster as r on r.RegId=m.RecieverID
	where m.SenderID=@RecieverID
		group by SenderID,RecieverId,FirstName,LastName,PhotoPath
	End	
END



GO
/****** Object:  StoredProcedure [dbo].[sp_Registration]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Registration]
	@RegID int=null,
	@FirstName varchar(20)=null,
	@LastName varchar(20)=null,
	@ContactNo varchar(20)=null,
	@PhotoPath varchar(max)=null,
	@UserType varchar(20)=null,
	@CompanyName varchar(max)=null,
	@EmailId varchar(50)=null,
	@Password varchar(50)=null,
	@Address varchar(60)=null,
	@CategoryID int=null,
	@Description varchar(max)=null,
	@SubCategoryID int=null,
	@Action varchar(20)=null,
	@status bit=null
AS
BEGIN
	if (@Action = 'Insert') 
	begin	
		INSERT INTO RegistrationMaster Values(@FirstName,@LastName,@ContactNo,@PhotoPath,@UserType,@CompanyName,@EmailId,@Password,@Address,@CategoryID,@Description,@status,@SubCategoryID )
	End
	 if (@Action = 'Update') 
	begin	
		Update RegistrationMaster set
		 FirstName = @FirstName,LastName = @LastName , ContactNo = @ContactNo,
		 PhotoPath = @PhotoPath, CompanyName = @CompanyName,UserType=@UserType,
		 Address = @Address, CategoryID = @CategoryID, Description = @Description,SubCategoryID=@SubCategoryID  
		 where RegID=@RegID
	End
	 if (@Action = 'StatusUpdate') 
	begin	
		update RegistrationMaster set 
		Status=@status where RegID=@RegID
	End
	 if (@Action = 'Delete') 
	begin	
		Delete from  RegistrationMaster  where RegID=@RegID
	End
	if(@Action='SelectedByEmail')
	begin
	select * from RegistrationMaster where EmailId=@EmailId
	end
	if(@Action='SelectedById')
	begin
	select * from RegistrationMaster where RegId=@RegID
	end
END



GO
/****** Object:  StoredProcedure [dbo].[sp_Subcategory]    Script Date: 12/3/2018 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Subcategory]
	@SubcatID int= null,  
	@CategoryID varchar(20)=null,  
	@SubCategoryName varchar(20)=null,  
	@Action varchar(20)=null
AS
BEGIN
 if (@Action = 'Insert') 
	begin	
		insert into SubCategoryMaster(Name,CategoryId) values(@SubCategoryName,@CategoryID)
	End
if (@Action = 'Update')  
	begin
		Update SubCategoryMaster set @SubCategoryName=@SubCategoryName,CategoryId=@CategoryID where SubCategoryID=@SubcatID
	End
 if (@Action = 'Delete')
	begin
		Delete from SubCategoryMaster where SubCategoryID=@SubcatID
	End
 if (@Action = 'Select')
	begin
		select * from SubCategoryMaster as s
		 inner join CategoryMaster as c on s.CategoryId=c.CategoryID 
	End
END



GO
