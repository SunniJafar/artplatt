﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ArtPlace.aspx.cs" Inherits="ArtPlatt.Admin.ArtPlace" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>User List</title>
    <script>
        $(document).ready(function () {
            BindDatatable();

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPathRider" runat="Server">
    <li><a href="Default.aspx">Home</a></li>
    <li><a href="ArtPlace.aspx">Artist Place</a></li>
    <li class="active">View</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">
    <section class="box-typical">
        <div id="toolbar">
        </div>
        <div class="table-responsive">
            <table class="Datatable">
                <thead>
                    <tr>
                        <th data-sortable="true">Company Name</th>

                        <th>Contact No</th>
                        <th>Email Id</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="GV" OnItemDataBound="GV_ItemDataBound" OnItemCommand="GV_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("CompanyName")%></td>
                                <td><%# Eval("ContactNo")%></td>
                                <td><%# Eval("EmailId")%></td>
                                <td>
                                    <a class="btn btn-sm btn-success" data-toggle="modal" data-target="#Statistics<%# Eval("RegID")%>">
                                        <i class="fa fa-line-chart"></i>&nbsp;View
                                    </a>


                                    <asp:HiddenField runat="server" ID="hfRegID" Value='<%# Eval("RegID")%>' />
                                    <asp:LinkButton runat="server" class="btn btn-sm btn-danger" CommandArgument='<%# Eval("RegID")%>' CommandName="delete"
                                        OnClientClick="return ConfirmOnDelete();">
                                        <i class="fa fa-trash"></i>&nbsp;Delete
                                    </asp:LinkButton>

                                    <asp:LinkButton runat="server" ID="lnkdeactivate" class="btn btn-sm btn-warning" CommandArgument='<%# Eval("RegID")%>' CommandName="deactivate"
                                        OnClientClick="return ConfirmOnDeactivate();">
                                        <i class="fa fa-warning"></i>&nbsp;Deactivate
                                    </asp:LinkButton>

                                    <%--Description Modal Start--%>
                                    <div class="modal fade" id='Statistics<%# Eval("RegID")%>' role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content card-blue-fill">
                                                <div class="modal-header card-header">
                                                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                                                        <i class="fa fa-2x fa-close"></i>
                                                    </button>
                                                    <h4 class="modal-title"><%# Eval("FirstName")%> <%# Eval("LastName")%></h4>
                                                </div>
                                                <div class="modal-body card-block">
                                                    <table class="tbl-typical table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    <div>Image</div>
                                                                </th>
                                                                <th>
                                                                    <div>Name</div>
                                                                </th>
                                                                <th>
                                                                    <div>Email ID</div>
                                                                </th>
                                                                <th>
                                                                    <div>Contac No</div>
                                                                </th>
                                                                <th>
                                                                    <div>Address</div>
                                                                </th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <tr>
                                                                <td>
                                                                    <img src="../Image/Artist/<%# Eval("PhotoPath")%>" alt="" class="img-thumbnail img-table"></td>
                                                                <td><%# Eval("FirstName")%> <%# Eval("LastName")%></td>
                                                                <td><%# Eval("EmailId")%></td>
                                                                <td><%# Eval("ContactNo")%></td>
                                                                <td><%# Eval("Address")%></td>

                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--Description Modal End--%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphModal" runat="server">
</asp:Content>
