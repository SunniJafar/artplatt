﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ArtPlatt.Admin.Login" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sign In</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <link href="Files/DefaultImage/favicon.ico" rel="icon" type="image/png" />
    <link href="assets/css/Custom.css" rel="stylesheet" />
    <link href="assets/css/main.css" rel="stylesheet" />
    <link href="assets/plugins/font-awesome/font-awesome.min.css" rel="stylesheet" />

    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="assets/plugins/hide-show-password/bootstrap-show-password.js"></script>
    <script src="assets/js/Custom.js"></script>
    <script>
        $(document).ready(function () {
            $('.hide-show-password').password();
        });
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

    <form id="form1" runat="server">
        <div class="page-center">
            <div class="page-center-in">
                <div class="container-fluid">
                    <div class="sign-box validform">
                        <div class="sign-avatar">
                            <img src="Files/DefaultImage/1.png" alt="" />
                        </div>
                        <header class="sign-title">Sign In</header>
                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                        <div class="form-group">
                            <div class="form-control-wrapper">
                                <asp:TextBox runat="server" ID="txtEmailID" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Email ID"
                                    data-validation="[NOTEMPTY]"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-control-wrapper">
                                <asp:TextBox runat="server" ID="txtPassword" CssClass="form-control hide-show-password" AutoCompleteType="Disabled" placeholder="Password"
                                    TextMode="Password" data-validation="[NOTEMPTY]">
                                </asp:TextBox>
                            </div>
                        </div>

                        <asp:Button runat="server" ID="btnSubmit" CssClass="btn btn-success" Text="Sign in" OnClick="btnSubmit_Click" />
                        <p class="sign-note">Forgotten password? <a href="#">Reset Password</a></p>
                    </div>
                </div>
            </div>
        </div>

     <%--   <script src="../assets/plugins/notie/notie.js"></script>--%>
    </form>
</body>
</html>