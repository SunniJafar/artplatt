﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ManageArt.aspx.cs" Inherits="ArtPlatt.Admin.ManageArt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Manage Inquiry</title>
    <link href="../assets/plugins/summernote/summernote.css" rel="stylesheet" />
    <script src="../assets/plugins/summernote/summernote.js"></script>
    <script>
        $(document).ready(function () {
            BindDatatable();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPathRider" runat="server">
    <li><a href="Default.aspx">Home</a></li>
    <li><a href="ManageInquiry.aspx">Inquiry</a></li>
    <li class="active">Manage</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">
    <section class="box-typical">
        <div id="toolbar">
        </div>
        <div class="table-responsive">
            <table class="Datatable">
                <thead>
                    <tr>
                        <th>Art</th>
                        <th>Art Name</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="GV" OnItemCommand="GV_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <img src="/Image/Art/<%# Eval("ArtPath")%>" alt="" class="img-thumbnail img-table"></td>
                                <td><%# Eval("ArtName")%></td>
                                <td>€ <%# Eval("Price")%></td>
                                <td><%# Eval("Name")%></td>
                                <asp:HiddenField runat="server" ID="hfdArtId" Value='<%# Eval("ArtID")%>' />
                                <td>
                                    <asp:LinkButton runat="server" class="btn btn-sm btn-danger" CommandArgument='<%# Eval("ImageID")%>' CommandName="delete"
                                        OnClientClick="return ConfirmOnDelete();">
                                        <i class="fa fa-trash"></i>&nbsp;Delete
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphModal" runat="server">
</asp:Content>
