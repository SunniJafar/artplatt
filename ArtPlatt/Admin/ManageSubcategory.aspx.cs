﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ArtPlatt.Admin
{
    public partial class ManageSubcategory : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindData();
                BindCategory();
            }
        }

        private void BindCategory()
        {
            SqlCommand com = new SqlCommand("sp_Category", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Action", "Select");
            SqlDataAdapter sda = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            try
            {

                drdCategory.DataSource = ds;
                drdCategory.DataValueField = "CategoryID";
                drdCategory.DataTextField = "CategoryName";
                drdCategory.DataBind();
                drdCategory.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindData()
        {
            SqlCommand com = new SqlCommand("sp_Subcategory", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Action", "Select");
            SqlDataAdapter sda = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            try
            {
                GV.DataSource = ds;
                GV.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            con.Close();

        }

        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("sp_Subcategory", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Insert");
            cmd.Parameters.AddWithValue("@SubCategoryName", txtCategoryName.Text.Trim());
            cmd.Parameters.AddWithValue("@CategoryID", drdCategory.SelectedValue);
            cmd.ExecuteNonQuery();
            con.Close();
            BindData();
        }

        protected void GV_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlCommand cmd = new SqlCommand("sp_Subcategory", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "Delete");
                cmd.Parameters.AddWithValue("@SubcatID", id);
                cmd.ExecuteNonQuery();
                con.Close();
                BindData();
            }
            else if (e.CommandName == "update")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlCommand cmd = new SqlCommand("sp_Category", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "Update");
                cmd.Parameters.AddWithValue("@CategoryName", txtCategoryName.Text);
                //cmd.Parameters.AddWithValue("@CategoryImage", img1);
                cmd.Parameters.AddWithValue("@CatID", id);
                cmd.ExecuteNonQuery();
                con.Close();
                BindData();
            }
        }


        protected void GV_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string categoryId = ((HiddenField)e.Item.FindControl("hdnID")).Value;
            if (e.Item.ItemType == ListItemType.Item ||
             e.Item.ItemType == ListItemType.AlternatingItem)
            {
                SqlCommand com = new SqlCommand("sp_Category", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Action", "Select");
                SqlDataAdapter sda = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                sda.Fill(ds);

                string selectedCategoryId = ((DropDownList)e.Item.FindControl("drdCategory")).AccessKey;

                ((DropDownList)e.Item.FindControl("drdCategory")).DataSource = ds;//Or any other datasource.
                ((DropDownList)e.Item.FindControl("drdCategory")).DataValueField = "CategoryID";
                ((DropDownList)e.Item.FindControl("drdCategory")).DataTextField = "CategoryName";

                ((DropDownList)e.Item.FindControl("drdCategory")).DataBind();
                ((DropDownList)e.Item.FindControl("drdCategory")).Items.Insert(0, new ListItem("Select", "0"));
                ((DropDownList)e.Item.FindControl("drdCategory")).SelectedValue = categoryId;

            }
        }
    }
}