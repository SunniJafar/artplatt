﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ArtPlatt.Admin
{
    public partial class ManageCategory : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            SqlCommand com = new SqlCommand("sp_Category", con);
            con.Open();
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Action", "Select");
            SqlDataAdapter sda = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            try
            {
                GV.DataSource = ds;
                GV.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            string img1 = fluFile.FileName.ToString();
            fluFile.PostedFile.SaveAs(Server.MapPath("~/assets/img/Category/") + img1);
            SqlCommand cmd = new SqlCommand("sp_Category", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Insert");
            cmd.Parameters.AddWithValue("@CategoryName", txtCategoryName.Text);
            cmd.Parameters.AddWithValue("@CategoryImage", img1);
            cmd.ExecuteNonQuery();
            con.Close();
            BindData();
        }

        protected void GV_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlCommand cmd = new SqlCommand("sp_Category", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "Delete");
                cmd.Parameters.AddWithValue("@CatID", id);
                cmd.ExecuteNonQuery();
                con.Close();
                BindData();
            }
            else if (e.CommandName == "update")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                int index = e.Item.ItemIndex;
                string CatName = ((TextBox)(GV.Items[index].FindControl("txtCategoryName"))).Text;
                FileUpload CatImage = ((FileUpload)(GV.Items[index].FindControl("fluFile")));
                string OldImage = ((HiddenField)(GV.Items[index].FindControl("hfimage"))).Value;

                string ImageName = OldImage;

                if (CatImage.HasFile)
                {
                    ImageName = CatImage.FileName.ToString();
                    string old_image_name = Server.MapPath("~/assets/img//Category/" + OldImage + "");
                    if (System.IO.File.Exists(old_image_name))
                    {
                        System.IO.File.Delete(Server.MapPath("~/Image/Art/" + OldImage + ""));
                        System.IO.File.Delete(old_image_name);
                    }
                    CatImage.PostedFile.SaveAs(Server.MapPath("~/assets/img//category/") + ImageName);
                }
                SqlCommand cmd = new SqlCommand("sp_category", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@action", "update");
                cmd.Parameters.AddWithValue("@categoryname", CatName);
                cmd.Parameters.AddWithValue("@CategoryImage", ImageName);

                cmd.Parameters.AddWithValue("@catid", id);
                cmd.ExecuteNonQuery();
                con.Close();
            }

            BindData();
        }
    }
}