﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ArtPlatt.Admin
{
    public partial class AdminMaster : System.Web.UI.MasterPage
    {
        string AdminId;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AdminID"] != null)
            {
                AdminId = Convert.ToString(Session["AdminID"]);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void lnkbtnChangePassword_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("sp_change_pwd", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Admin");
            cmd.Parameters.AddWithValue("@username", AdminId.Trim());
            cmd.Parameters.AddWithValue("@old_pwd", txtCurrentPassword.Text.Trim());
            cmd.Parameters.AddWithValue("@new_pwd", txtNewPassword.Text.Trim());

            cmd.Parameters.Add("@Status", SqlDbType.Int);
            cmd.Parameters["@Status"].Direction = ParameterDirection.Output;
            con.Open();
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            int retVal = Convert.ToInt32(cmd.Parameters["@Status"].Value);
            if (retVal == 1)
            {
                //lblStatus.Text = "Password has been changed successfully";
            }
            else
            {
                //lblStatus.Text = "Wrong old username/password. Please re-enter.";
            }
        }
    }
}