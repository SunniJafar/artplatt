﻿//Notification
{
    function PageNotification(status, message) {
        var Timeout = 2;
        notie.alert(status, message, Timeout);
    }
}

//Bind Summernote
{
    function BindSummernote() {
        $('.summernote').summernote();
    }
}

//Bind Datatable
{
    function BindDatatable() {
        $('.Datatable').bootstrapTable({ pageSize: 100 });
    }
    function BindInnerTable() {
        $('.innertable').bootstrapTable({
            classes: 'table table-hover table-striped table-sm',
            pagination: false,
            search: false,
            showPaginationSwitch: false,
            showToggle: false,
            showColumns: false,
            toolbar: null,
        });
        $(".bootstrap-table .fixed-table-toolbar").hide();
    }
}
//Delete Confirm
{
    function ConfirmOnDelete() {
        return confirm("Are you sure to delete this data?");
    }
}
function ConfirmOnDeactivate() {
    return confirm("Are you sure you want to Deactivate?");
}
function ConfirmOnActivate() {
    return confirm("Are you sure you want to Activate?");
}
//Validation
{
    $(document).ready(function () {
        $('form1').validate({
            submit: {
                settings: {
                    form: null,
                    display: "inline",
                    insertion: "append",
                    allErrors: !1,
                    trigger: "click",
                    button: "[type='submit']",
                    errorClass: "error",
                    errorListClass: "error-list",
                    errorListContainer: null,
                    inputContainer: '.form-group',
                    clear: "focusin",
                    scrollToError: !0,
                    debug: !0
                },
                callback: {
                    onInit: function () {
                    },
                    onValidate: function () {
                    },
                    onError: function () {
                        PageNotification(0, "Form have some error");
                    },
                    onBeforeSubmit: function () {
                        console.log("onBeforeSubmit");
                    },
                    onSubmit: function () {
                        console.log("onSubmit");
                    },
                    onAfterSubmit: function () {
                        console.log("onAfterSubmit");
                    }
                }
            }
        });
    });
}

//Auto size text area
{
    $(document).ready(function () {
        autosize($('textarea[data-autosize]'));
    });
}