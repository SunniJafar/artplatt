﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ArtPlatt.Admin
{
    public partial class ManageArt : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindData()
        {
            SqlCommand cmd = new SqlCommand("sp_artimage", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "select");
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            try
            {
                GV.DataSource = ds;
                GV.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            con.Close();

        }


        protected void GV_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlCommand cmd = new SqlCommand("sp_artimage", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "Delete");
                cmd.Parameters.AddWithValue("@ArtID", id);
                cmd.ExecuteNonQuery();
                string ArtId = ((HiddenField)e.Item.FindControl("hfdArtId")).Value;
                SqlCommand com = new SqlCommand("select * from ImageMaster where ArtID='" + ArtId + "'", con);
                object obj = com.ExecuteScalar();
                if (obj == null)
                {

                    SqlCommand cmddel = new SqlCommand("sp_artimage", con);
                    cmddel.CommandType = CommandType.StoredProcedure;
                    cmddel.Parameters.AddWithValue("@Action", "DeleteArt");
                    cmddel.Parameters.AddWithValue("@ArtID", ArtId);
                    cmddel.ExecuteNonQuery();
                }


                con.Close();
                BindData();
            }
        }
    }
}