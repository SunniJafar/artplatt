﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ArtPlatt.Admin
{
    public partial class Default : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["AdminID"] != null)
                {
                    BindTotal();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        private void BindTotal()
        {
            SqlCommand cattotal = new SqlCommand("select COUNT(CategoryID) from CategoryMaster", con);
            con.Open();
            ltrTotalCategory.Text = Convert.ToString(cattotal.ExecuteScalar());

            SqlCommand Subcattotal = new SqlCommand("select COUNT(SubCategoryID) from SubCategoryMaster", con);
            ltrTotalSubCateegory.Text = Convert.ToString(Subcattotal.ExecuteScalar());

            SqlCommand totalArt = new SqlCommand("select COUNT(RegID) from RegistrationMaster where  usertype='1'", con);
            ltrTotalArtPlace.Text = Convert.ToString(totalArt.ExecuteScalar());

            SqlCommand totalArtist = new SqlCommand("select COUNT(RegID) from RegistrationMaster where  usertype='0'", con);
            ltrTotalArtist.Text = Convert.ToString(totalArtist.ExecuteScalar());

            SqlCommand tottaluser = new SqlCommand("select COUNT(RegID) from RegistrationMaster where  usertype='2'", con);
            ltrTotalUser.Text = Convert.ToString(tottaluser.ExecuteScalar());
            con.Close();
        }
    }
}