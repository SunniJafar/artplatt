﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ArtPlatt.Admin
{
    public partial class Artist : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            SqlCommand cmd = new SqlCommand("select * from RegistrationMaster where UserType='" + 0 + "'", con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            try
            {
                GV.DataSource = ds;
                GV.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void GV_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "deactivate")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlCommand cmd = new SqlCommand("sp_Registration", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "StatusUpdate");
                cmd.Parameters.AddWithValue("@RegID", id);
                cmd.Parameters.AddWithValue("@status", 0);

                cmd.ExecuteNonQuery();
                con.Close();
                BindData();
            }
            else if (e.CommandName == "delete")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlCommand cmd = new SqlCommand("sp_Registration", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "Delete");
                cmd.Parameters.AddWithValue("@RegID", id);
                cmd.ExecuteNonQuery();
                con.Close();
                BindData();
            }
            else if (e.CommandName == "activate")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                SqlCommand cmd = new SqlCommand("sp_Registration", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "StatusUpdate");
                cmd.Parameters.AddWithValue("@RegID", id);
                cmd.Parameters.AddWithValue("@status", true);
                cmd.ExecuteNonQuery();
                con.Close();
                BindData();
            }
        }

        protected void GV_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string RegID = ((HiddenField)e.Item.FindControl("hfRegID")).Value;
            LinkButton lnkbutton = ((LinkButton)e.Item.FindControl("lnkdeactivate"));
            SqlCommand cmd = new SqlCommand("select * from RegistrationMaster where Status='" + true + "' and RegID='" + RegID + "'", con);
            con.Open();
            int obj = Convert.ToInt32(cmd.ExecuteScalar());
            if (obj == 0)
            {
                //lnkbutton.Visible = false;
                lnkbutton.Text = "Activate";
                lnkbutton.OnClientClick = "ConfirmOnActivate()";
                lnkbutton.CommandName = "activate";
                lnkbutton.CssClass = "btn btn-sm btn-warning";
            }
            else
            {
                lnkbutton.Visible = true;
            }
            con.Close();
        }
    }
}