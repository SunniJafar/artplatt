﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ArtPlatt.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SqlCommand com = new SqlCommand("sp_adminLogin", con);
            con.Open();
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Usename", txtEmailID.Text.ToString());
            com.Parameters.AddWithValue("@word", txtPassword.Text.ToString());

            int usercount = (Int32)com.ExecuteScalar();

            if (usercount == 1)
            {
                Session["AdminID"] = txtEmailID.Text;
                Response.Redirect("Default.aspx");
            }
            else
            {
                con.Close();
                lblmsg.Text = "Invalid User Name or password";
            }
        }
    }
}