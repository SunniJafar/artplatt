﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ManageCategory.aspx.cs" Inherits="ArtPlatt.Admin.ManageCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Manage Category</title>
    <link href="assets/plugins/bootstrap-fileinput/fileinput.css" rel="stylesheet" />
    <script src="assets/plugins/bootstrap-fileinput/fileinput.js"></script>

    <script>
        $(document).ready(function () {
            BindDatatable();
        });
        function btnAdd_Click() {
            $("#Add_Modal").modal();
            var ImageName = "Default.png";
            Bindfileinput(ImageName);
        }
        function btnEdit_Click(CategoryID, ImageName) {
            $("#Edit_Modal" + CategoryID).modal();
            Bindfileinput(ImageName);
        }
        function btnAddAudio_Click(CategoryID, ImageName) {
            $("#AddAudio_Modal" + CategoryID).modal();
            BindAudiofileinput();
        }

        function BindAudiofileinput() {
            $(".flufile").fileinput("destroy");
            $(".flufile").fileinput({
                maxFileCount: 1,
                allowedFileExtensions: ["mp3"],
            });
        }

        function Bindfileinput(ImageName) {
            $(".flufile").fileinput("destroy");
            $(".flufile").fileinput({
                maxFileCount: 1,
                minImageWidth: 100,
                minImageHeight: 100,
                allowedFileExtensions: ["jpg", "png", "gif"],
                initialPreview: [
                    "<img style='height:160px' src='" + ImageName + "'>",
                ],
                initialCaption: ImageName,
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPathRider" runat="Server">
    <li><a href="Default.aspx">Home</a></li>
    <li><a href="ManageCategory.aspx">Category</a></li>
    <li class="active">Manage</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="Server">
    <section class="box-typical">
        <div id="toolbar">
            <a class="btn btn-success btn-rounded" onclick="btnAdd_Click();">
                <i class="font-icon fa fa-plus"></i>&nbsp;Add
            </a>
        </div>
        <div class="table-responsive">
            <table class="Datatable">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th data-sortable="true">Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="GV" OnItemCommand="GV_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <img src="/assets/img/Category/<%# Eval("CategoryImage")%>" alt="" class="img-thumbnail img-table">
                                </td>

                                <td><%# Eval("CategoryName")%></td>

                                <td>
                                    <a class="btn btn-sm" onclick="btnEdit_Click(<%# Eval("CategoryID")%>,'/assets/img/Category/<%# Eval("CategoryImage")%>')">
                                        <i class="fa fa-pencil"></i>&nbsp;Edit
                                    </a>

                                    <asp:LinkButton runat="server" class="btn btn-sm btn-danger" CommandArgument='<%# Eval("CategoryID")%>' CommandName="delete"
                                        OnClientClick="return ConfirmOnDelete();">
                                        <i class="fa fa-trash"></i>&nbsp;Delete
                                    </asp:LinkButton>
                                    <%--Edit Modal Start--%>
                                    <div class="modal fade" id='Edit_Modal<%# Eval("CategoryID")%>' role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content card-blue-fill">
                                                <div class="modal-header card-header">
                                                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                                                        <i class="fa fa-2x fa-close"></i>
                                                    </button>
                                                    <h4 class="modal-title"><i class="fa fa-pencil"></i>&nbsp;Edit <%# Eval("CategoryName")%></h4>
                                                </div>
                                                <div class="modal-body card-block">
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 form-control-label">Category Name</label>
                                                        <div class="col-sm-9">
                                                            <asp:TextBox runat="server" ID="txtCategoryName" CssClass="form-control" placeholder="Category Name"
                                                                Text='<%# Eval("CategoryName")%>'></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server"
                                                                ControlToValidate="txtCategoryName"
                                                                ErrorMessage="Please enter category name"
                                                                Display="Dynamic"
                                                                ValidationGroup="update"
                                                                CssClass="errormessage">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-12 form-control-label">Category Icon (Size: 100px height * 100px width</label>
                                                        <div class="col-sm-12">
                                                            <asp:HiddenField id="hfimage" runat="server" Value='<%# Eval("CategoryImage")%>' />
                                                            <asp:FileUpload runat="server" ID="fluFile" CssClass="flufile" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <asp:LinkButton runat="server" CssClass="btn btn-success btn-rounded"
                                                        CommandArgument='<%# Eval("CategoryID")%>' CommandName="update" ValidationGroup="update">
                                                        <i class="font-icon fa fa-pencil"></i>&nbsp;Update
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphModal" runat="Server">
    <div class="modal fade" id="Add_Modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content card-blue-fill">
                <div class="modal-header card-header">
                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-2x fa-close"></i>
                    </button>
                    <h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;Add New Category</h4>
                </div>
                <div class="modal-body card-block">
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Category Name</label>
                        <div class="col-sm-9">
                            <asp:TextBox runat="server" ID="txtCategoryName" CssClass="form-control" placeholder="Category Name"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server"
                                ControlToValidate="txtCategoryName"
                                ErrorMessage="Please enter category name"
                                Display="Dynamic"
                                ValidationGroup="add"
                                CssClass="errormessage">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <asp:FileUpload runat="server" ID="fluFile" CssClass="flufile" />
                            <asp:RequiredFieldValidator runat="server"
                                ControlToValidate="fluFile"
                                ErrorMessage="Please select category icon"
                                Display="Dynamic"
                                ValidationGroup="add"
                                CssClass="errormessage">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton runat="server" ID="lnkbtnSave" OnClick="lnkbtnSave_Click" CssClass="btn btn-success btn-rounded" ValidationGroup="add">
                        <i class="font-icon fa fa-plus"></i>&nbsp;Save
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

