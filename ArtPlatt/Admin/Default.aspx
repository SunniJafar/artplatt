﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ArtPlatt.Admin.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Dashboard</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPathRider" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">
    <div class="row">

        <div class="col-xl-12">
            <div class="row">
                <div class="col-sm-3">
                    <article class="statistic-box yellow">
                        <div>
                            <div class="number">
                                <asp:Literal runat="server" ID="ltrTotalUser" Text="0"></asp:Literal>
                            </div>
                            <div class="caption">
                                <div>Total User</div>
                            </div>
                        </div>
                    </article>
                </div>
                 <div class="col-sm-3">
                    <article class="statistic-box red">
                        <div>
                            <div class="number">
                                <asp:Literal runat="server" ID="ltrTotalArtist" Text="0"></asp:Literal>
                            </div>
                            <div class="caption">
                                <div>Total Artist</div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-3">
                    <article class="statistic-box purple">
                        <div>
                            <div class="number">
                                <asp:Literal runat="server" ID="ltrTotalArtPlace" Text="0"></asp:Literal>
                            </div>
                            <div class="caption">
                                <div>Total Art Place</div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="col-sm-3">
                    <article class="statistic-box yellow">
                        <div>
                            <div class="number">
                                <asp:Literal runat="server" ID="ltrTotalCategory" Text="0"></asp:Literal>
                            </div>
                            <div class="caption">
                                <div>Total Category</div>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="col-sm-3">
                    <article class="statistic-box green">
                        <div>
                            <div class="number">
                                <asp:Literal runat="server" ID="ltrTotalSubCateegory" Text="0"></asp:Literal>
                            </div>
                            <div class="caption">
                                <div>Total Subcategory </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphModal" runat="server">
</asp:Content>

