﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="ArtPlatt.ContactUs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/backgrounds/contact-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>Contact Us</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->

    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-contact-page">
                
                <!-- General Information -->
                <div class="col-sm-6">
                    <div class="ws-contact-info">
                        <h2>General:</h2>
                        <p><a href="mailto:office@artplatt.com">office@artplatt.com</a></p>
                        <br />
                        <h2>Press:</h2>
                        <p><a href="mailto:info@artplatt.com">press@artplatt.com</a></p>
                        <br />
                        <h2>Shipping / Orders:</h2>
                        <p><a href="mailto:info@artplatt.com">shipping@artplatt.com</a></p>
                        <br />
                        <h2>Address:</h2>
                        <p>
                            Lassallestrasse 40/4/8, 1020 Wien/ Vienna, Austria<br />
                        </p>
                        <br />
                        <br />
                         <p>Artplatt.com is a worldwide ecosystem for Artists, Artplaces and everyone interested in Art of all it's farious forms. Our passion is Art. Our Mission is to create a simplified and global access to all art-related activities for everyone.</p>
                        <%--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>--%>
                        <br>
                        <%--<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>--%>
                    </div>
                </div>

                <!-- Contact Form -->
                <div class="col-sm-6">
                    <div class="form-horizontal ws-contact-form">
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                        <!-- Name -->
                        <div class="form-group">
                            <label class="control-label">Name <span>*</span></label>
                            <asp:TextBox ID="txtname" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="Enter Name" ForeColor="Red" ControlToValidate="txtname" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                            <cc1:filteredtextboxextender runat="server" id="FilteredTextBoxExtender27" targetcontrolid="txtname" validchars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ " enabled="true"></cc1:filteredtextboxextender>

                        </div>

                        <!-- Email -->
                        <div class="form-group">
                            <label class="control-label">Email <span>*</span></label>
                            <asp:TextBox ID="txtemail" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ErrorMessage="Enter Email" Display="Dynamic" ForeColor="Red" ControlToValidate="txtemail" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtemail"
                                ErrorMessage="Enter a valid Email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="Insert" Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </div>

                        <!-- Message -->
                        <div class="form-group">
                            <label class="control-label">Message <span>*</span></label>
                            <asp:TextBox ID="txtmsg" runat="server" class="form-control" Rows="7" TextMode="MultiLine"></asp:TextBox>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtmsg" ID="RegularExpressionValidator1" ValidationExpression="^[\s\S]{50,}$" ValidationGroup="Insert" ForeColor="Red" runat="server" ErrorMessage="Minimum 50 characters required."></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ErrorMessage="Enter Message" ForeColor="Red" ControlToValidate="txtmsg" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                        </div>

                        <!-- Submit Button -->
                        <div class="form-group">
                            <asp:Button ID="btnsubmit" runat="server" Text="Submit" class="btn ws-big-btn" ValidationGroup="Insert" OnClick="btnsubmit_Click" Style="width: 100%; background-color: #C2A476; color: #fff;" />
                        </div>
                    </div>
                </div>
                <asp:ValidationSummary ID="vali" runat="server" ValidationGroup="Insert" ShowMessageBox="false" ShowSummary="false" />

            </div>
        </div>
    </div>
</asp:Content>
