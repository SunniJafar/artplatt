﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="ArtPlatt.Category" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function ValidatorUpdateDisplay(val) {
            if (typeof (val.display) == "string") {
                if (val.display == "None") {
                    return;
                }
                if (val.display == "Dynamic") {
                    val.style.display = val.isvalid ? "none" : "inline";
                    return;
                }

            }
            val.style.visibility = val.isvalid ? "hidden" : "visible";
            if (val.isvalid) {
                document.getElementById(val.controltovalidate).style.border = '1px solid #333';
            }
            else {
                document.getElementById(val.controltovalidate).style.border = '1px solid red';
            }
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/backgrounds/shop-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>Art</h1>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css" media="all">
        .ui-autocomplete {
            position: absolute;
            cursor: default;
            height: 200px;
            overflow-y: scroll;
            overflow-x: hidden;
        }
    </style>
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-subscribe-content text-center clearfix">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="form-inline">
                        <div class="form-group">
                            <asp:TextBox ID="txtsearch" runat="server" class="form-control" placeholder="Search here"></asp:TextBox>
                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtsearch"
                                MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetName">
                            </cc1:AutoCompleteExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="required" ControlToValidate="txtsearch" ValidationGroup="Add"></asp:RequiredFieldValidator>

                        </div>
                        <asp:Button runat="server" ID="btnsearch" Text="Search" ValidationGroup="Add" OnClick="btnsearch_Click" class="btn ws-btn-subscribe" />
                    </div>
                </div>
            </div>

            <div class="ws-shop-page" style="padding-top: 10px">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                    <asp:Repeater ID="rptsubCat" runat="server">
                        <ItemTemplate>
                            <li role="presentation">
                                <a href="#<%#Eval("trimName") %>" aria-controls="<%#Eval("trimName") %>" role="tab" data-toggle="tab"><%#Eval("Name") %>(<%#Eval("Total") %>)</a>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <div class="tab-content">
                    <!-- All -->
                    <div class="clearfix"></div>
                    <div role="tabpanel" class="tab-pane fade in active" id="all">
                        <div class="col-md-12">
                            <asp:Repeater ID="rptCategoryall" runat="server" OnDataBinding="rptCategoryall_DataBinding">
                                <ItemTemplate>
                                    <div class="col-sm-6 col-md-4 ws-works-item">

                                        <div class="ws-item-offer">
                                            <!-- Image -->
                                            <figure>
                                                <center>
                                                <asp:Panel ID="Panel1" runat="server" Visible='<%# Eval("FileType").ToString()=="Viedio"%>'>
                                                    <video id="video" src='/Image/Art/<%#Eval("Path") %>' style="padding-top: 65px;" controls="true" class="img-responsive"></video>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel2" runat="server" Visible='<%# Eval("FileType").ToString()=="Image"%>'>
                                                    <img src='/Image/Art/<%#Eval("Path") %>' class="img-responsive" />
                                                </asp:Panel>
                                                <asp:Panel ID="Panel3" runat="server" Visible='<%# Eval("FileType").ToString()=="Audio"%>'>
                                                    <img src="assets/DefaultImage/Audio.png" style="height: 236px; width: 100%;" />
                                                    <audio src='/Image/Art/<%#Eval("Path") %>' style="width: 100%" controls></audio>

                                                </asp:Panel>
                                                <asp:Panel ID="Panel4" runat="server" Visible='<%# Eval("FileType").ToString()=="Pdf"%>'>
                                                    <a href='/Image/Art/<%#Eval("Path") %>' target="_blank">
                                                        <img src="assets/DefaultImage/Pdf.png" class="img-responsive" /></a>
                                                </asp:Panel>
                                                    </center>
                                            </figure>
                                        </div>

                                        <div class="ws-works-caption text-center">
                                            <!-- Item Category -->
                                            <a href="ProductDetail.aspx?ArtID=<%#Eval("Id") %>">
                                                <div class="ws-item-category">
                                                    <%#Eval("SubCategoryName") %>
                                                </div>

                                                <center>
                                            <h3 class="ws-item-title">
                                                <%#Eval("ArtName") %>
                                            </h3>
                                            </a>
                                            <a href="ArtistProfile.aspx?ArtistID=<%#Eval("ArtistId") %>">
                                            <div class="ws-item-category" style="text-transform: none !important; margin-top: 5px;">

                                                    <%#Eval("ArtistName") %>
                                                </div>
                                            </a>
                                            <div class="ws-item-separator"></div>
                                            <div class="ws-item-price">
                                                <%#Eval("Price") %>
                                            </div>
                                            </center>

                                        </div>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <center>
                                <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Currently there are no arts available."></asp:Label>
                                    </center>
                                </FooterTemplate>

                            </asp:Repeater>

                        </div>
                    </div>

                    <asp:Repeater ID="rptsubCatTab" runat="server" OnItemDataBound="rptsubCatTab_ItemDataBound">
                        <ItemTemplate>
                            <div class="clearfix"></div>
                            <div role="tabpanel" class="tab-pane fade" id="<%#Eval("trimName") %>">
                                <asp:HiddenField ID="hdnID" Value='<%# Eval("Id")%>' runat="server" />
                                <div class="col-md-12">
                                    <asp:Repeater ID="rptProducts" runat="server">
                                        <ItemTemplate>
                                            <div class="col-sm-6 col-md-4 ws-works-item">

                                                <div class="ws-item-offer">
                                                    <figure>
                                                        <center>
                                                        <asp:Panel ID="Panel1" runat="server" Visible='<%# Eval("FileType").ToString()=="Viedio"%>'>
                                                            <video id="video" src='/Image/Art/<%#Eval("Path") %>' style="padding-top: 65px;" controls="true" class="img-responsive"></video>
                                                        </asp:Panel>
                                                        <asp:Panel ID="Panel2" runat="server" Visible='<%# Eval("FileType").ToString()=="Image"%>'>
                                                            <img src='/Image/Art/<%#Eval("Path") %>' class="img-responsive" />
                                                        </asp:Panel>
                                                        <asp:Panel ID="Panel3" runat="server" Visible='<%# Eval("FileType").ToString()=="Audio"%>'>
                                                            <img src="assets/DefaultImage/Audio.png" style="height: 236px; width: 100%;" />
                                                            <audio src='/Image/Art/<%#Eval("Path") %>' style="width: 100%" controls></audio>

                                                        </asp:Panel>
                                                        <asp:Panel ID="Panel4" runat="server" Visible='<%# Eval("FileType").ToString()=="Pdf"%>'>
                                                            <a href='/Image/Art/<%#Eval("Path") %>' target="_blank">
                                                                <img src="assets/DefaultImage/Pdf.png" class="img-responsive" /></a>
                                                        </asp:Panel>
                                                            </center>
                                                    </figure>
                                                </div>

                                                <div class="ws-works-caption text-center">
                                                    <a href="ProductDetail.aspx?ArtID=<%#Eval("Id") %>">
                                                        <div class="ws-item-category">
                                                            <%#Eval("CategoryName") %>
                                                        </div>
                                                        <center>
                                                            <h3 class="ws-item-title">
                                                                <%#Eval("SubCategoryName") %>
                                                            </h3>
                                                    </a>
                                                 
                                                    <a href="ArtistProfile.aspx?ArtistID=<%#Eval("ArtistId") %>">
                                                 <div class="ws-item-category" style="text-transform: none !important; margin-top: 5px;">
                                                            <%#Eval("ArtistName") %>
                                                        </div>
                                                    </a>
                                                       <div class="ws-item-separator"></div>
                                                    <div class="ws-item-price">
                                                        <%#Eval("Price") %>
                                                    </div>
                                                    </center>
                                                   
                                                </div>

                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <center>
                                <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Currently there are no arts available."></asp:Label>
                                    </center>
                                        </FooterTemplate>

                                    </asp:Repeater>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:ValidationSummary ID="val" runat="server" ShowSummary="false" ShowMessageBox="false" ValidationGroup="Add" />

                </div>
            </div>
        </div>
    </div>
</asp:Content>
