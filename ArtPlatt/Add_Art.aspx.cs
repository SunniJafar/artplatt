﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace ArtPlatt
{
    public partial class Add_Art : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        string filetype;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Email"] != null || Session["UserEmail"] != null)
                {
                    //BindCategory();
                }
                else
                {
                    Response.Redirect("Login.aspx");

                }

            }
        }

        #region Category Succategory
        //private void BindCategory()
        //{
        //    SqlDataAdapter sda = new SqlDataAdapter("select * from CategoryMaster", con);
        //    DataSet ds = new DataSet();
        //    sda.Fill(ds);
        //    drdCategory.DataSource = ds;
        //    drdCategory.DataValueField = "CategoryID";
        //    drdCategory.DataTextField = "CategoryName";
        //    drdCategory.DataBind();
        //    drdCategory.Items.Insert(0, new ListItem("Select", "0"));


        //}

        //protected void drdCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    drdsubcategory.Enabled = true;
        //    int CID = Convert.ToInt32(drdCategory.SelectedValue);
        //    SqlDataAdapter sda = new SqlDataAdapter("select * from SubCategoryMaster where CategoryId='" + CID + "'", con);
        //    DataSet ds = new DataSet();
        //    sda.Fill(ds);
        //    drdsubcategory.DataSource = ds;
        //    drdsubcategory.DataTextField = "Name";
        //    drdsubcategory.DataValueField = "SubCategoryID";
        //    drdsubcategory.DataBind();
        //    drdsubcategory.Items.Insert(0, new ListItem("Select", "0"));

        //} 
        #endregion

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            string Email;

            if (Session["Email"] != null)
                Email = Convert.ToString(Session["Email"]);
            else
                Email = Convert.ToString(Session["UserEmail"]);
            SqlCommand id = new SqlCommand("select RegID from RegistrationMaster where EmailId='" + Email + "'", con);
            SqlCommand SubcatId = new SqlCommand("select SubCategoryID from RegistrationMaster where EmailId='" + Email + "'", con);
            con.Open();
            int RegId = Convert.ToInt32(id.ExecuteScalar());
            int SubId = Convert.ToInt32(SubcatId.ExecuteScalar());
            //long SubId = Convert.ToInt64(drdsubcategory.SelectedValue);
            SqlCommand cmd = new SqlCommand("insert into ArtMaster values('" + txtname.Text + "','" + txtprice.Text + "','" + SubId + "','" + txtdesc.Text + "','" + RegId + "')", con);
            cmd.ExecuteNonQuery();
            SqlCommand sqcmd = new SqlCommand("select max(ArtID) from ArtMaster", con);
            int maxId = Convert.ToInt32(sqcmd.ExecuteScalar());



            bool isexists = Directory.Exists(Server.MapPath("~/" + "Image" + "/" + "Art"));
            if (isexists == false)
                Directory.CreateDirectory(Server.MapPath("~/" + "Image" + "/" + "Art"));
            if (fuimage1.HasFile)
            {
                string fileextension = Path.GetExtension(fuimage1.FileName).ToLower();

                if (fileextension == ".pdf")
                    filetype = "Pdf";
                else if (fileextension == ".jpeg" || fileextension == ".jpg" || fileextension == ".png" || fileextension == ".gif")
                    filetype = "Image";
                else if (fileextension == ".mp4" || fileextension == ".3gp" || fileextension == ".mkv" || fileextension == ".mpeg")
                    filetype = "Viedio";
                else if (fileextension == ".mp3" || fileextension == ".waw")
                    filetype = "Audio";
                SqlCommand Icmd = new SqlCommand("select max(ImageID) from imageMaster", con);
                object objsql = Icmd.ExecuteScalar();

                int maxImageID1 = 1;
                if (objsql != null && DBNull.Value != objsql)
                {
                    maxImageID1 = 1 + Convert.ToInt32(Icmd.ExecuteScalar());
                }
                string path = Server.MapPath("~/" + "Image" + "/" + "Art");
                //string ImageName = txtname.Text + "_" + maxImageID1 + fileextension;
                string ImageName = ShortGuid.NewGuid() + fileextension;
                var mappath = Path.Combine(path, ImageName);
                bool isexists3 = Directory.Exists(mappath);
                if (isexists3 == false)
                {
                    fuimage1.SaveAs(mappath);
                }
                SqlCommand cm = new SqlCommand("insert into ImageMaster values('" + ImageName + "','" + maxId + "','" + filetype + "')", con);
                cm.ExecuteNonQuery();
            }
            if (fuimage2.HasFile)
            {
                string fileextension = Path.GetExtension(fuimage2.FileName).ToLower();
                if (fileextension == ".pdf")
                    filetype = "Pdf";
                else if (fileextension == ".jpeg" || fileextension == ".jpg" || fileextension == ".png" || fileextension == ".gif")
                    filetype = "Image";
                else if (fileextension == ".mp4" || fileextension == ".3gp" || fileextension == ".mkv")
                    filetype = "Viedio";
                else if (fileextension == ".mp3" || fileextension == ".waw")
                    filetype = "Audio";
                SqlCommand Icmd = new SqlCommand("select max(ImageID) from imageMaster", con);
                int maxImageID1 = 1 + Convert.ToInt32(Icmd.ExecuteScalar());

                string path = Server.MapPath("~/" + "Image" + "/" + "Art");
                string ImageName = ShortGuid.NewGuid() + fileextension;
                var mappath = Path.Combine(path, ImageName);
                bool isexists3 = Directory.Exists(mappath);
                if (isexists3 == false)
                {
                    fuimage2.SaveAs(mappath);
                }
                SqlCommand cm = new SqlCommand("insert into ImageMaster values('" + ImageName + "','" + maxId + "','" + filetype + "')", con);
                cm.ExecuteNonQuery();
            }
            if (fuimage3.HasFile)
            {

                string fileextension = Path.GetExtension(fuimage3.FileName).ToLower();
                if (fileextension == ".pdf")
                    filetype = "Pdf";
                else if (fileextension == ".jpeg" || fileextension == ".jpg" || fileextension == ".png" || fileextension == ".gif")
                    filetype = "Image";
                else if (fileextension == ".mp4" || fileextension == ".3gp" || fileextension == ".mkv")
                    filetype = "Viedio";
                else if (fileextension == ".mp3" || fileextension == ".waw")
                    filetype = "Audio";
                SqlCommand Icmd = new SqlCommand("select max(ImageID) from imageMaster", con);
                int maxImageID1 = 1 + Convert.ToInt32(Icmd.ExecuteScalar());

                string path = Server.MapPath("~/" + "Image" + "/" + "Art");

                string ImageName = ShortGuid.NewGuid() + fileextension;
                var mappath = Path.Combine(path, ImageName);
                bool isexists3 = Directory.Exists(mappath);
                if (isexists3 == false)
                {
                    fuimage3.SaveAs(mappath);
                }
                SqlCommand cm = new SqlCommand("insert into ImageMaster values('" + ImageName + "','" + maxId + "','" + filetype + "')", con);
                cm.ExecuteNonQuery();
            }
            lblmsg.Text = "Art Uploaded successfully";
            con.Close();
            Clear();

        }
        public void Clear()
        {
            //drdsubcategory.ClearSelection();
            //drdCategory.ClearSelection();
            txtprice.Text = string.Empty;
            txtname.Text = string.Empty;
            txtdesc.Text = string.Empty;

        }
    }
}