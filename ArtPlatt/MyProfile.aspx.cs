﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ArtPlatt
{
    public partial class MyProfile : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        int ArtistID;
        string EmailID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] != null && (Session["Email"] != null || Session["UserEmail"] != null))
                {

                    string dec = decryptData(Request.QueryString["ID"]);
                    ArtistID = Convert.ToInt32(dec);
                    getArtistDetails(ArtistID);
                    //Bindart(ArtistID);
                }
                else
                {
                    Response.Redirect("Login.aspx");

                }
            }
        }

        private void getArtistDetails(int artistID)
        {
            if (Session["Email"] != null)
            {
                EmailID = Convert.ToString(Session["Email"]);
            }
            else if (Session["UserEmail"] != null)
            {
                EmailID = Convert.ToString(Session["UserEmail"]);
            }

            SqlCommand cmd = new SqlCommand("sp_ArtistProfile", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "selected");
            cmd.Parameters.AddWithValue("@RegId", artistID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                lblfullname.Text = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                lblfname.Text = dt.Rows[0]["FirstName"].ToString();
                lbllname.Text = dt.Rows[0]["LastName"].ToString();
                hfdID.Value = dt.Rows[0]["RegID"].ToString();
                if (!string.IsNullOrEmpty(dt.Rows[0]["ContactNo"].ToString()))
                    lblcontact.Text = dt.Rows[0]["ContactNo"].ToString();
                else
                    lblcontact.Text = "-";
                lbladdress.Text = dt.Rows[0]["Address"].ToString();
                lblEmail1.Text = dt.Rows[0]["EmailID"].ToString();
                lblemail.Text = dt.Rows[0]["EmailID"].ToString();
                pDesc.InnerText = dt.Rows[0]["Description"].ToString();
                imgpv.ImageUrl = "assets/DefaultImage/hello.png";
                if (!string.IsNullOrEmpty(dt.Rows[0]["PhotoPath"].ToString()))
                    imgpv.ImageUrl = "Image/Artist/" + dt.Rows[0]["PhotoPath"].ToString();

                if (!string.IsNullOrEmpty(dt.Rows[0]["CompanyName"].ToString()))
                    lblcompany.Text = dt.Rows[0]["CompanyName"].ToString();
                else
                {
                    lblcompany.Visible = false;
                    lblcom.Visible = false;
                    abt.Visible = false;
                }
                if (!string.IsNullOrEmpty(dt.Rows[0]["CompanyName"].ToString()))
                    lblcompany.Text = dt.Rows[0]["CompanyName"].ToString();
                else
                    lblcompany.Text = "-";

                if (!string.IsNullOrEmpty(dt.Rows[0]["CategoryName"].ToString()))
                    lblcategory.Text = dt.Rows[0]["CategoryName"].ToString();
                if (!string.IsNullOrEmpty(dt.Rows[0]["Name"].ToString()))
                    lblSubcategory.Text = dt.Rows[0]["Name"].ToString();
                else
                    lblcategory.Text = "-";
                if (dt.Rows[0]["UserType"].ToString() == "1")
                    hiddenNameDiv.Visible = false;
                else if (dt.Rows[0]["UserType"].ToString() == "3")
                    hiddendivcategory.Visible = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            con.Close();
        }
        //private void Bindart(int artistID)
        //{
        //    SqlCommand cmd = new SqlCommand("sp_GetAllCategory", con);
        //    con.Open();
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@Action", "ArtistProduct");
        //    cmd.Parameters.AddWithValue("@RegId", artistID);
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);
        //    if (ds.Tables.Count > 0)
        //    {

        //        if (ds.Tables[0].Rows.Count == 0)
        //        {
        //            h3.Visible = false;
        //        }
        //        else
        //        {
        //            rptCategory.DataSource = ds;
        //            rptCategory.DataBind();
        //        }
        //    }

        //    con.Close();
        //}



        protected void lnkmanageArt_Click(object sender, EventArgs e)
        {
            //string ID =Convert.ToString(EncryptData(hfdID.Value));
            Response.Redirect("ManageArt.aspx");
        }
        protected void lnkeditprofile_Click(object sender, EventArgs e)
        {
            string encId = Request.QueryString["ID"];
            Response.Redirect("Registration.aspx?ID=" + encId + "");
        }
        public string EncryptData(string pwd)
        {
            byte[] pd = System.Text.ASCIIEncoding.ASCII.GetBytes(pwd);
            string enpwd = Convert.ToBase64String(pd);
            return enpwd;
        }
        public string decryptData(string pwd)
        {
            byte[] pd = Convert.FromBase64String(pwd);
            string str = System.Text.ASCIIEncoding.ASCII.GetString(pd);
            return str;
        }

        protected void btnchange_Click(object sender, EventArgs e)
        {
            string oldpass = EncryptData(txtCurrentPassword.Text);
            string NewPass = EncryptData(txtNewPassword.Text);
            SqlCommand cmd = new SqlCommand("sp_change_pwd", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@username", lblEmail1.Text);
            cmd.Parameters.AddWithValue("@Action", "User");
            cmd.Parameters.AddWithValue("@old_pwd", oldpass);
            cmd.Parameters.AddWithValue("@new_pwd", NewPass);

            cmd.Parameters.Add("@Status", SqlDbType.Int);
            cmd.Parameters["@Status"].Direction = ParameterDirection.Output;
            con.Open();
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            int retVal = Convert.ToInt32(cmd.Parameters["@Status"].Value);
            if (retVal == 1)
            {
                clear();
                //lblStatus.Text = "Password has been changed successfully";
            }
            else
            {
                clear();
                //lblStatus.Text = "Wrong old username/password. Please re-enter.";
            }
        }
        public void clear()
        {
            txtCurrentPassword.Text = string.Empty;
            txtNewPassword.Text = string.Empty;

        }

        protected void lnkmessage_Click(object sender, EventArgs e)
        {
            string encId = Request.QueryString["ID"];
            Response.Redirect("Messages.aspx?ID=" + encId + "");
        }
    }
}