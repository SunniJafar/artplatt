﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;

namespace ArtPlatt
{
    public partial class EditArt : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        int ArtID;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Session["Email"] != null || Session["UserEmail"] != null)
                {
                    if (Request.QueryString["ArtID"] != null)
                    {
                        ArtID = Convert.ToInt32(Request.QueryString["ArtID"]);
                        SelectData(ArtID);
                    }
                    //BindCategory();

                }
                else
                {
                    Response.Redirect("Login.aspx");

                }

            }
        }

        private void SelectData(int artID)
        {

            SqlCommand cm = new SqlCommand("Select r.FirstName,i.ArtPath,i.ImageID,i.FileType,r.LastName,c.CategoryId, a.ArtName,a.Description,a.SubCategoryId, a.Price from RegistrationMaster as r inner join ArtMaster as a on a.RegID = r.RegId inner join SubCategoryMaster as s on a.SubCategoryId = s.SubCategoryId inner join CategoryMaster as c on s.CategoryId = c.CategoryId inner join ImageMaster as i on a.ArtId=i.ArtID  where a.ArtID='" + artID + "'", con);

            SqlDataAdapter sda = new SqlDataAdapter(cm);
            DataTable dsResut = new DataTable();
            sda.Fill(dsResut);
            ViewState["artID"] = artID;
            //drdCategory.SelectedValue = dsResut.Rows[0]["CategoryId"].ToString();
            //BindSubCategory(Convert.ToInt32(dsResut.Rows[0]["CategoryId"]));
            //drdsubcategory.SelectedValue = dsResut.Rows[0]["SubCategoryId"].ToString();
            txtname.Text = dsResut.Rows[0]["ArtName"].ToString();
            txtprice.Text = dsResut.Rows[0]["Price"].ToString();
            txtdesc.Text = dsResut.Rows[0]["Description"].ToString();

            //drdsubcategory.SelectedValue = dsResut.Rows[0]["SubCategoryId"].ToString();
            if (dsResut.Rows[0]["FileType"].ToString() == "Image")
                imgprv1.ImageUrl = "/Image/Art/" + dsResut.Rows[0]["ArtPath"].ToString();
            else
            {
                prv1.HRef = "/Image/Art/" + dsResut.Rows[0]["ArtPath"].ToString();
                prv1.Visible = true;
            }
            hf1.Value = dsResut.Rows[0]["ArtPath"].ToString();
            hfid1.Value = dsResut.Rows[0]["ImageID"].ToString();
            if (dsResut.Rows.Count >= 1 && dsResut.Rows.Count >= 2)
            {
                if (dsResut.Rows[1]["FileType"].ToString() == "Image")
                {
                    if (!string.IsNullOrEmpty(dsResut.Rows[1]["ArtPath"].ToString()))
                        imgprv2.ImageUrl = "/Image/Art/" + dsResut.Rows[1]["ArtPath"].ToString();
                }
                else
                {
                    prv2.HRef = "/Image/Art/" + dsResut.Rows[1]["ArtPath"].ToString();
                    prv2.Visible = true;
                }

                hf2.Value = dsResut.Rows[1]["ArtPath"].ToString();
                hfid2.Value = dsResut.Rows[0]["ImageID"].ToString();
            }
            if (dsResut.Rows.Count >= 2 && dsResut.Rows.Count >= 3)
            {
                if (dsResut.Rows[2]["FileType"].ToString() == "Image")
                {
                    if (!string.IsNullOrEmpty(dsResut.Rows[2]["ArtPath"].ToString()))
                        imgprv3.ImageUrl = "/Image/Art/" + dsResut.Rows[2]["ArtPath"].ToString();
                }
                else
                {
                    prv3.HRef = "/Image/Art/" + dsResut.Rows[2]["ArtPath"].ToString();
                    prv3.Visible = true;
                }
                hf3.Value = dsResut.Rows[2]["ArtPath"].ToString();
                hfid3.Value = dsResut.Rows[0]["ImageID"].ToString();
            }



        }

        #region category Comment 
        //private void BindSubCategory(int catId)
        //{
        //    drdsubcategory.Enabled = true;
        //    // int CID = Convert.ToInt32(drdCategory.SelectedValue);
        //    SqlDataAdapter sda = new SqlDataAdapter("select * from SubCategoryMaster where CategoryId='" + catId + "'", con);
        //    DataSet ds = new DataSet();
        //    sda.Fill(ds);
        //    drdsubcategory.DataSource = ds;
        //    drdsubcategory.DataTextField = "Name";
        //    drdsubcategory.DataValueField = "SubCategoryID";
        //    drdsubcategory.DataBind();
        //    drdsubcategory.Items.Insert(0, new ListItem("Select", "0"));
        //}

        //private void BindCategory()
        //{
        //    SqlDataAdapter sda = new SqlDataAdapter("select * from CategoryMaster", con);
        //    DataSet ds = new DataSet();
        //    sda.Fill(ds);
        //    drdCategory.DataSource = ds;
        //    drdCategory.DataValueField = "CategoryID";
        //    drdCategory.DataTextField = "CategoryName";
        //    drdCategory.DataBind();
        //    drdCategory.Items.Insert(0, new ListItem("Select", "0"));
        //}
        //protected void drdCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    drdsubcategory.Enabled = true;
        //    int CID = Convert.ToInt32(drdCategory.SelectedValue);
        //    SqlDataAdapter sda = new SqlDataAdapter("select * from SubCategoryMaster where CategoryId='" + CID + "'", con);
        //    DataSet ds = new DataSet();
        //    sda.Fill(ds);
        //    drdsubcategory.DataSource = ds;
        //    drdsubcategory.DataTextField = "Name";
        //    drdsubcategory.DataValueField = "SubCategoryID";
        //    drdsubcategory.DataBind();
        //    drdsubcategory.Items.Insert(0, new ListItem("Select", "0"));

        //} 
        #endregion


        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            ArtID = Convert.ToInt32(ViewState["artID"]);
            string Email = Convert.ToString(Session["Email"]);
            SqlCommand id = new SqlCommand("select * from RegistrationMaster where EmailId='" + Email + "'", con);
            SqlCommand SubcatId = new SqlCommand("select SubCategoryID from RegistrationMaster where EmailId='" + Email + "'", con);
            con.Open();
            int RegId = Convert.ToInt32(id.ExecuteScalar());
            int SubId = Convert.ToInt32(SubcatId.ExecuteScalar());
            SqlCommand cmd = new SqlCommand("update ArtMaster set ArtName='" + txtname.Text + "',Price='" + txtprice.Text + "',SubCategoryId='" + SubId + "',Description='" + txtdesc.Text + "'where ArtID='" + ArtID + "'", con);
            cmd.ExecuteNonQuery();
            SqlCommand sqcmd = new SqlCommand("select max(ArtID) from ArtMaster", con);
            int maxId = Convert.ToInt32(sqcmd.ExecuteScalar());

            if (fuimage1.HasFile)
            {
                string old_image_name = Server.MapPath("~/Image/Art/" + hf1.Value + "");
                if (System.IO.File.Exists(old_image_name))
                {
                    System.IO.File.Delete(Server.MapPath("~/Image/Art/" + hf1.Value + ""));
                    System.IO.File.Delete(old_image_name);
                }
                string fileextension = Path.GetExtension(fuimage1.FileName).ToLower();
                if (hf1.Value != "")
                {

                    string path = Server.MapPath("~/" + "Image" + "/" + "Art");
                    string mappath = path + "\\" + txtname.Text + "_" + hfid1 + fileextension;
                    string ImageName = txtname.Text + "_" + hfid1 + fileextension;
                    bool isexists3 = Directory.Exists(mappath);
                    if (isexists3 == false)
                    {
                        fuimage1.SaveAs(mappath);
                    }
                    SqlCommand cm1 = new SqlCommand("update ImageMaster set ArtPath='" + ImageName + "' where ImageId='" + hfid1.Value + "'", con);
                    cm1.ExecuteNonQuery();
                }
                else
                {
                    SqlCommand Icmd = new SqlCommand("select max(ImageID) from imageMaster", con);
                    int maxImageID1 = 1 + Convert.ToInt32(Icmd.ExecuteScalar());
                    string path = Server.MapPath("~/" + "Image" + "/" + "Art");
                    string mappath = path + "\\" + txtname.Text + "_" + maxImageID1 + fileextension;
                    string ImageName = txtname.Text + "_" + maxImageID1 + fileextension;
                    bool isexists3 = Directory.Exists(mappath);
                    if (isexists3 == false)
                    {
                        fuimage1.SaveAs(mappath);
                    }
                    SqlCommand cmd1 = new SqlCommand("sp_artimage", con);
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@Action", "Insert");
                    cmd1.Parameters.AddWithValue("@ArtPath", ImageName);
                    cmd1.Parameters.AddWithValue("@ArtID", ArtID);
                    cmd1.ExecuteNonQuery();
                }


            }
            if (fuimage2.HasFile)
            {
                string old_image_name = Server.MapPath("~/Image/Art/" + hf2.Value + "");
                if (System.IO.File.Exists(old_image_name))
                {
                    System.IO.File.Delete(Server.MapPath("~/Image/Art/" + hf2.Value + ""));
                    System.IO.File.Delete(old_image_name);
                }
                string img2 = fuimage2.FileName.ToString();
                fuimage2.PostedFile.SaveAs(Server.MapPath("~/Image/Art/") + img2);
                if (hf2.Value != "")
                {
                    SqlCommand cm2 = new SqlCommand("update ImageMaster set ArtPath='" + img2 + "' where ArtId='" + Convert.ToInt32(ViewState["artID"]) + "'", con);
                    cm2.ExecuteNonQuery();
                }
                else
                {
                    SqlCommand cmd2 = new SqlCommand("sp_artimage", con);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.Parameters.AddWithValue("@Action", "Insert");
                    cmd2.Parameters.AddWithValue("@ArtPath", img2);
                    cmd2.Parameters.AddWithValue("@ArtID", ArtID);
                    cmd2.ExecuteNonQuery();
                }

            }
            if (fuimage3.HasFile)
            {
                string old_image_name = Server.MapPath("~/Image/Art/" + hf3.Value + "");
                if (System.IO.File.Exists(old_image_name))
                {
                    System.IO.File.Delete(Server.MapPath("~/Image/Art/" + hf3.Value + ""));
                    System.IO.File.Delete(old_image_name);
                }
                string img3 = fuimage3.FileName.ToString();
                fuimage3.PostedFile.SaveAs(Server.MapPath("~/Image/Art/") + img3);
                if (hf3.Value != "")
                {
                    SqlCommand cm3 = new SqlCommand("update ImageMaster set ArtPath='" + img3 + "' where ArtId='" + Convert.ToInt32(ViewState["artID"]) + "'", con);
                    cm3.ExecuteNonQuery();
                }
                else
                {
                    SqlCommand cmd3 = new SqlCommand("sp_artimage", con);
                    cmd3.CommandType = CommandType.StoredProcedure;
                    cmd3.Parameters.AddWithValue("@Action", "Insert");
                    cmd3.Parameters.AddWithValue("@ArtPath", img3);
                    cmd3.Parameters.AddWithValue("@ArtID", ArtID);
                    cmd3.ExecuteNonQuery();
                }

            }
            Response.Redirect("ManageArt.aspx");
            lblmsg.Text = "Art Upload successfully";
            con.Close();
            Clear();

        }
        public void Clear()
        {
            //drdsubcategory.ClearSelection();
            //drdCategory.ClearSelection();
            txtprice.Text = string.Empty;
            txtname.Text = string.Empty;
            txtdesc.Text = string.Empty;

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string artID = Convert.ToString(ViewState["artID"]);
            SqlCommand cmd = new SqlCommand("delete from ImageMaster where ArtID ='" + artID + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            SqlCommand cmd1 = new SqlCommand("delete from ArtMaster where ArtID = '" + artID + "'", con);
            cmd1.ExecuteNonQuery();
            con.Close();
            Response.Redirect("ManageArt.aspx");

        }
    }
}