﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ArtPlatt
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        string ArtistId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Email"] != null)
            {
                SqlCommand cmd = new SqlCommand("select * from RegistrationMaster where EmailId='" + Session["Email"] + "'", con);
                DataTable dt = new DataTable();
                con.Open();
                SqlDataAdapter SDA = new SqlDataAdapter(cmd);
                SDA.Fill(dt);
                ArtistId = Convert.ToString(dt.Rows[0]["RegID"]);
                btnReg.Text = "Welcome " + dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                btnlogin.Text = "Logout";
                pnlmenuArtist.Visible = true;
                pnllogin.Visible = false;

            }
            else if (Session["UserEmail"] != null)
            {
                SqlCommand cmd = new SqlCommand("select * from RegistrationMaster where EmailId='" + Session["UserEmail"] + "'", con);
                DataTable dt = new DataTable();
                con.Open();
                SqlDataAdapter SDA = new SqlDataAdapter(cmd);
                SDA.Fill(dt);
                ArtistId = Convert.ToString(dt.Rows[0]["RegID"]);
                btnReg.Text = "Welcome " + dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                btnlogin.Text = "Logout";
                pnlmenuArtist.Visible = true;
                pnllogin.Visible = false;
            }
            if (!IsPostBack)
            {

                BindCategory();
            }
        }

        private void BindCategory()
        {
            SqlCommand com = new SqlCommand("sp_Category", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Action", "Select");
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            rptCategory.DataSource = ds;
            rptCategory.DataBind();

            rptfootersub.DataSource = ds;
            rptfootersub.DataBind();

            rptheader.DataSource = ds;
            rptheader.DataBind();
        }

        protected void btnReg_Click(object sender, EventArgs e)
        {
            if (btnReg.Text == "Registration")
            {
                Response.Redirect("Registration.aspx");
            }
            else if (btnReg.Text != "Registration")
            {
                string encId = EncryptData(ArtistId);
                Response.Redirect("MyProfile.aspx?ID=" + encId + "");
            }
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            if (btnlogin.Text == "Login")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                Session.Abandon();
                Session.Clear();
                Response.Redirect("Login.aspx?mode=logout");
            }
        }
        public string EncryptData(string pwd)
        {
            byte[] pd = System.Text.ASCIIEncoding.ASCII.GetBytes(pwd);
            string enpwd = Convert.ToBase64String(pd);
            return enpwd;
        }

        protected void lnk_Click(object sender, EventArgs e)
        {
            string encId = EncryptData(ArtistId);
            Response.Redirect("Registration.aspx?ID=" + encId + "");
        }

        protected void lnkUser_Click(object sender, EventArgs e)
        {
            string encId = EncryptData(ArtistId);
            Response.Redirect("Registration.aspx?UserID=" + encId + "");
        }


        protected void lnkprofile_Click(object sender, EventArgs e)
        {
            string encId = EncryptData(ArtistId);
            Response.Redirect("MyProfile.aspx?ID=" + encId + "");
        }
    }
}