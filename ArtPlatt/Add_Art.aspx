﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Add_Art.aspx.cs" Inherits="ArtPlatt.Add_Art" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        function ValidateFileUploadExtension(Source, args) {
            var fupData = document.getElementById('<%= fuimage1.ClientID %>');
            var FileUploadPath = fupData.value;

            if (FileUploadPath == '') {
                // There is no file selected
                alert("Please select file to upload");
            }
            else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "gif" || Extension == "jpeg" || Extension == "jpg" || Extension == "png" || Extension == "doc" || Extension == "docx" || Extension == "xls" || Extension == "xlsx") {
                    args.IsValid = true; // Valid file type
                }
                else {
                    args.IsValid = false; // Not valid file type
                }
            }
        }
    </script>
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/backgrounds/shop-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>Add Art</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->
    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Coupon -->

            <div class="ws-checkout-content clearfix">

                <!-- Cart Content -->
                <div class="col-sm-7">
                    <center><asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label></center>
                     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <!-- Billing Details -->
                    <div class="ws-checkout-billing">
                        <h3>Art Details</h3>

                        <!-- Form Inputs -->
                        <div class="form-inline">
                            <!-- Name -->
                           <%-- <div class="ws-checkout-first-row">
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Category <span>* </span></label>
                                    <br>
                                   
                                    <asp:UpdatePanel ID="upSetSession" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drdCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drdCategory_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Select Category" CssClass="required" ControlToValidate="drdCategory" InitialValue="0" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="drdCategory" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Sub Category <span>* </span></label>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drdsubcategory" runat="server"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Sub Category" CssClass="required" ControlToValidate="drdsubcategory" ValidationGroup="Add" InitialValue="0"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="drdsubcategory" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>--%>
                            <div class="ws-checkout-first-row">
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Art Name <span>* </span></label>
                                    <br>
                                    <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Art Name" CssClass="required" ControlToValidate="txtname" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                </div>

                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Price (Price In Euro)</label>
                                    <br />
                                    <asp:TextBox ID="txtprice" runat="server" placeholder="00.00"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Price" CssClass="required" ControlToValidate="txtprice" ValidationGroup="Add"></asp:RequiredFieldValidator>--%>
                                    <cc1:filteredtextboxextender runat="server" id="FilteredTextBoxExtender" targetcontrolid="txtprice"
                                        validchars="0123456789." filtermode="ValidChars"></cc1:filteredtextboxextender>
                                </div>
                            </div>
                            <!-- Company -->

                            <div class="col-no-p ws-checkout-input col-sm-12">
                                <label>Description</label><br />
                                <asp:TextBox ID="txtdesc" Rows="2" Columns="5" runat="server" TextMode="MultiLine"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ErrorMessage="Enter Description" CssClass="required" ControlToValidate="txtdesc" ValidationGroup="Add"></asp:RequiredFieldValidator>
                               <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Characters Minimum (50 - 200)"
                                    ControlToValidate="txtdesc" ValidationExpression="^[\s\S]{50,200}" CssClass="required" Display="Dynamic" ValidationGroup="Add"></asp:RegularExpressionValidator>--%>
                            </div>
                            <asp:Button ID="btnsubmit" runat="server" Text="Add Art" Font-Italic="false" Style="width: 100%; background-color: #C2A476; color: #fff;" OnClick="btnsubmit_Click" ValidationGroup="Add" class="btn ws-btn-fullwidth" />
                        </div>
                    </div>
                </div>

                <!-- Cart Total -->
                <div class="col-sm-5">
                    <div class="ws-checkout-order">
                        <h2>Upload your Art </h2>
                        <label class="col-sm-12 form-control-label">Upload Image, Audio, Video, Pdf</label>
                        <asp:FileUpload ID="fuimage1" runat="server" onchange="Validate()(this);" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.mp3|.3gp|.mp4|.png|.jpg|.gif)$"
                            ControlToValidate="fuimage1" runat="server" ForeColor="Red" ValidationGroup="Add" ErrorMessage="Please Upload a valid Video, Audio, Image or PDF File"
                            Display="Dynamic" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Upload Art Image" CssClass="required" ControlToValidate="fuimage1" ValidationGroup="Add"></asp:RequiredFieldValidator>
                        <asp:FileUpload ID="fuimage2" runat="server" /><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.mp3|.3gp|.mp4|.png|.jpg|.gif)$"
                            ControlToValidate="fuimage2" runat="server" ForeColor="Red" ValidationGroup="Add" ErrorMessage="Please Upload a valid Video, Audio, Image or PDF File"
                            Display="Dynamic" />
                        <asp:FileUpload ID="fuimage3" runat="server" /><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.mp3|.3gp|.mp4|.png|.jpg|.gif)$"
                            ControlToValidate="fuimage3" runat="server" ForeColor="Red" ValidationGroup="Add" ErrorMessage="Please Upload a valid Video, Audio, Image or PDF File"
                            Display="Dynamic" />
                    </div>
                </div>
            </div>
        </div>
        <asp:ValidationSummary ID="val" runat="server" ShowSummary="false" ShowMessageBox="false" ValidationGroup="Add" />
    </div>
</asp:Content>
