﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using ArtPlatt.BusinessModel;

namespace ArtPlatt
{
    public partial class ManageArt : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        static List<ArtImage> objArtlist = new List<ArtImage>();
        string EmailID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Email"] != null)
                {
                    EmailID = Convert.ToString(Session["Email"]);
                    BindArt(EmailID);
                }
                else if (Session["UserEmail"] != null)
                {
                    EmailID = Convert.ToString(Session["UserEmail"]);
                    BindArt(EmailID);
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        private void BindArt(string RegEmail)
        {
            SqlCommand cmd = new SqlCommand("select DISTINCT i.ArtID, i.ArtPath,i.FileType, a.ArtName, a.Price, s.Name, c.CategoryName from ArtMaster  as a inner join ImageMaster as i on a.ArtID = i.ArtID inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID inner join CategoryMaster as c on s.CategoryID = c.CategoryId inner join RegistrationMaster as r on r.RegID = a.RegID where r.EmailId='" + RegEmail + "' Order By i.ArtID desc", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dsCategories = new DataSet();
            con.Close();
            da.Fill(dsCategories);
            objArtlist.Clear();

            if (dsCategories.Tables.Count > 0)
            {
                if (dsCategories.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsCategories.Tables[0].Rows)
                    {
                        ArtImage objArt = new ArtImage();
                        objArt.Id = Convert.ToInt16(dr["ArtID"]);
                        //objArt.SubCategoryId = Convert.ToInt16(dr["SubCategoryID"]);
                        objArt.ArtName = dr["ArtName"].ToString();
                        objArt.CategoryName = dr["CategoryName"].ToString();
                        objArt.Path = dr["ArtPath"].ToString();
                        objArt.SubCategoryName = dr["Name"].ToString();
                        objArt.FileType = dr["FileType"].ToString();
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["Price"])))
                            objArt.Price = "€" + " " + Convert.ToDecimal(dr["Price"]);
                        objArtlist.Add(objArt);
                    }
                }
            }
            rptCategory.DataSource = objArtlist;
            rptCategory.DataBind();
        }
    }
}