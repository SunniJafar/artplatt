﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtPlatt.BusinessModel
{
    public class SubCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FileType { get; set; }
        public int Total { get; set; }
        public string trimName { get; set; }

    }
    public class ArtImage
    {
        public string CategoryName { get; set; }
        public short Id { get; set; }
        public string ArtName { get; set; }
        public string Path { get; set; }
        public string Price { get; set; }
        public short SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public string FileType { get; set; }
        public int ArtistId { get; set; }
        public string ArtistName { get; set; }
    }
}