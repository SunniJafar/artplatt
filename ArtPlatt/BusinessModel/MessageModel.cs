﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtPlatt.BusinessModel
{
    public class MessageModel
    {
        public int SenderID { get; set; }
        public string RecieverID { get; set; }
        public string Message { get; set; }
        public string TimeStamp { get; set; }
    }
    public class MessagInfo
    {
        public int SenderID { get; set; }
        public int RecieverID { get; set; }
        public string PhotoPath { get; set; }
        public string message { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Time { get; set; }
    }

}