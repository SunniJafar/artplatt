﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ArtPlatt.Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFt_fAkCV_pURC2Q7qppS-SYO4bhPll2A&libraries=places"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <%--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>--%>
    <script type="text/javascript">
        var markers = [
            <asp:Repeater ID="rptMarkers" runat="server">
                <ItemTemplate>
                    {
                        "title": '<%# Eval("Company") %>',
                        "lat": '<%# Eval("latitude") %>',
                        "lng": '<%# Eval("longitude") %>',
                        "ArtistId": '<%# Eval("ArtistId") %>',
                        "ArtistPhoto": '<%# Eval("ArtistPhoto") %>',
                        "Company": '<%# Eval("Company") %>',


        }
</ItemTemplate>
                <SeparatorTemplate>
                    ,
</SeparatorTemplate>
            </asp:Repeater >
];
    </script>
    <script type="text/javascript">
        window.onload = function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    map.setCenter(initialLocation);
                });
            }
            var mapOptions = {
                //center: new google.maps.LatLng(23.013413, 72.562410),
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{ "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#444444" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 45 }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#46bcec" }, { "visibility": "on" }] }]
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var icon = "";
                if (data.Company == "") {
                    icon = "blue";
                } else {
                    icon = "red";
                }
                icon = "https://maps.google.com/mapfiles/ms/icons/" + icon + ".png";
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,

                    title: data.title,
                    animation: google.maps.Animation.DROP,
                    icon: new google.maps.MarkerImage(icon)

                    //var marker = new google.maps.Marker({
                    //    position: myLatlng,
                    //    map: map,
                    //    title: data.title
                });

                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent("<a href=ArtistProfile.aspx?ArtistID=" + data.ArtistId + "> <img height=50px width=50px src=/Image/Artist/" + data.ArtistPhoto + ">" + "<span style='color:blue'>" + data.title + "<br>" + " View Profile...</span></a>");
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>
    <script type="text/javascript">
        function ValidatorUpdateDisplay(val) {
            if (typeof (val.display) == "string") {
                if (val.display == "None") {
                    return;
                }
                if (val.display == "Dynamic") {
                    val.style.display = val.isvalid ? "none" : "inline";
                    return;
                }

            }
            val.style.visibility = val.isvalid ? "hidden" : "visible";
            if (val.isvalid) {
                document.getElementById(val.controltovalidate).style.border = '1px solid #333';
            }
            else {
                document.getElementById(val.controltovalidate).style.border = '1px solid red';
            }
        }
    </script>
    <div class="ws-subscribe-content text-center clearfix" style="padding-top: 20px; padding-bottom: 20px">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="form-inline">
                <div class="form-group">
                    <asp:TextBox ID="txtsearch" runat="server" class="form-control" placeholder="Search here"></asp:TextBox>
                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtsearch"
                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetName">
                    </cc1:AutoCompleteExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="required" ControlToValidate="txtsearch" ValidationGroup="Add"></asp:RequiredFieldValidator>
                </div>
                <asp:Button runat="server" ID="btnsearch" Text="Search" OnClick="btnsearch_Click" ValidationGroup="Add" class="btn ws-btn-subscribe" />
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12">
        <div id="dvMap" style="width: 100%; height: 400px">
        </div>
        <%--<cc1:GMap ID="GMap1" runat="server" Width="100%" />--%>
    </div>
    <!-- End 3D Parallax Slider -->
    <!-- About Section Start -->
    <%--  <section class="ws-about-section" style="border-bottom: 0px">
        <div class="container">
            <div class="row">

                <!-- Description -->
                <%-- <div class="ws-about-content clearfix">
                    <div class="col-sm-8 col-sm-offset-2" style="padding-top: 25px">
                        <h3>Latest Works</h3>
                        <div class="ws-separator"></div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                    </div>
                </div>-
            </div>
        </div>
    </section>--%>

    <section class="ws-works-section">
        <div class="container">
            <div class="row">

                <asp:HiddenField ID="hfLat" runat="server" />
                <asp:HiddenField ID="hfLon" runat="server" />

                <!-- Item -->
                <asp:Repeater ID="rptCategory" runat="server">
                    <ItemTemplate>

                        <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.1s, ease-in 20px'>

                            <div class="ws-item-offer">
                                <figure>
                                    <center>
                                    <asp:Panel ID="Panel1" runat="server" Visible='<%# Eval("FileType").ToString() == "Viedio"%>'>
                                     <center>  <video id="video" src='/Image/Art/<%#Eval("Path") %>' style="padding-top: 65px;" controls="true" class="img-responsive"></video></center>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel2" runat="server" Visible='<%# Eval("FileType").ToString() == "Image"%>'>
                                      <center>  <img src='/Image/Art/<%#Eval("Path") %>' class="img-responsive" /></center>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel3" runat="server" Visible='<%# Eval("FileType").ToString() == "Audio"%>'>
                                        <img src="assets/DefaultImage/Audio.png" style="height: 236px; width: 100%;" />
                                        <audio src='/Image/Art/<%#Eval("Path") %>' style="width: 100%" controls></audio>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel4" runat="server" Visible='<%# Eval("FileType").ToString() == "Pdf"%>'>
                                        <a href='/Image/Art/<%#Eval("Path") %>' target="_blank">
                                            <img src="assets/DefaultImage/Pdf.png" class="img-responsive" width="100%" /></a>
                                    </asp:Panel>
                                        </center>
                                </figure>
                                <asp:Label runat="server" ForeColor="Red" Font-Size="XX-Large" ID="lblResult" Text=""></asp:Label>
                            </div>
                            <div class="ws-works-caption text-center">
                                <a href="ProductDetail.aspx?ArtID=<%#Eval("Id") %>">
                                    <div class="ws-item-category">
                                        <%#Eval("SubCategoryName") %>
                                    </div>
                                    <h3 class="ws-item-title">
                                        <%#Eval("ArtName") %>
                                    </h3>
                                </a>
                                <a href="ArtistProfile.aspx?ArtistID=<%#Eval("ArtistId") %>">
                                    <div class="ws-item-category" style="text-transform: none !important; margin-top: 5px;">
                                        <%#Eval("ArtistName") %>
                                    </div>
                                </a>


                                <div class="ws-item-separator"></div>
                                <div class="ws-item-price">
                                    <%#Eval("Price") %>
                                </div>

                            </div>

                        </div>

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>
    <!-- Work Collection End  -->
    <!-- Call to Action Section -->
    <section class="ws-call-section">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h2>Are you an Artist or a Place of Art?</h2>
                        <div class="ws-separator"></div>
                        <p>We always welcome talented artists to register and list their work on our website. Your work will be displayed to numerous users across the world.</p>
                        <div class="ws-call-btn">
                            <a href="Registration.aspx">Register here!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:ValidationSummary ID="val" runat="server" ShowSummary="false" ShowMessageBox="false" ValidationGroup="Add" />
    </section>
</asp:Content>
