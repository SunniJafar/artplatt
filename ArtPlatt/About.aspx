﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="ArtPlatt.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page Parallax Header -->
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/backgrounds/faq-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>About Artplatt</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->

    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-faq-page">
                <div class="col-md-8 col-md-offset-2">

                    <!-- Tab Navabar -->

                    <!-- Tab Panes -->
                    <div class="tab-content">

                        <!-- Services Panel -->
                        <div role="tabpanel" class="tab-pane fade in active ws-faq-pane-holder" id="service">

                           
                            <div class="text-center">
                                <h3>About Us</h3>
                                <div class="ws-separator"></div>
                                <p>Artplatt.com is a worldwide ecosystem for Artists, Artplaces and everyone interested in Art of all it's various forms. Our passion is Art. Our Mission is to create a simplified and global access to all art-related activities for everyone.</p>
                                <br />
                                <p>Based on a social network with special art relevant functions it is possible to register as an User to follow or get in touch with the latest trends or most interesting art and artists, or as an Artist to share, to present or to market your works and to get in contact with other Artists, Artplaces and fans. Also you can register as an art-related location or company as an Artplace and use all our benefits for free.</p>
                                <br />
                                <p>All registrated Artists and Artplaces are shown on our interactive worldmap (Artworld), in which the Artists are only visible for registrated users and Artplaces are visible for non-registrated viewers to.</p>
                                <br />
                                <p>Artworld shows up besides the worldwide useractivity in general, all Artplaces on their address. That way Art, Artists and Artplaces can be found and contacted more easily regional and global.</p>
                                <br />
                                <p>
                                    Register on<a href="Registration.aspx" style="color: #C2A476; font-style: normal;"> Artplatt.com</a>  and get started. Let yourself be inspired and inspire the world
                                </p>
                            </div>

                            <!-- Accordion Panel -->
                            <div class="ws-accordion">

                                <!-- Group -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle active" data-toggle="collapse" href="#collapseOne">Privacy Policy
                                        </a>
                                    </div>

                                    <div id="collapseOne" class="accordion-body collapse in">
                                        <div class="accordion-inner">
                                            <p><b>What personal information do we collect from the people that register to our Network?</b></p>
                                            <p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address or other details to help you with your experience.</p>
                                            <br />
                                            <p><b>When do we collect Information?</b></p>
                                            <p>We collect information from you when you register on our site or enter information on our site.</p>
                                            <br />
                                            <p><b>How do we use your information? </b></p>
                                            <p>Responsible use of data is a high priority to us. We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
                                            <br />
                                            <p style="padding-left: 20px">- To improve our website in order to better serve you.</p>
                                            <p style="padding-left: 20px">- To allow us to better service you in responding to your customer service requests.</p>
                                            <p style="padding-left: 20px">- To quickly process your Transactions.</p>
                                            <br />
                                            <p><b>How do we protect your Information?</b></p>
                                            <p>We do not use Malware Scanning.</p>
                                            <br />
                                            <p>We do not use vulnerability scanning and/or scanning to PCI Standards.</p>
                                            <br />
                                            <p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>
                                            <br />

                                            <p>We implement a variety of security measures when a user places an order to maintain the safety of your personal Information.</p>
                                            <br />
                                            <p>For your convenience we may store your credit card information kept for more than 60 days in order to expedite future orders, and to automate the billing process</p>
                                            <br />

                                            <p><b>Use of Cookies.</b></p>
                                            <p>Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>
                                            <br />
                                            <p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. </p>
                                            <br />
                                            <p><b>If users disable cookies in their browser:</b></p>
                                            <p>If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.Some of the features that make your site experience more efficient and may not function properly.</p>
                                            <br />
                                            <p><b>Third-party disclosure</b></p>
                                            <p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information.</p>
                                            <br />
                                            <p><b>Google Analytics</b></p>
                                            <p>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en </p>
                                            <br />
                                            <p>We use Google Analytics on our Website.</p>
                                            <br />
                                            <p>Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.</p>
                                            <br />
                                            <p><b>We have implemented the following:</b></p>
                                            <p style="padding-left: 20px">- Demographics and Interests Reporting</p>
                                            <br />
                                            <p>We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies or other third-party identifiers together track the Position of the user</p>
                                            <br />
                                            <p>Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</p>
                                            <br />
                                            <p><b>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</b></p>
                                            <p>We will notify the users via in-site notification</p><br />
                                            <p style="padding-left:20px">- Within 7 business days</p><br />
                                            <p>We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.
</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Group -->
                                <%--  <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseTwo">Datenschutzerklarung
                                        </a>
                                    </div>

                                    <div id="collapseTwo" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                        </div>
                                    </div>
                                </div>--%>

                                <!-- Group -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseThree">Imprint
                                        </a>
                                    </div>

                                    <div id="collapseThree" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <p><b>Medieninhaber:</b> Artplatt e.U., Lassallestrasse 40/4/8, 1020 Wien/ Vienna, Austria</p><br />
                                            <p><b>Unternehmensgegenstand:</b> Dienstleistungen in der automatischen Datenverarbeitung und Informationstechnik</p><br />
                                            <p><b>Geschäftsführer:</b> Ellia Nikolaevskij</p><br />
                                            <p><b>Firmenbuchnummer:</b> FN 478925b</p><br />
                                            <p><b>Firmenbuchgericht:</b> Handelsgericht Wien</p><br />
                                            <p><b>Kammer:</b> Wirtschaftskammer Wien</p><br />
                                        </div>
                                    </div>
                                </div>

                                <!-- Group -->


                            </div>
                            <!-- End Accordion Panel -->
                        </div>


                    </div>
                    <!-- End Tab Panes -->

                </div>
            </div>
        </div>
    </div>
    <!-- End Page Content -->
</asp:Content>
