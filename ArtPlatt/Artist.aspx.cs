﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ArtPlatt
{
    public partial class Artist : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);


        static List<ArtistModel> objArtist = new List<ArtistModel>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindArtist();
            }

        }

        private void BindArtist()
        {

            SqlCommand cmd = new SqlCommand("sp_ArtistProfile", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            DataSet dsCategories = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            con.Close();
            da.Fill(dsCategories);
            objArtist.Clear();
            if (dsCategories.Tables.Count > 0)
            {
                if (dsCategories.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsCategories.Tables[0].Rows)
                    {
                        ArtistModel objArt = new ArtistModel();
                        objArt.RegId = Convert.ToInt16(dr["RegId"]);
                        //objArt.FirstName = dr["FirstName"].ToString();
                        //objArt.LastName = dr["LastName"].ToString();
                        objArt.CompanyName = dr["CompanyName"].ToString();
                        if (dr["PhotoPath"].ToString() != "")
                        {
                            objArt.PhotoPath = "/Image/Artist/" + dr["PhotoPath"].ToString();
                        }
                        else
                            objArt.PhotoPath = "assets/DefaultImage/ArtistImage.png";
                        objArtist.Add(objArt);
                    }
                }
            }
            rptArtist.DataSource = objArtist;
            rptArtist.DataBind();
        }
        public class ArtistModel
        {
            public short RegId { get; set; }
            public string ArtistName { get; set; }
            public string PhotoPath { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string CompanyName { get; set; }

        }
    }
}