﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Artist.aspx.cs" Inherits="ArtPlatt.Artist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/backgrounds/shop-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>Art Place</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-shop-page">
                <div class="tab-content">
                    <asp:Repeater ID="rptArtist" runat="server">
                        <ItemTemplate>
                            <div class="col-sm-6 col-md-4 ws-works-item">
                                <center>
                                <a href="ArtistProfile.aspx?ArtistID=<%#Eval("RegId") %>">
                                    <div class="ws-item-offer">
                                        <figure>
                                            <img src='<%#Eval("PhotoPath") %>' class="img-responsive" style="height: 300px; width: 300px" />
                                        </figure>
                                    </div>
                              
                                <div class="ws-works-caption text-center">
                                    <%#Eval("CompanyName") %>
                                </div>
                                      </a>
                                    </center>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            <center>
                                <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Currently Art not Available"></asp:Label>
                                    </center>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
