﻿using ArtPlatt.BusinessModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArtPlatt
{
    public partial class Messages : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        DataSet dsMessage = new DataSet();
        static List<MessagInfo> objMessageInfo = new List<MessagInfo>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Email"] != null || Session["UserEmail"] != null)
                {
                    if (Request.QueryString["ID"] != null)
                    {
                        int RegId = Convert.ToInt32(decryptData(Request.QueryString["ID"]));
                        GetMessage(RegId);
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        private void GetMessage(int id)
        {
            SqlCommand cmd = new SqlCommand("sp_Message", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "SelectedMessage");
            cmd.Parameters.AddWithValue("@RecieverID", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(dsMessage);
            objMessageInfo.Clear();
            if (dsMessage.Tables.Count > 0)
            {
                if (dsMessage.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsMessage.Tables[0].Rows)
                    {
                        MessagInfo objMessage = new MessagInfo();
                        objMessage.RecieverID = Convert.ToInt32(dr["RecieverID"]);
                        objMessage.SenderID = Convert.ToInt32(dr["SenderID"].ToString());
                        objMessage.PhotoPath = dr["PhotoPath"].ToString();
                        objMessage.FirstName = dr["FirstName"].ToString();
                        objMessage.LastName = dr["LastName"].ToString();
                        objMessage.message = dr["message"].ToString();
                        objMessage.Time = dr["Time"].ToString();

                        if (dr["PhotoPath"].ToString() != "")
                        {
                            objMessage.PhotoPath = "/Image/Artist/" + dr["PhotoPath"].ToString();
                        }
                        else
                        {
                            objMessage.PhotoPath = "assets/DefaultImage/ArtistImage.png";
                        }
                        objMessageInfo.Add(objMessage);
                    }
                }
            }
            rptMessages.DataSource = objMessageInfo;
            rptMessages.DataBind();

        }
        public string EncryptData(string pwd)
        {
            byte[] pd = System.Text.ASCIIEncoding.ASCII.GetBytes(pwd);
            string enpwd = Convert.ToBase64String(pd);
            return enpwd;
        }
        public string decryptData(string pwd)
        {
            byte[] pd = Convert.FromBase64String(pwd);
            string str = System.Text.ASCIIEncoding.ASCII.GetString(pd);
            return str;
        }

    }

}