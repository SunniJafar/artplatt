﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using ArtPlatt.BusinessModel;

namespace ArtPlatt
{
    public partial class Category : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        DataSet dsCategories = new DataSet();
        static List<ArtImage> objArtList = new List<ArtImage>();
        static List<SubCategory> objSubCategoryList = new List<SubCategory>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime now = DateTime.Now;
                String dt = System.DateTime.Now.ToString("hh : mm: tt");
                if (Request.QueryString["CatId"] != null)
                {
                    int CatId = Convert.ToInt32(Request.QueryString["CatId"]);
                    //getCategory(CatId);
                    BindAllCategory(CatId);
                    BindSubCategory(CatId);

                }
                else if (Request.QueryString["Search"] != null)
                {
                    string Search = Convert.ToString(Request.QueryString["Search"]);
                    SearchData(Search);
                }
                else
                    BindCategory();


            }

        }

        private void BindCategory()
        {
            SqlCommand cmd = new SqlCommand("sp_GetAllCategory", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "Select");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            con.Close();
            da.Fill(dsCategories);

            objArtList.Clear();

            if (dsCategories.Tables.Count > 0)
            {
                if (dsCategories.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsCategories.Tables[0].Rows)
                    {
                        ArtImage objArt = new ArtImage();
                        objArt.Id = Convert.ToInt16(dr["ArtID"]);
                        objArt.SubCategoryId = Convert.ToInt16(dr["SubCategoryID"]);
                        objArt.ArtName = dr["ArtName"].ToString();
                        objArt.CategoryName = dr["CategoryName"].ToString();
                        objArt.Path = dr["ArtPath"].ToString();
                        objArt.SubCategoryName = dr["Name"].ToString();
                        objArt.FileType = dr["FileType"].ToString();
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["Price"])))
                            objArt.Price = "€" + " " + Convert.ToDecimal(dr["Price"]);
                        objArtList.Add(objArt);
                    }
                }
            }

            rptCategoryall.DataSource = objArtList;
            rptCategoryall.DataBind();
        }

        private void BindSubCategory(int CatId)
        {
            SqlCommand cm = new SqlCommand("select Distinct s.SubCategoryID As Id, s.Name, count(i.SubCategoryID) As Total  from SubCategoryMaster as s left join ArtMaster as i on s.SubCategoryId = i.SubCategoryId where CategoryId='" + CatId + "' group by s.Name, s.SubCategoryID", con);
            SqlDataAdapter ad = new SqlDataAdapter(cm);
            DataSet sd = new DataSet();
            con.Close();
            ad.Fill(sd);
            objSubCategoryList.Clear();
            if (sd.Tables.Count > 0)
            {
                if (sd.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in sd.Tables[0].Rows)
                    {
                        SubCategory objSubCategory = new SubCategory();
                        objSubCategory.Id = Convert.ToInt16(dr["Id"]);
                        objSubCategory.Name = dr["Name"].ToString();
                        objSubCategory.trimName = dr["Name"].ToString().Replace(" ", string.Empty);
                        objSubCategory.Total = Convert.ToInt16(dr["Total"]);
                        objSubCategoryList.Add(objSubCategory);
                    }
                }
            }
            rptsubCat.DataSource = objSubCategoryList;
            rptsubCat.DataBind();

            rptsubCatTab.DataSource = objSubCategoryList;
            rptsubCatTab.DataBind();
        }

        //private void getCategory(int CatId)
        //{
        //    SqlCommand cmd = new SqlCommand("sp_GetAllCategory", con);
        //    con.Open();
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@Action", "ByCategory");
        //    cmd.Parameters.AddWithValue("@CatID", CatId);
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    con.Close();
        //    da.Fill(ds);

        //    objArtList.Clear();
        //    if (ds.Tables.Count > 0)
        //    {
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in dsCategories.Tables[0].Rows)
        //            {
        //                ArtImage objArt = new ArtImage();
        //                objArt.Id = Convert.ToInt16(dr["ArtID"]);
        //                objArt.SubCategoryId = Convert.ToInt16(dr["SubCategoryID"]);
        //                objArt.ArtName = dr["ArtName"].ToString();
        //                objArt.CategoryName = dr["CategoryName"].ToString();
        //                objArt.Path = dr["ArtPath"].ToString();
        //                objArt.Price = Convert.ToDecimal(dr["Price"]);
        //                objArtList.Add(objArt);
        //            }
        //        }
        //    }

        //    rptCategoryall.DataSource = objArtList;
        //    rptCategoryall.DataBind();
        //}




        //protected void Unnamed_Click(object sender, EventArgs e)
        //{
        //    int SubCategoryId = Convert.ToInt16(((Button)sender).CommandArgument);
        //    GetBySubCategory(SubCategoryId);
        //}

        protected void rptsubCat_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int SubCategoryId = Convert.ToInt16(e.CommandArgument);
            GetBySubCategory(SubCategoryId);
        }
        #region Search WebMethod
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetName(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("select Distinct(FirstName) from RegistrationMaster where UserType!=3 and FirstName like @Name+'%'", con);
            cmd.Parameters.AddWithValue("@Name", prefixText);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<string> ArtistNames = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ArtistNames.Add(dt.Rows[i][0].ToString());
            }

            SqlCommand cat = new SqlCommand("select * from CategoryMaster where CategoryName like @Name+'%'", con);
            cat.Parameters.AddWithValue("@Name", prefixText);
            SqlDataAdapter daCate = new SqlDataAdapter(cat);
            DataTable dtResult = new DataTable();
            daCate.Fill(dtResult);
            for (int i = 0; i < dtResult.Rows.Count; i++)
            {
                ArtistNames.Add(dtResult.Rows[i][1].ToString());
            }

            SqlCommand scat = new SqlCommand("select  Distinct(Name) from SubCategoryMaster where Name like @subName+'%'", con);
            scat.Parameters.AddWithValue("@subName", prefixText);
            SqlDataAdapter dasCate = new SqlDataAdapter(scat);
            DataTable dtsResult = new DataTable();
            dasCate.Fill(dtsResult);
            for (int i = 0; i < dtsResult.Rows.Count; i++)
            {
                ArtistNames.Add(dtsResult.Rows[i][0].ToString());
            }

            SqlCommand acat = new SqlCommand("select ArtName from ArtMaster where ArtName like @ArtName+'%'", con);
            acat.Parameters.AddWithValue("@ArtName", prefixText);
            SqlDataAdapter daArt = new SqlDataAdapter(acat);
            DataTable dtsArtResult = new DataTable();
            daArt.Fill(dtsArtResult);
            for (int i = 0; i < dtsArtResult.Rows.Count; i++)
            {
                ArtistNames.Add(dtsArtResult.Rows[i][0].ToString());
            }

            SqlCommand pcat = new SqlCommand("select CompanyName from RegistrationMaster where CompanyName like @CompanyName+'%'", con);
            pcat.Parameters.AddWithValue("@CompanyName", prefixText);
            SqlDataAdapter paArt = new SqlDataAdapter(pcat);
            DataTable ptsArtResult = new DataTable();
            paArt.Fill(ptsArtResult);
            for (int i = 0; i < ptsArtResult.Rows.Count; i++)
            {
                ArtistNames.Add(ptsArtResult.Rows[i][0].ToString());
            }
            return ArtistNames;
        }
        #endregion

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("select RegId from RegistrationMaster where FirstName='" + txtsearch.Text + "' or CompanyName='" + txtsearch.Text + "'", con);
            con.Open();
            int RegID = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            if (RegID > 0)
            {
                Response.Redirect("ArtistProfile.aspx?ArtistID=" + RegID + "");
            }
            Response.Redirect("Category.aspx?Search=" + txtsearch.Text + "");
        }

        #region Method
        private void SearchData(string SearchText)
        {
            SqlCommand cmd = new SqlCommand("select  DISTINCT i.ArtID,(select top 1 ArtPath from ImageMaster where ArtId=i.ArtID) as ArtPath, (select top 1 FileType from ImageMaster where ArtId = i.ArtID) as FileType, a.ArtName, a.Price, s.SubCategoryID, s.Name, c.CategoryName, r.FirstName,r.LastName,r.CompanyName,r.RegID from ArtMaster  as a inner join ImageMaster as i on a.ArtID = i.ArtID inner join SubCategoryMaster as s on a.SubCategoryId = s.SubcategoryID inner join CategoryMaster as c on s.CategoryID = c.CategoryId inner join RegistrationMaster as r on r.RegID = a.RegID where c.CategoryName = @Search or r.firstName = @Search or a.ArtName = @Search or s.Name = @Search or r.LastName = @Search or r.CompanyName = @Search Group By i.ArtID, a.ArtName, a.Price, s.SubCategoryID, s.Name, c.CategoryName, r.FirstName,r.LastName,r.CompanyName,r.RegID", con);
            con.Open();
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@Action", "BySearch");
            cmd.Parameters.AddWithValue("@Search", SearchText.Trim());
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            con.Close();
            da.Fill(dsCategories);

            objArtList.Clear();

            if (dsCategories.Tables.Count > 0)
            {
                if (dsCategories.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsCategories.Tables[0].Rows)
                    {
                        ArtImage objArt = new ArtImage();
                        objArt.Id = Convert.ToInt16(dr["ArtID"]);
                        objArt.SubCategoryId = Convert.ToInt16(dr["SubCategoryID"]);
                        objArt.ArtName = dr["ArtName"].ToString();
                        objArt.CategoryName = dr["CategoryName"].ToString();
                        objArt.Path = dr["ArtPath"].ToString();
                        objArt.SubCategoryName = dr["Name"].ToString();
                        objArt.FileType = dr["FileType"].ToString();
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["Price"])))
                            objArt.Price = "€" + " " + Convert.ToDecimal(dr["Price"]);
                        if (!string.IsNullOrEmpty(dr["FirstName"].ToString()) && !string.IsNullOrEmpty(dr["LastName"].ToString()))
                            objArt.ArtistName = dr["FirstName"].ToString() + dr["LastName"].ToString();
                        else
                            objArt.ArtistName = dr["CompanyName"].ToString();
                        objArt.ArtistId = Convert.ToInt32(dr["RegID"]);
                        objArtList.Add(objArt);
                    }
                }
            }

            rptCategoryall.DataSource = objArtList;
            rptCategoryall.DataBind();
        }

        private void GetBySubCategory(int subCatId)
        {
            List<ArtImage> artListBySubCategory = objArtList.Where(x => x.SubCategoryId == subCatId).ToList();

            rptCategoryall.DataSource = artListBySubCategory;
            rptCategoryall.DataBind();

        }

        private void BindAllCategory(int CatId)
        {
            SqlCommand cmd = new SqlCommand("sp_GetAllCategory", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "ByCategory");
            cmd.Parameters.AddWithValue("@CatID", CatId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            con.Close();
            da.Fill(dsCategories);

            objArtList.Clear();

            if (dsCategories.Tables.Count > 0)
            {
                if (dsCategories.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsCategories.Tables[0].Rows)
                    {
                        ArtImage objArt = new ArtImage();
                        objArt.Id = Convert.ToInt16(dr["ArtID"]);
                        objArt.SubCategoryId = Convert.ToInt16(dr["SubCategoryID"]);
                        objArt.ArtName = dr["ArtName"].ToString();
                        objArt.CategoryName = dr["CategoryName"].ToString();
                        objArt.Path = dr["ArtPath"].ToString();
                        objArt.SubCategoryName = dr["Name"].ToString();
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["Price"])))
                            objArt.Price = "€" + " " + Convert.ToDecimal(dr["Price"]);
                        objArt.FileType = dr["FileType"].ToString();
                        if (!string.IsNullOrEmpty(dr["FirstName"].ToString()) && !string.IsNullOrEmpty(dr["LastName"].ToString()))
                            objArt.ArtistName = "By-" + dr["FirstName"].ToString() + dr["LastName"].ToString();
                        else
                            objArt.ArtistName = "By-" + dr["CompanyName"].ToString();
                        objArt.ArtistId = Convert.ToInt32(dr["RegID"]);
                        objArtList.Add(objArt);
                    }
                }
            }

            rptCategoryall.DataSource = objArtList;
            rptCategoryall.DataBind();
        }
        #endregion

        protected void rptsubCatTab_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            int subCatId = Convert.ToInt16(((HiddenField)e.Item.FindControl("hdnID")).Value);

            List<ArtImage> artListBySubCategory = objArtList.Where(x => x.SubCategoryId == subCatId).ToList();

            Repeater rptProducts = (Repeater)e.Item.FindControl("rptProducts");
            rptProducts.DataSource = artListBySubCategory;
            rptProducts.DataBind();

        }

        protected void rptCategoryall_DataBinding(object sender, EventArgs e)
        {

        }


        protected void imgPdf_Click(object sender, EventArgs e)
        {
            string url = string.Format("/Image/Art/?FN=/Image/Art/{0}", (sender as LinkButton).CommandArgument);
            string script = "<script type='text/javascript'>window.open('" + url + "')</script>";
            this.ClientScript.RegisterStartupScript(this.GetType(), "script", script);
        }
        public class ArtImage
        {
            public string CategoryName { get; set; }
            public short Id { get; set; }
            public string ArtName { get; set; }
            public string Path { get; set; }
            public string Price { get; set; }
            public short SubCategoryId { get; set; }
            public string SubCategoryName { get; set; }
            public string FileType { get; set; }
            public int ArtistId { get; set; }
            public string ArtistName { get; set; }
        }
    }
}