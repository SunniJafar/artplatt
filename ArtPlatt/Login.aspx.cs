﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using System.Net.Mail;

namespace ArtPlatt
{
    public partial class Login : System.Web.UI.Page
    {
        private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        string buidstring = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            string encPassword = Encryptpassword(txtpassword.Text);

            SqlCommand cmd = new SqlCommand("sp_login", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "login");
            cmd.Parameters.AddWithValue("@EmailId", txtemail.Text);
            cmd.Parameters.AddWithValue("@Password", encPassword);
            con.Open();
            int obj = Convert.ToInt32(cmd.ExecuteScalar());
            if (obj > 0)
            {
                SqlCommand cmd1 = new SqlCommand("sp_login", con);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Action", "checkstatus");
                cmd1.Parameters.AddWithValue("@EmailId", txtemail.Text);
                cmd1.Parameters.AddWithValue("@Password", encPassword);
                bool chkStatus = Convert.ToBoolean(cmd1.ExecuteScalar());
                if (chkStatus == true)
                {


                    SqlCommand cm = new SqlCommand("select count(*) from RegistrationMaster where EmailId='" + txtemail.Text + "' and UserType=1", con);
                    int obj1 = Convert.ToInt32(cm.ExecuteScalar());
                    if (obj1 > 0)
                    {

                        Session["Email"] = txtemail.Text;
                        Response.Redirect("Default.aspx");
                    }
                    else
                    {
                        Session["UserEmail"] = txtemail.Text;
                        Response.Redirect("Default.aspx");
                    }
                }
                else
                {
                    lblmsg.Text = "Your account has been deactivated!";
                }
            }
            else
            {
                lblmsg.Text = "Email Address/Password is invalid";
            }
        }

        public string Encryptpassword(string pwd)
        {
            byte[] pd = System.Text.ASCIIEncoding.ASCII.GetBytes(pwd);
            string enpwd = Convert.ToBase64String(pd);
            return enpwd;
        }
        public string decryptpassword(string pwd)
        {
            byte[] pd = Convert.FromBase64String(pwd);
            string str = System.Text.ASCIIEncoding.ASCII.GetString(pd);
            return str;
        }

        protected void btnforget_Click(object sender, EventArgs e)
        {

            SqlCommand cmd = new SqlCommand("select * from RegistrationMaster where EmailId='" + txtforgetEmail.Text + "'", con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                #region send mail of ForgetPassword
                string GetPasword = decryptpassword(dt.Rows[0]["Password"].ToString());
                buidstring = dt.Rows[0]["FirstName"] + " " + dt.Rows[0]["LastName"] + "_" + dt.Rows[0]["EmailId"] + "_" + dt.Rows[0]["RegId"] + "_" + GetPasword;

                MailMessage messagemail = new MailMessage();
                SmtpClient smtpClient = new SmtpClient();
                MailAddress fromAddress = new MailAddress("service@artplatt.com");
                string[] strbuild = buidstring.Split('_');
                messagemail.Body = PopulateMailBody(strbuild[0].ToString(), strbuild[1].ToString(), strbuild[2].ToString(), strbuild[3].ToString(), "http://www.artplatt.com/Login.aspx", "");
                messagemail.From = fromAddress;
                messagemail.To.Add(strbuild[1].ToString());
                messagemail.Subject = "ArtPlatt Account Forget Password";
                messagemail.IsBodyHtml = true;
                smtpClient.Host = "smtp.world4you.com";
                smtpClient.Port = Convert.ToInt32(25);
                smtpClient.EnableSsl = false;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential("service@artplatt.com", "service5");
                smtpClient.Send(messagemail);
                #endregion
                lblmsg.Text = "Your password is sent to your Email";

            }
            else
            {
                lblmsg.Text = "Enter Register Email Address";
                txtforgetEmail.Text = string.Empty;
            }
        }
        private string PopulateMailBody(string name, string userName, string refid, string pwd, string url, string description)
        {
            string tittle = "You Successfully Forget Your Password";
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/HTML/Forgetpassword.htm")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Title}", tittle);
            body = body.Replace("{UserName}", userName);
            body = body.Replace("{Name}", name);
            body = body.Replace("{ReferenceID}", refid);
            body = body.Replace("{Password}", pwd);
            body = body.Replace("{url}", url);
            body = body.Replace("{Description}", description);
            return body;
        }
    }
}