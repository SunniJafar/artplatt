﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ArtPlatt
{
    public partial class ArtistProfile : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        string Email;
        int SenderID;

        int ArtistID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ArtistID"] != null)
                {
                    ArtistID = Convert.ToInt32(Request.QueryString["ArtistID"]);
                    getArtistDetails(ArtistID);
                    Bindart(ArtistID);
                }
            }
        }

        private void getArtistDetails(int artistID)
        {
            SqlCommand cmd = new SqlCommand("sp_ArtistProfile", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "selected");
            cmd.Parameters.AddWithValue("@RegId", artistID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            try
            {
                if (!string.IsNullOrEmpty(dt.Rows[0]["CompanyName"].ToString()))
                    lblName.Text = dt.Rows[0]["CompanyName"].ToString();
                else
                    lblName.Text = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                lblcontact.Text = dt.Rows[0]["ContactNo"].ToString();
                lbladdress.Text = dt.Rows[0]["Address"].ToString();
                if (dt.Rows[0]["UserType"].ToString() == "1")
                    lbladdress.Visible = true;
                else
                    lbladdress.Visible = false;
                //lblEmail.Text = dt.Rows[0]["EmailID"].ToString();
                pDesc.InnerText = dt.Rows[0]["Description"].ToString();
                imgurl.ImageUrl = "assets/DefaultImage/ArtistImage.png";
                if (!string.IsNullOrEmpty(dt.Rows[0]["PhotoPath"].ToString()))
                    imgurl.ImageUrl = "Image/Artist/" + dt.Rows[0]["PhotoPath"].ToString();
                if (Session["Email"] != null)
                    Email = Convert.ToString(Session["Email"]);
                else if (Session["UserEmail"] != null)
                    Email = Convert.ToString(Session["UserEmail"]);

                if (Email != null)
                {
                    SqlCommand id = new SqlCommand("select RegID from RegistrationMaster where EmailId='" + Email + "'", con);
                    SenderID = Convert.ToInt32(id.ExecuteScalar());
                    btncontact.Attributes["href"] = "MessageChat.aspx?SenderId=" + SenderID + "&RecieverID=" + ArtistID + "";
                    if (Email == dt.Rows[0]["EmailID"].ToString())
                    {
                        btncontact.Visible = false;
                    }

                }
                con.Close();

            }

            catch (Exception ex)
            {
                throw ex;
            }
            con.Close();
        }

        private void Bindart(int artistID)
        {
            SqlCommand cmd = new SqlCommand("sp_GetAllCategory", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "ArtistProduct");
            cmd.Parameters.AddWithValue("@RegId", artistID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count == 0)
                {
                    h3.Visible = false;
                }
                else
                {
                    rptCategory.DataSource = ds;
                    rptCategory.DataBind();
                }
            }

            con.Close();
        }
    }
}