﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.Net;
using System.Web.Script.Serialization;
using ArtPlatt.BusinessModel;

namespace ArtPlatt
{
    public partial class Default : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        DataSet dsLocation = new DataSet();
        static List<ArtImage> objArtList = new List<ArtImage>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<LocationMaster> objLocation = new List<LocationMaster>();

                SqlCommand cmd = new SqlCommand("sp_Location", con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Action", "select");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                con.Close();
                da.Fill(dsLocation);

                objLocation.Clear();

                if (dsLocation.Tables.Count > 0)
                {
                    if (dsLocation.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsLocation.Tables[0].Rows)
                        {
                            LocationMaster objLoc = new LocationMaster();
                            objLoc.latitude = dr["LocLat"].ToString();
                            objLoc.longitude = dr["LocLong"].ToString();
                            objLoc.ArtistName = dr["FirstName"].ToString() + " " + dr["LastName"].ToString();
                            objLoc.ArtistId = Convert.ToInt64(dr["RegID"].ToString());
                            objLoc.ArtistPhoto = dr["PhotoPath"].ToString();
                            objLoc.Company = dr["CompanyName"].ToString();
                            // objLoc.Total = dr["Total"].ToString();
                            objLocation.Add(objLoc);
                        }
                    }
                }
                rptMarkers.DataSource = objLocation;
                rptMarkers.DataBind();

                //double lat = Convert.ToDouble(hfLat.Value);
                //double lon = Convert.ToDouble(hfLon.Value);


                //    GLatLng mainLocation = new GLatLng(23.004185, 72.494254);
                //    GMap1.setCenter(mainLocation, 10);

                //    XPinLetter xpinLetter = new XPinLetter(PinShapes.pin_star, "H", Color.Blue, Color.White, Color.Chocolate);
                //    GMap1.Add(new GMarker(mainLocation, new GMarkerOptions(new GIcon(xpinLetter.ToString(), xpinLetter.Shadow()))));

                //    List<LocationMaster> objLocation = new List<LocationMaster>();

                //    SqlCommand cmd = new SqlCommand("sp_Location", con);
                //    con.Open();
                //    cmd.CommandType = CommandType.StoredProcedure;
                //    cmd.Parameters.AddWithValue("@Action", "select");
                //    SqlDataAdapter da = new SqlDataAdapter(cmd);
                //    DataSet ds = new DataSet();
                //    con.Close();
                //    da.Fill(dsLocation);

                //    objLocation.Clear();

                //    if (dsLocation.Tables.Count > 0)
                //    {
                //        if (dsLocation.Tables[0].Rows.Count > 0)
                //        {
                //            foreach (DataRow dr in dsLocation.Tables[0].Rows)
                //            {
                //                LocationMaster objLoc = new LocationMaster();
                //                objLoc.latitude = dr["LocLat"].ToString();
                //                objLoc.longitude = dr["LocLong"].ToString();
                //                objLoc.ArtistName = dr["FirstName"].ToString() + " " + dr["LastName"].ToString();
                //                objLoc.ArtistId = Convert.ToInt64(dr["RegID"].ToString());
                //                objLoc.ArtistPhoto = dr["PhotoPath"].ToString();
                //                objLocation.Add(objLoc);
                //            }
                //        }
                //    }

                //    List<LocationMaster> ArtistLocation = objLocation;

                //    PinIcon p;
                //    GMarker gm;
                //    GInfoWindow win;
                //    foreach (var i in ArtistLocation)
                //    {
                //        p = new PinIcon(PinIcons.location, Color.Red);
                //        gm = new GMarker(new GLatLng(Convert.ToDouble(i.latitude), Convert.ToDouble(i.longitude)),
                //            new GMarkerOptions(new GIcon(p.ToString(), p.Shadow())));
                //        win = new GInfoWindow(gm, "<img height=50px width=50px src=/Image/Artist/" + i.ArtistPhoto + ">" + "  " + i.ArtistName + "<br>" + " <a href=ArtistProfile.aspx?ArtistID=" + i.ArtistId + ">View Profile...</a>", false, GListener.Event.mouseover);

                //        GMap1.Add(win);
                //    }
                //}
                BindAllCategory();
            }
        }

        private void BindAllCategory()
        {
            SqlCommand cmd = new SqlCommand("sp_GetAllCategory", con);
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Action", "TopProduct");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dsCategories = new DataSet();
            da.Fill(dsCategories);
            objArtList.Clear();

            if (dsCategories.Tables.Count > 0)
            {
                if (dsCategories.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsCategories.Tables[0].Rows)
                    {
                        ArtImage objArt = new ArtImage();
                        objArt.Id = Convert.ToInt16(dr["ArtID"]);
                        objArt.SubCategoryId = Convert.ToInt16(dr["SubCategoryID"]);
                        objArt.ArtName = dr["ArtName"].ToString();
                        objArt.CategoryName = dr["CategoryName"].ToString();
                        objArt.Path = dr["ArtPath"].ToString();
                        objArt.SubCategoryName = dr["Name"].ToString();
                        objArt.FileType = dr["FileType"].ToString();
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["Price"])))
                            objArt.Price = "€" + " " + Convert.ToDecimal(dr["Price"]);
                        if (!string.IsNullOrEmpty(dr["FirstName"].ToString()) && !string.IsNullOrEmpty(dr["LastName"].ToString()))
                            objArt.ArtistName = dr["FirstName"].ToString() + dr["LastName"].ToString();
                        else
                            objArt.ArtistName = dr["CompanyName"].ToString();
                        objArt.ArtistId = Convert.ToInt32(dr["RegID"]);
                        objArtList.Add(objArt);
                    }
                }
            }
            rptCategory.DataSource = objArtList;
            rptCategory.DataBind();
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetName(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("select Distinct(FirstName) from RegistrationMaster where UserType!=3 and FirstName like @Name+'%'", con);
            cmd.Parameters.AddWithValue("@Name", prefixText);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<string> ArtistNames = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ArtistNames.Add(dt.Rows[i][0].ToString());
            }

            SqlCommand cat = new SqlCommand("select * from CategoryMaster where CategoryName like @Name+'%'", con);
            cat.Parameters.AddWithValue("@Name", prefixText);
            SqlDataAdapter daCate = new SqlDataAdapter(cat);
            DataTable dtResult = new DataTable();
            daCate.Fill(dtResult);
            for (int i = 0; i < dtResult.Rows.Count; i++)
            {
                ArtistNames.Add(dtResult.Rows[i][1].ToString());
            }

            SqlCommand scat = new SqlCommand("select  Distinct(Name) from SubCategoryMaster where Name like @subName+'%'", con);
            scat.Parameters.AddWithValue("@subName", prefixText);
            SqlDataAdapter dasCate = new SqlDataAdapter(scat);
            DataTable dtsResult = new DataTable();
            dasCate.Fill(dtsResult);
            for (int i = 0; i < dtsResult.Rows.Count; i++)
            {
                ArtistNames.Add(dtsResult.Rows[i][0].ToString());
            }

            SqlCommand acat = new SqlCommand("select ArtName from ArtMaster where ArtName like @ArtName+'%'", con);
            acat.Parameters.AddWithValue("@ArtName", prefixText);
            SqlDataAdapter daArt = new SqlDataAdapter(acat);
            DataTable dtsArtResult = new DataTable();
            daArt.Fill(dtsArtResult);
            for (int i = 0; i < dtsArtResult.Rows.Count; i++)
            {
                ArtistNames.Add(dtsArtResult.Rows[i][0].ToString());
            }

            SqlCommand pcat = new SqlCommand("select CompanyName from RegistrationMaster where CompanyName like @CompanyName+'%'", con);
            //SqlCommand pcat = new SqlCommand("select CompanyName from RegistrationMaster where CompanyName like '"+prefixText+"'", con);
            pcat.Parameters.AddWithValue("@CompanyName", prefixText);
            SqlDataAdapter paArt = new SqlDataAdapter(pcat);
            DataTable ptsArtResult = new DataTable();
            paArt.Fill(ptsArtResult);
            for (int i = 0; i < ptsArtResult.Rows.Count; i++)
            {
                ArtistNames.Add(ptsArtResult.Rows[i][0].ToString());
            }
            return ArtistNames;
        }
        protected void btnsearch_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("select RegId from RegistrationMaster where FirstName='" + txtsearch.Text + "' or CompanyName='" + txtsearch.Text + "'", con);
            con.Open();
            int RegID = Convert.ToInt32(cmd.ExecuteScalar());
            con.Close();
            if (RegID > 0)
            {
                Response.Redirect("ArtistProfile.aspx?ArtistID=" + RegID + "");
            }
            Response.Redirect("Category.aspx?Search=" + txtsearch.Text + "");
        }
        public class LocationMaster
        {
            public string latitude { get; set; }
            public string longitude { get; set; }
            public long ArtistId { get; set; }
            public string ArtistName { get; set; }
            public string ReadMoreUrl { get; set; }
            public string ArtistPhoto { get; set; }
            public string Company { get; set; }
            public string Total { get; set; }
        }
    }
}