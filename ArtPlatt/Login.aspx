﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ArtPlatt.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/backgrounds/shop-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>My Account</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->
    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <!-- Login Form -->
                <div class="ws-login-form">
                    <!-- Email -->
                    <div class="form-group">
                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label><br />
                        <label class="control-label">Email Address<span>*</span></label><br />
                        <asp:TextBox ID="txtemail" class="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Username or Email" ForeColor="Red" ControlToValidate="txtemail" ValidationGroup="login"></asp:RequiredFieldValidator>
                    </div>
                    <!-- Password -->
                    <div class="form-group">
                        <label class="control-label">Password <span>*</span></label>
                        <asp:TextBox ID="txtpassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Password" ForeColor="Red" ControlToValidate="txtpassword" ValidationGroup="login"></asp:RequiredFieldValidator>
                    </div>
                    <!-- Checkbox -->
                    <div class="pull-left">
                        <div class="checkbox">
                            <%--  <label>
                                <input type="checkbox">Stay signed in
                            </label>--%>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="ws-forgot-pass">
                            <a href="#ws-register-modal" data-toggle="modal">Forgot your password?</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="padding-top-x50"></div>
                    <!-- Button -->
                    <asp:Button ID="btnlogin" runat="server" Text="Login" ValidationGroup="login" Font-Italic="false" OnClick="btnlogin_Click" class="btn ws-btn-fullwidth" Style="width: 100%; background-color: #C2A476; color: #fff;" />
                    <br />
                    <br />
                    <div class="modal fade" id="ws-register-modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                </div>

                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="modal-body">
                                            <div class="ws-register-form">
                                                <h3>Forgot your password</h3>
                                                <div class="ws-separator"></div>
                                                <!-- Name -->
                                                <div class="form-group">
                                                    <label class="control-label">Enter Register Email Address <span>*</span></label>
                                                    <asp:TextBox ID="txtforgetEmail" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Email Address" CssClass="required" ControlToValidate="txtforgetEmail" ValidationGroup="Insert" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtforgetEmail"
                                                        ErrorMessage="Enter Valid Email Address" CssClass="required" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                        ValidationGroup="Insert" Display="Dynamic">
                                                    </asp:RegularExpressionValidator>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <asp:Button ID="btnforget" runat="server" class="btn ws-btn-fullwidth" OnClick="btnforget_Click" Text="Send password" Style="width: 100%; background-color: #C2A476; color: #fff;"></asp:Button>
                                            <br />
                                            <b></b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Facebook Button -->
                </div>

                <!-- End Login Form -->
                <!-- Register Form-->
                <!-- End Register -->
            </div>
        </div>
    </div>
    <asp:ValidationSummary ID="val" runat="server" ShowSummary="false" ShowMessageBox="false" ValidationGroup="login" />
</asp:Content>
