﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="MessageChat.aspx.cs" Inherits="ArtPlatt.Mesage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <link href="assets/css/Message.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {
            var element = document.getElementById("scroll");
            element.scrollTop = element.scrollHeight;
            $('btnSendMsg').click(function () {
                var element = document.getElementById("scroll");
                element.scrollTop = element.scrollHeight;
            });
        });
        function getData() {
            var element = document.getElementById("scroll");
            var test = document.getElementById("<%=txtMessage.ClientID%>").focus();
            element.scrollTop = element.scrollHeight;
        }
        function ValidatorUpdateDisplay(val) {
            if (typeof (val.display) == "string") {
                if (val.display == "None") {
                    return;
                }
                if (val.display == "Dynamic") {
                    val.style.display = val.isvalid ? "none" : "inline";
                    return;
                }

            }
            val.style.visibility = val.isvalid ? "hidden" : "visible";
            if (val.isvalid) {
                document.getElementById(val.controltovalidate).style.border = '1px solid #333';
            }
            else {
                document.getElementById(val.controltovalidate).style.border = '1px solid red';
            }
        }
    </script>
    <div class="container ws-page-container">
        <asp:ScriptManager EnablePartialRendering="true"
            ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h4 class="modal-title">
                    <asp:Label ID="lblName" runat="server" Text="lblName"></asp:Label></h4>
                <meta http-equiv="refresh" content="30" id="refresh" />
                <div class="modal-body">
                    <div id="scroll" style="overflow-y: scroll; overflow-x: hidden; max-height: 500px;">
                        <asp:Repeater runat="server" ID="rpt" OnItemDataBound="rpt_ItemDataBound">
                            <ItemTemplate>

                                <div class="row">
                                    <div runat="server" id="maindiv" class="col-sm-6 pb15">
                                        <div class="white-box2" runat="server" id="divselect">
                                            <div>
                                                <span class="hd">
                                                    <asp:HiddenField ID="hfd" runat="server" Value='<%#Eval("SenderID")%>' />
                                                    <p><%#Eval("Message")%></p>
                                            </div>
                                            <span class="hdsmall"><%#Eval("TimeStamp")%></span>
                                        </div>
                                    </div>

                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <textarea class="white-box2" runat="server" onfocus="this.value = this.value;" id="txtMessage" style="width: 100%"></textarea>
                </div>

                <div class="modal-footer tac">
                  <asp:Timer runat="server" ID="tmr" Interval="25000" ontick="tmr_Tick"></asp:Timer>
                    <center><asp:Button ID="btnSendMsg" ClientIDMode="Static" runat="server" OnClick="btnSendMsg_Click" Text="Send" Font-Italic="false" class="btn ws-btn-fullwidth" Style="width: 50%; background-color: #C2A476; color: #fff;" /></center>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSendMsg" EventName="Click" />
            </Triggers>

        </asp:UpdatePanel>
        <asp:ValidationSummary ID="val" runat="server" ShowSummary="false" ShowMessageBox="false" ValidationGroup="Add" />
    </div>
</asp:Content>
