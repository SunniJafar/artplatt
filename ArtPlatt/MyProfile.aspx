﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="MyProfile.aspx.cs" Inherits="ArtPlatt.MyProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .ProfileLable {
            padding: 0 0 7px 0;
            margin: 0;
            font-size: 12px;
            text-transform: uppercase;
            letter-spacing: 1.5px;
            font-weight: 200;
        }

        .ws-checkout-billing label {
            width: 31% !important;
            font-weight: bold;
            color: black;
        }
    </style>
    <div class="container ws-page-container" style="padding: 5px!important">
        <!-- Cart Content -->
        <div class="clearfix col-sm-12">
            <div class="col-sm-3 ">
                <div class="ws-checkout-order">
                    <center>
                    <asp:Image CssClass="img-circle img-responsive" runat="server" ID="imgpv" width="200" height="200" /><br />
                               <asp:Label ID="lblfullname" CssClass="ProfileLable" Font-Size="X-Large" Font-Bold="true" runat="server" Text="Full Name"></asp:Label><br /><br
                               <asp:Label ID="lblEmail1" CssClass="ProfileLable" runat="server" Text="Email"></asp:Label></center>

                </div>
                <div class="ws-checkout-order">
                    <div class="ws-forgot-pass" style="font-style: normal!important">
                        <div class="ws-forgot-pass" style="border: 1px,solid">
                            <asp:LinkButton runat="server" ID="lnkmanageArt" OnClick="lnkmanageArt_Click" Text="Manage Art"></asp:LinkButton><br />
                            <br />
                            <asp:LinkButton runat="server" ID="lnkmessage" Text="Message" OnClick="lnkmessage_Click"></asp:LinkButton><br />
                            <br />
                            <asp:LinkButton runat="server" ID="lnkeditprofile" OnClick="lnkeditprofile_Click" Text="Edit Profile"></asp:LinkButton><br />
                            <br />
                            <a href="#ws-register-modal" data-toggle="modal">Change password</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-9" style="background-color: #f5f5f5; height: 563px">
                <!-- Billing Details -->
                <div class="ws-checkout-billing">
                    <!-- Form Inputs -->
                    <div class="form-inline">
                        <!-- Name -->
                        <div class="ws-checkout-order-row" style="">
                            <div class="col-no-p ws-checkout-input col-sm-12">
                                <div class="ws-instagram-header h3">
                                    <h3 style="padding-top: 5px">Biography
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div id="hiddenNameDiv" style='display: block;' runat="server">
                            <div class="ws-checkout-first-row">
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>First Name</label>
                                    <asp:Label ID="lblfname" Style="text-align: right" CssClass="ProfileLable" runat="server" Text="Email"></asp:Label>
                                </div>
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Last Name</label>
                                    <asp:Label ID="lbllname" runat="server" CssClass="ProfileLable" Text="Last Name"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="ws-checkout-first-row">
                            <div class="col-no-p ws-checkout-input col-sm-6">
                                <label>Email</label>
                                <asp:Label ID="lblemail" runat="server" CssClass="ProfileLable" Text="Email"></asp:Label>
                            </div>
                            <div class="col-no-p ws-checkout-input col-sm-6">
                                <label>Phone Number</label>
                                <asp:Label ID="lblcontact" runat="server" CssClass="ProfileLable" Text="Number"></asp:Label>
                                <asp:HiddenField ID="hfdID" runat="server" />
                            </div>
                        </div>
                        <div id="hiddendivcategory" style='display: block;' runat="server">
                            <div class="ws-checkout-first-row">
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Category</label>
                                    <asp:Label ID="lblcategory" runat="server" CssClass="ProfileLable" Text="Category"></asp:Label>
                                </div>
                                <div class="col-no-p ws-checkout-input col-sm-6">
                                    <label>Subcategory</label>
                                    <asp:Label ID="lblSubcategory" runat="server" CssClass="ProfileLable" Text="Subcategory"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="ws-checkout-first-row">
                            <div class="col-no-p ws-checkout-input col-sm-6">
                                <label>Address</label>
                                <asp:Label ID="lbladdress" runat="server" CssClass="ProfileLable" Text="Address"></asp:Label>
                            </div>
                        </div>
                        <div class="ws-checkout-first-row">
                            <div class=" col-no-p ws-checkout-input col-sm-6">

                                <label runat="server" id="lblcom">
                                    Company Name</label>
                                <asp:Label ID="lblcompany" runat="server" CssClass="ProfileLable" Text="Company"></asp:Label>
                            </div>
                            <div class=" col-no-p ws-checkout-input col-sm-6">

                                <asp:Label ID="Label1" Visible="false" runat="server" CssClass="ProfileLable" Text="Address"></asp:Label>
                            </div>
                        </div>
                        <div class="ws-checkout-order-row">
                            <div class="col-no-p ws-checkout-input col-sm-12">
                                <div class="ws-instagram-header h3">
                                    <h3 runat="server" id="abt">About</h3>
                                </div>
                            </div>
                        </div>
                        <div class="ws-checkout-first-row">
                            <div class=" col-no-p ws-checkout-input col-sm-12">
                                <div class="col-no-p ws-checkout-input col-sm-12">
                                    <p id="pDesc" runat="server">Des</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="container">
                <div class="row">

                    <%--    <asp:Repeater ID="rptCategory" runat="server">
                <ItemTemplate>

                    <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.1s, ease-in 20px'>
                        <a href="ProductDetail.aspx?ArtID=<%#Eval("ArtID") %>">
                            <div class="ws-item-offer">
                                <figure>
                                    <img src='/Image/Art/<%#Eval("ArtPath") %>' class="img-responsive" />
                                </figure>
                                <asp:Label runat="server" ForeColor="Red" Font-Size="XX-Large" ID="lblResult" Text=""></asp:Label>
                            </div>
                            <div class="ws-works-caption text-center">
                                <div class="ws-item-category">
                                    <%#Eval("Name") %>
                                </div>
                                <div class="ws-item-separator"></div>
                                <h3 class="ws-item-title">
                                    <%#Eval("ArtName") %>
                                    <div class="ws-item-separator"></div>
                                    <div class="ws-item-price">
                                        € <%#Eval("Price") %>
                                    </div>
                            </div>
                        </a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>--%>
                </div>
            </div>
            <div class="modal fade" id="ws-register-modal" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        </div>

                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="modal-body">
                                    <div class="ws-register-form">
                                        <h3>Change password</h3>
                                        <div class="ws-separator"></div>
                                        <!-- Name -->
                                        <div class="form-group">
                                            <label class="control-label">Current Password<span>*</span></label>
                                            <asp:TextBox ID="txtCurrentPassword" TextMode="Password" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Current Password" CssClass="required" ControlToValidate="txtCurrentPassword" ValidationGroup="Insert" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">New Password<span>*</span></label>
                                            <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter New Password" CssClass="required" ControlToValidate="txtNewPassword" ValidationGroup="Insert" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <asp:Button ID="btnchange" runat="server" ValidationGroup="Insert" class="btn ws-btn-fullwidth" OnClick="btnchange_Click" Text="Save Changes" Style="width: 100%; background-color: #C2A476; color: #fff;"></asp:Button>
                                    <br />
                                    <asp:ValidationSummary ID="vali" runat="server" ValidationGroup="Insert" ShowMessageBox="false" ShowSummary="false" />

                                    <b></b>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
