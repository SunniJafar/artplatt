﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Imprint.aspx.cs" Inherits="ArtPlatt.Imprint" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Page Parallax Header -->
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/backgrounds/faq-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>Imprint</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->

    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-faq-page">
                <div class="col-md-8 col-md-offset-2">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active ws-faq-pane-holder" id="service">
                            <div class="accordion-inner">
                                <h3>Imprint</h3>
                                <br />
                                  <p><b>Medieninhaber:</b> Artplatt e.U., Lassallestrasse 40/4/8, 1020 Wien/ Vienna, Austria</p><br />
                                            <p><b>Unternehmensgegenstand:</b> Dienstleistungen in der automatischen Datenverarbeitung und Informationstechnik</p><br />
                                            <p><b>Geschäftsführer:</b> Ellia Nikolaevskij</p><br />
                                            <p><b>Firmenbuchnummer:</b> FN 478925b</p><br />
                                            <p><b>Firmenbuchgericht:</b> Handelsgericht Wien</p><br />
                                            <p><b>Kammer:</b> Wirtschaftskammer Wien</p><br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
