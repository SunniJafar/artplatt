﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageArt.aspx.cs" Inherits="ArtPlatt.ManageArt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/backgrounds/shop-header-bg.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>Manage Art </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container ws-page-container">
        <div class="row">

            <asp:Repeater ID="rptCategory" runat="server">
                <ItemTemplate>

                    <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.1s, ease-in 20px'>
                        <a href="EditArt.aspx?ArtID=<%#Eval("Id") %>">
                            <div class="ws-item-offer">
                                <figure>
                                    <asp:Panel ID="Panel1" runat="server" Visible='<%# Eval("FileType").ToString()=="Viedio"%>'>
                                        <video id="video" src='/Image/Art/<%#Eval("Path") %>' style="padding-top: 65px;" controls="true" class="img-responsive"></video>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel2" runat="server" Visible='<%# Eval("FileType").ToString()=="Image"%>'>
                                        <img src='/Image/Art/<%#Eval("Path") %>' class="img-responsive" />
                                    </asp:Panel>
                                    <asp:Panel ID="Panel3" runat="server" Visible='<%# Eval("FileType").ToString()=="Audio"%>'>
                                        <img src="assets/DefaultImage/Audio.png" style="height: 236px; width: 100%;" />
                                        <audio src='/Image/Art/<%#Eval("Path") %>' style="width: 100%" controls></audio>

                                    </asp:Panel>
                                    <asp:Panel ID="Panel4" runat="server" Visible='<%# Eval("FileType").ToString()=="Pdf"%>'>
                                     <%--   <a href='/Image/Art/<%#Eval("Path") %>' target="_blank"></a>--%>
                                            <img src="assets/DefaultImage/Pdf.png" class="img-responsive" />
                                    </asp:Panel>
                                </figure>
                            </div>
                            <div class="ws-works-caption text-center">
                                <div class="ws-item-category">
                                    <%#Eval("SubCategoryName") %>
                                </div>

                                <h3 class="ws-item-title">
                                    <%#Eval("ArtName") %>

                                    <div class="ws-item-separator"></div>

                                    <div class="ws-item-price">
                                        <%#Eval("Price") %>
                                    </div>
                            </div>
                        </a>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    <center>
                                <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="You haven't uploaded any Art"></asp:Label>
                                    </center>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
