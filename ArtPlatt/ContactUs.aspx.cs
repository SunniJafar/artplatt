﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ArtPlatt
{
    public partial class ContactUs : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (btnsubmit.Text == "Submit")
            {
                SqlCommand cmd = new SqlCommand("insert into ContactUs values('" + txtname.Text + "','" + txtemail.Text + "','" + txtmsg.Text + "')", con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                lblmessage.Text = "Message Sent successfully!";
                Clear();
            }
        }

        private void Clear()
        {
            txtmsg.Text = string.Empty;
            txtemail.Text = string.Empty;
            txtmsg.Text = string.Empty;
            txtname.Text = string.Empty;
        }
    }
}