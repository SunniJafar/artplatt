﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="ArtPlatt.Messages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <%-- <link href="assets/css/Message.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="text/javascript">
          $(document).ready(function () {
              var element = document.getElementById("scroll");
              element.scrollTop = element.scrollHeight;
              });
          });
          </script>
   <style>
       .message-list {
           float: left;
           padding: 0 30px 30px;
           width: 100%;
       }

           .message-list img.left {
               border-radius: 6px 6px 6px 6px;
               -moz-border-radius: 6px 6px 6px 6px;
               -webkit-border-radius: 6px 6px 6px 6px;
           }

           .message-list .white-box2 {
               margin-bottom: 15px;
           }

           .message-list .right {
               margin-top: 15px;
           }

               .message-list .right p {
                   font-size: 12px;
                   margin-bottom: 10px;
                   line-height: 12px;
               }

           .message-list .info-text {
               min-height: 80px;
               padding-left: 95px;
               padding-right: 100px;
           }

               .message-list .info-text .hd {
                   text-transform: uppercase;
                   font-size: 18px;
                   color: #353535;
               }

               .message-list .info-text p {
                   font-size: 14px;
                   color: #ccc;
                   margin-bottom: 0px;
               }

       .white-box2 {
           background-color: #f5f5f5;
           border-radius: 6px 6px 6px 6px;
           padding: 10px;
       }

       .left {
           float: left;
       }

       .right {
           float: right;
       }

       .btn-group-vertical > .btn-group:after, .btn-group-vertical > .btn-group:before, .btn-toolbar:after, .btn-toolbar:before, .clearfix:after, .clearfix:before, .container-fluid:after, .container-fluid:before, .container:after, .container:before, .dl-horizontal dd:after, .dl-horizontal dd:before, .form-horizontal .form-group:after, .form-horizontal .form-group:before, .modal-footer:after, .modal-footer:before, .modal-header:after, .modal-header:before, .nav:after, .nav:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .navbar:after, .navbar:before, .pager:after, .pager:before, .panel-body:after, .panel-body:before, .row:after, .row:before {
           display: table;
           content: " "
       }

       .btn-group-vertical > .btn-group:after, .btn-toolbar:after, .clearfix:after, .container-fluid:after, .container:after, .dl-horizontal dd:after, .form-horizontal .form-group:after, .modal-footer:after, .modal-header:after, .nav:after, .navbar-collapse:after, .navbar-header:after, .navbar:after, .pager:after, .panel-body:after, .row:after {
           clear: both
       }

       .n-btn-1 {
           font-family: 'Raleway-Medium';
           text-align: center;
           color: #fff !important;
           background-color: #c2a476;
           display: inline-block;
           padding: 7px 14px;
           -moz-border-radius: 5px;
           -webkit-border-radius: 5px;
           border-radius: 5px;
       }

           .n-btn-1:hover {
               color: #fff;
           }
   </style>
    <div class="container ws-page-container"">
        <div class="message-list">
              <div id="scroll" style="overflow-y: scroll; overflow-x: hidden; max-height: 500px;">
            <asp:Repeater runat="server" ID="rptMessages">
                <ItemTemplate>
                    <div class="white-box2">
                        <img src="<%#Eval("PhotoPath") %>" height="80px" width="80px" class="left" />
                        <div class="right tac">
                         <%--   <p><%#Eval("Time") %></p>--%>
                            <a class="n-btn-1" href="MessageChat.aspx?SenderID=<%#Eval("SenderID") %>&RecieverID=<%#Eval("RecieverId") %>">Message</a>
                        </div>
                        <div class="info-text">
                            <div class="hd"><%#Eval("FirstName") %> <%#Eval("LastName") %></div>
                            <p><%#Eval("message") %></p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
                  </div>
        </div>
    </div>
</asp:Content>
